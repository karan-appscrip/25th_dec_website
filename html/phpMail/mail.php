<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

define('MAILGUN_URL', 'https://api.mailgun.net/v3/www.appscrip.com');
define('MAILGUN_KEY', 'key-dd26bd7bea672686badde648f8f0dc88'); 

function sendmailToCustomer($to,$toname,$mailfromname,$mailfrom,$subject,$html,$text,$tag,$replyto){
$array_data = array(
                'from'=> 'Appscrip[AMP] <support@appscrip.com>',
                'to'=>$toname.'<'.$to.'>',
                'subject'=>$subject,
                'html'=>$html,
                'text'=>$text,
                'o:tracking'=>'yes',
                'o:tracking-clicks'=>'yes',
                'o:tracking-opens'=>'yes',
                'o:tag'=>$tag,
                'h:Reply-To'=>$replyto
    );
    $session = curl_init(MAILGUN_URL.'/messages');
    curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($session, CURLOPT_USERPWD, 'api:'.MAILGUN_KEY);
    curl_setopt($session, CURLOPT_POST, true);
    curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($session);
    $statusCode = curl_getinfo($session, CURLINFO_HTTP_CODE);

    curl_close($session);
    $results = json_decode($response, true);
    header('Location:https://www.appscrip.com/thank-you/');
//    return $results;

}

function sendmailbymailgun($to,$toname,$mailfromname,$mailfrom,$subject,$html,$text,$tag,$replyto){
    $array_data = array(
		'from'=> 'Appscrip[AMP] <support@appscrip.com>',
		'to'=>$toname.'<'.$to.'>',
		'subject'=>$subject,
		'html'=>$html,
		'text'=>$text,
		'o:tracking'=>'yes',
		'o:tracking-clicks'=>'yes',
		'o:tracking-opens'=>'yes',
		'o:tag'=>$tag,
		'h:Reply-To'=>$replyto
    );
    $session = curl_init(MAILGUN_URL.'/messages');
    curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($session, CURLOPT_USERPWD, 'api:'.MAILGUN_KEY);
    curl_setopt($session, CURLOPT_POST, true);
    curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($session);
    $statusCode = curl_getinfo($session, CURLINFO_HTTP_CODE);

    curl_close($session);
    $results = json_decode($response, true);
    if($statusCode == 200){
	$data="Dear ".$_REQUEST['name']."<br><br> Your enquiry has been noted and sent to our representatives. Kindly allow us 1-2 working days to attend to your request.<br><br> We thank you for your patience while we review your request. In the meantime, keep brainstorming! We love having healthy & innovative discussions. You can check out more ideas on blog.appscrip.com <https://blog.appscrip.com/> <br><br>Watch our product videos here<https://www.appscrip.com/best-software-demo-videos/> <br> Browse our products here<https://www.appscrip.com/ios-android-clone-scripts/> <br> Know more about us here<https://www.appscrip.com/about/> <br><br> Warm Regards, <br>Appscrip ";
	sendmailToCustomer($_REQUEST['email'],$_REQUEST['name'],'appscrip','appscrip.com','Appscrip Has Received Your Enquiry',$data,$data,'','');
	}
    return $results;
}

function getRealIpAddr() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

$ip = getRealIpAddr();
//$testData = 'name : '.$_POST['name'];
$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$testData='From : '.$_REQUEST['name'].' <br>Email : '.$_REQUEST['email'].' <br>Phone : '.$_REQUEST['number'].' <br>product : '.$_REQUEST['cars'].' <br> Message Body : '.$_REQUEST['comment'].'<br><br> Additional info : <br>'.'Sender Ip: '.$ip.'<br> Url : '.$url.'<br> agent : '.$_SERVER['HTTP_USER_AGENT'].'<br><br> -- This e-mail was sent from a AMP contact form on Appscrip
its for admin';
sendmailbymailgun('appscripmarketing@gmail.com','karan','appscrip','appscrip.com','AMP contact form submission',$testData,$testData,'','');
?>
