<?php
/*
  Template Name: Blog Page
 */
?>


<style>
.post-link {
    background: url("https://www.appscrip.com/wp-content/uploads/2018/01/dv.png") !important;
    border-radius: 10px;
    color: #fff;
    font-size: 26px;
    margin: 20px 0 40px 0;
    padding: 20px 0;
    text-align: center;
    width: 100%;
    opacity: 1;
    font-weight: 700;
    /* background-repeat: no-repeat; */
    border-radius: 9px;
    object-fit: contain;
    background-size: 100%;
}
.post.single span {
    font-weight: 500 !important;
}
.post_content span {
    font-weight:500 !important;
}
.content-bar h2 {
    font-size: 20px;
}
@media only screen and (max-width: 480px) and (min-width: 320px){

    h2 {
        font-size: 16px;
        line-height: 25px;
    }
}

ul {
font-weight: 500 !important;
font-size: 16px !important;
line-height: 30px !important;
}
ol{
font-weight: 500 !important;
font-size: 16px !important;
line-height: 30px !important;
}
h2 {
font-size: 21px !important;
}

.page-content .sidebar ul {
    list-style: none;
    padding-left: 0;
    margin-left: 0;
    margin-bottom: 20px;
    padding-bottom: 12px;
    border-bottom: 1px solid #ddd;
    padding: 20px !important;
}
.content-bar .post .post_title a {
    color: #010101;
    font-size: 22px !important;
    line-height: 36px !important;
    text-decoration: none;
}
#blog_p_banner {
    background: url(https://nextjuggernaut.com/wp-content/uploads/2015/07/Juggernaut_blog_banner-1.png) no-repeat center center;
    background-size: cover;
    width: 100%;
    height: 450px;
}
h1#blog_heading {
    color: #fff;
    text-align: center;
    font-size: 33px;
    margin-top: 199px;
}
div#blog_contact_layout {
    background: #EFEFEF;
    padding: 10px;
    max-width: 300px;
    padding-bottom: 10px;
}
div#blog_contact_layout h1 {
    background: #302C29;
    color: #FFF;
    text-align: center;
    padding: 8px 4px 9px;
    font-weight: 400;
    font-size: 17px;
}
div#blog_contact_layout h2 {
    text-align: center;
    padding: 5px 0;
    color: #666;
    font-size: 15px;
}
input#blog_contact_name,input#blog_contact_mail,input#blog_contact_phone {
    width: 96%;
    height: 34px;
    border-radius: 4px;
    border: 1px solid #ccc;
    padding-left: 10px;
    font-size: 16px;
    margin-bottom: 14px;
    outline: 0;
}
textarea#blog_contact_message {
    margin-top: 14px;
    margin-bottom: 14px;
    height: 167px;
	    width: 96%;
    height: 90px;
    border-radius: 4px;
    border: 1px solid #ccc;
    padding-left: 10px;
    font-size: 16px;
    margin-bottom: 14px;
    outline: 0;
    margin-top: 0px;
}
input#blog_contact_submit {
    outline: 0;
    background: #F45C05;
    border: none;
    color: #FFF;
    padding: 0px;
    width: 100%;
    font-size: 20px;
    cursor: pointer;
    text-transform: uppercase;
    border-radius: 5px;
}
div#blog_contact_layout p {
    padding: 0px 0;
    color: #666;
    font-size: 12px;
    text-align: center;
    margin-bottom: 0px;
}
.nopadding{padding:0px!important;position:static!important}
.moving_cart_form.moving_form {
    position: fixed;
    top: 102px;
}
.navbar-default .nav li a:hover, .navbar-default .nav li a:focus {
    outline: 0;
    color: greenyellow!important;
    background: transparent!important;
}
.navbar .sf-menu li:hover, .navbar .sf-menu li.sfHover {
background-color: transparent!important;
color:greenyellow}
li#menu-item-3976 a {
    color: greenyellow!important;
}
.moving_form_pos {
        position: absolute!important;
    top: auto!important;
    bottom: 0px!important;
}
.moving_cart_form.moving_form {
    position: fixed;
    top: 132px;
}
.sidebar_widget:nth-of-type(2),.sidebar_widget:nth-of-type(3),.sidebar_widget:nth-of-type(4){display:none}
ul.paging {
    display: inline-block;
    list-style-type: none;
	padding-left:0px
}
ul.paging li {
    display: inline-block;
    margin-right: 10px;
    border: solid 1px grey;
    padding: 10px;
	background: gainsboro;
}
ul.paging li:hover {
    background: white;
    transition: all 300ms ease-in-out;
}
.post_thumbnil {
    display: none;
}
</style>

<?php get_header(); ?>
<div class="" id="blog_p_banner">
  <div class="container">
        <div class="row">
		<div class="col-md-12">
<div class="page_heading_content">
<h1 id="blog_heading">Where everything about the hottest mobile first startups is discussed !</h1>
</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>

<div class="page-container" style="position:relative!important">
    <div class="container">
        <div class="row">
            <div class="page-content">
                <div class="col-md-9">
                    <div class="content-bar gallery"> 
                        <?php the_content(); ?>
                        <?php
                        $limit = get_option('posts_per_page');
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        query_posts('showposts=' . $limit . '&paged=' . $paged);
                        $wp_query->is_archive = true;
                        $wp_query->is_home = false;
                        ?>
                        <!--Start Post-->
                        <?php get_template_part('loop', 'blog'); ?>   
                        <div class="clear"></div>
                        <?php NovelLite_pagination(); ?> 
                    </div>
                </div>
				<div class="col-md-3 nopadding hidden-sm hidden-xs">
		<!--Start Sidebar-->
		<?php get_sidebar(); ?>
		<!--End Sidebar-->
		</div> 
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<script>


 jQuery(document).ready(function($) {	
 var k = $(document).height(); 
	  var y = $('.outer-footer').height()*2;
	  var ht = k-y;
	$(window).scroll(function() {
		   if ($(this).scrollTop()>450){
			   			   $('.moving_cart_form').addClass('moving_form');
}          else{$('.moving_cart_form').removeClass('moving_form');
}
           if($(this).scrollTop()>k-y){$('.moving_cart_form').removeClass('moving_form');
		   $('.moving_cart_form').addClass('moving_form_pos');
		   }
		   else{
			   $('.moving_cart_form').removeClass('moving_form_pos');

		   }
	  });
	  });
</script>
<?php get_footer(); ?>

