
<style>
.navbar .sf-menu li a{color:#000!important}
.contact_bg1{background:#fceceb;}
.contact_bg2{background:#fff7ee;}
#contact_bg_det{text-align:center;margin-left:-22%}
</style><?php
/*
  Template Name: contact us Page
 */
?>
<?php get_header(); ?>
<div class="page_heading_container3">
<div class="appscrip_contactus_total">
   <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-5 contact_bg1" >
              <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/contactus/contact_bang.png" id="contactus_img1" class="img-responsive wow fadeInRight" style="visibility: visible; animation-duration: 2s; animation-name: fadeInRight;" data-wow-duration="2s">
<div id="contact_bg_det">
<h1 class="contact_head1 wow fadeInUp" style="visibility: visible; animation-duration: 2s;  font-weight:900!important;background:#fceceb" data-wow-duration="2s">BENGALURU(INDIA)</h1>
<p class="contactus_para wow fadeInUp" style="visibility: visible; animation-duration: 2s;" data-wow-duration="2s"> #54, R.B.I.Colony, Anandnagar,<br>
Bengaluru, India, 560024.<br>
US No. @ +1-415-813-5833<br>
India No. @ +91-9902019342<br>
Skype @ appscrip<br>
Email @ dreamer@appscrip.com</p>
</div>
         </div>
         <div class="col-xs-12 col-sm-12 col-md-5 col-md-offset-1 col-sm-5   contact_bg2">
              <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/contactus/contact_atlanta.png"  id="contactus_img2" class="img-responsive wow fadeInLeft" style="visibility: visible; animation-duration: 2s;" data-wow-duration="2s">
           <div  id="contact_bg_det">
<h1 id="content_add_head">ATLANTA(USA)</h1>
<p class="contactus_para wow fadeInUp" style="visibility: visible; animation-duration: 2s;" data-wow-duration="2s" id="content_add_para2"> #54, R.B.I.Colony, Anandnagar,<br>
Bengaluru, India, 560024.<br>
US No. @ +1-415-813-5833<br>
India No. @ +91-9902019342<br>
Skype @ appscrip<br>
Email @ dreamer@appscrip.com</p>
</div>
         </div>
  </div>
</div>
</div>


<div class="row">
     <div class="col-xs-12" style="text-align:center;"><h1 id="contact_form_head">It takes few minutes but will help us reverting quicker!</h1>
     <p id="contact_form_para">Please update a brief about the solution you are looking for…<br>
We make it a point that your business requirements are fulfilled!!</p></div>
    <div class="col-xs-12 col-sm-12 col-md-offset-2 col-md-4 ">
       <h2 class="contact_form_lft_hd">GENERAL STUFF</h2>
   </div>
    <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-3 col-lg-offset-2 col-lg-3">
       <h2 class="contact_form_lft_hd" id="contact_form_lft_hd">YOUR PROJECT</h2>
    </div>
</div>
<div class="page-container">
    <div class="container-fluid" style="padding-right: 0px; 
     padding-left: 0px;border:none ">
        <div class="row">
            <div class="page-content">
             <div class="col-md-12">
              <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
				<?php the_content(); ?>	
			<?php endif; ?>	  
				</div>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>
</div>
<?php get_footer(); ?>

