<?php
/*
  Template Name: Pricing_new
 */
?>
<style>
.vc_column-inner.vc_custom_1474638788005 {
    border-radius: 13px;
    padding-bottom: 20px;
}
ul.products h3 {
    height: 46px!important;
}
.navbar .sf-menu li a {
    color: #000!important;
}
img.woocommerce-placeholder.wp-post-image{display:none}
div#pricing_roadyo_baseapp .post-4649,div#pricing_roadyo_baseapp .post-4650  {
    background: none!important;
    box-shadow: none!important ;
	margin-bottom:0px!important
}
div#pricing_roadyo_baseapp h3 {
    display: none;
}
li.post-4649 .pricing_buttonold,li.post-4650 .pricing_buttonold {
    display: none!important; 
}
div#pricing_roadyo_baseapp span.woocommerce-Price-amount.amount {
    display: none!important;
}
.woocommerce ul.products li.product .button {
     margin-top: 0px!important; 
}
.android_sample_price.wpb_column.vc_column_container.vc_col-sm-2.vc_col-xs-10 {
    width: 12%;
}
.pricing-roadyo-baseapp.wpb_column.vc_column_container.vc_col-sm-2 {
    margin-top: -30px!important;
}
.base_sub {
    margin-top: -10px;
}
.woocommerce ul.products li.product, .woocommerce-page ul.products li.product{
    text-align: center;
    padding-bottom: 25px;
    margin-bottom: 42px;
    border: 0;
    box-sizing: border-box;
    background: #ffffff!important;
    box-shadow: 0 0 1px #DDD;
    width: 100% !important;
    -webkit-box-shadow: 0px 0px 9px 1px rgba(0,0,0,0.23)!important;
    -moz-box-shadow: 0px 0px 9px 1px rgba(0,0,0,0.23)!important;
    box-shadow: 0px 0px 9px 1px rgba(0,0,0,0.15)!important;
}
.pricing_buttonold{
	margin-top:10px;
	outline:0px;
	background-color: transparent;
    color: #333;
    font-size: 1EM;
    border: solid 1px grey;
    border-radius: 25px;
    font-weight: 500;
    text-shadow: none;
    text-transform: uppercase;
    padding: 10px 30px;
    line-height: 110%;
    letter-spacing: .04em;
    box-shadow: none;
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
}
.woocommerce #content input.button:hover, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .woocommerce-page #content input.button:hover, .woocommerce-page #respond input#submit:hover, .woocommerce-page a.button:hover, .woocommerce-page button.button:hover, .woocommerce-page input.button:hover, .woocommerce #content input.button.alt:hover, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, .woocommerce-page #content input.button.alt:hover, .woocommerce-page #respond input#submit.alt:hover, .woocommerce-page a.button.alt:hover, .woocommerce-page button.button.alt:hover, .woocommerce-page input.button.alt:hover, .woocommerce ul.products li.product a.button:hover, .woocommerce.archive ul.products li.product a.button:hover, .woocommerce-page.archive ul.products li.product a.button:hover {
    background: #92cdff;
    color: #2a2a2a;
}

.woocommerce ul.products li.product:hover , .woocommerce-page ul.products li.product:hover {
    background: #03a9f4!important;
    color: white!important;
    transition: all 200ms ease-in-out;
}
.cart_appscrip.wpb_column.vc_column_container.vc_col-sm-3.vc_hidden-xs {
  position:fixed;
  right:0px
}
.Cart_bu {
   
    background: #fff;
	font-weight:600!important;
text-align:center;
border-radius:5px
}
.Cart_bu button:hover{
	background: #5bc0de;
    border: 0px;
    border-radius: 7px;
}
.Cart_bu button {
    width: 100%;
    background: #5bc0de;
    border: 0px;
    border-radius: 7px;
	font-size:14px;
	color:#fff;
	font-weight:500
}
table.shop_table.shop_table_responsive.cart thead tr th {
    display: none;
}
td.product-thumbnail {
    display: none;
}
.woocommerce-page table.cart td, .woocommerce-page table.cart th {
font-size: 12px;
font-weight:600!important}
.cart_totals.calculated_shipping {
    font-size: 10px;
}
.woocommerce .cart-collaterals .cart_totals, .woocommerce-page .cart-collaterals .cart_totals {
     float: none!important;
    width: 100%!important;
}
a.checkout-button.button.alt.wc-forward {
    background: #5bc0de;
    padding: 3% 3%;
    font-size: 14px;
    font-weight: bold;
}
td.product-price,.quantity {
    display: none;
}
td.actions>input{display:none!important}
table.shop_table.shop_table_responsive {
    font-size: 14px;
}
.coupon {
    width: 100%;
}
.woocommerce #content table.cart td.actions .coupon .input-text, .woocommerce table.cart td.actions .coupon .input-text, .woocommerce-page #content table.cart td.actions .coupon .input-text, .woocommerce-page table.cart td.actions .coupon .input-text {
    width: 50%!important;
}
.woocommerce-message {
    font-size: 12px!important;
}
span.woocommerce-Price-amount.amount {
    font-family: sans-serif;
    font-weight: 400;
}
.cart_but:active{background: #5bc0de!important;}
.woocommerce-page table.cart td, .woocommerce-page table.cart th {
    font-weight: 600;
    padding: 1em .5em;
}
a.checkout-button.button.alt.wc-forward {
    font-size: 14px;
}
.woocommerce ul.products li.product .price, .woocommerce-page ul.products li.product .price {
    color: #000;
    font-size: 1.25em;
 margin-top: 0px!important; }
 
 @media screen and (max-width:1370px) and (min-width: 1201px){
	h1#products_base_app {
    font-size: 23px!important;
    margin-top: 10px;
    margin-bottom: 20px;
}
p#products_base_app_p {
    font-size: 14px;
    font-weight: 500;
}
.products_roadyo_title_width.wpb_column.vc_column_container.vc_col-sm-2.vc_col-xs-4 {
    width: 20%;
}
.android_sample_price.wpb_column.vc_column_container.vc_col-sm-2.vc_col-xs-10 {
    width: 18%;
    text-align: right;
}
}
 
 @media screen and (max-width:480px) and (min-width:100px){
.woocommerce ul.products li.product, .woocommerce-page ul.products li.product {
    text-align: center;
    padding-bottom: 25px;
    margin-bottom: 10px;
    padding-top: 15px;
    border: 0;
    box-sizing: border-box;
    background: #96cdff;
    box-shadow: 0 0 1px #DDD;
    width: 90% !important;
    margin-left: 5%;
    padding: 3%;
 }
 p#products_base_app_p {
    font-size: 14px;
    font-weight: 500;
}
.wpb_single_image.vc_align_right {
    text-align: center!important;
}
.products_roadyo_title_width{text-align:rightimportant}
 .wpb_text_column.wpb_content_element.text_align p {
    text-align: right!important;
	    margin-right: -15px;
}
div#pricing_roadyo_buttn {
    -webkit-box-shadow: 0px 0px 16px 2px rgba(8,115,247,0.71);
    -moz-box-shadow: 0px 0px 16px 2px rgba(8,115,247,0.71);
    box-shadow: 0px 0px 16px 2px rgba(8,115,247,0.71);

}
 }
  @media screen and (max-width:767px) and (min-width:481px){
	  
	  .navbar-default.navbar-shrink{box-shadow:none!important;
	  -webkit-box-shadow: none!important; 
    -moz-box-shadow: none!important;
     box-shadow: none!important;}
	 
	 div#pricing_roadyo_buttn {
    position: fixed;
    top: 80px;
    width: 100%;
    z-index: 999999;
    background: white;
    padding-top: 10px;
    padding-bottom: 5px;
	
    -webkit-box-shadow: 0px 0px 16px 2px rgba(8,115,247,0.71);
    -moz-box-shadow: 0px 0px 16px 2px rgba(8,115,247,0.71);
    box-shadow: 0px 0px 16px 2px rgba(8,115,247,0.71);

}
	 
  }
 select.selectpicker {
       display: inline-block;
    padding: 7px 25px;
    margin-bottom: 0;
    font-size: 14px;
    color: #fff;
    width: 55%;
	outline:0px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    background: #00c4ff;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
 border-radius: 4px;}
div#pricing_roadyo_buttn {
        position: fixed;
    top: 56px;
    width: 100%;
    z-index: 999;
    background: white;
    padding-top: 10px;
    padding-bottom: 5px;
}
i.fa.fa-shopping-cart.fa-2x {
    color: white;
    margin-left: 3%;
    margin-top: 2%;
}
span.copy_amount {
        color: #fff;
    font-family: sans-serif;
    font-weight: 500;
    /* text-align: center; */
    margin-left: 11%;
    otmargin-btom: 10%;
font-size: 15px;}
.background_blue {
    background: #03a9f4!important;
}


.first.instock.shipping-taxable.purchasable.product-type-simple.background_blue{
    background: #03a9f4!important;
	color:#fff;
}
div#pricing_roadyo_baseapp .products button {
    display: none!important;
}
div#pricing_roadyo_baseapp .products li:first-child {
    background: transparent!important;
    box-shadow: none!important;
    margin-bottom: 0px!important;
}
.loader {
	display:none;
    position: fixed;
    top: 45%;
    LEFT: 45%;
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
    z-index: 9999999999999;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.drop_shadow {
	display:none;
    width: 100%;
    background: rgba(0,0,0,0.5);
    height: 100%;
    position: fixed;
    top: 0px;
    z-index: 999999;
}
</style>

<?php get_header(); ?>

<h1 style="text-align:center;font-weight:bolder;margin-top:100px;font-size:31px;font-weight:800;margin-bottom:60px">PRICING</h1> 
<div class="page-container">
    <div class="container-fluid" style="padding-right: 0px; 
     padding-left: 0px; ">
        <div class="row">
            <div class="page-content">
             <div class="col-md-12">
              <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
				<?php the_content(); ?>	
			<?php endif; ?>	  
				</div>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>
</div>


<!-- Google Code for Connversion Feb Conversion Page -->
<script type="text/javascript">
/ <![CDATA[ /
var google_conversion_id = 867220945;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "32vUCJa_zG4Q0fvCnQM";
var google_remarketing_only = false;
/ ]]> /
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/867220945/?label=32vUCJa_zG4Q0fvCnQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- END OF  Google Code for Connversion Feb Conversion Page -->


<script>

  jQuery(document).ready(function($) {
	  
if($('.amount').html() == '$0.00'){
	$('.cart_appscrip').hide();
}      function loading() {
						 console.log("23");
				  setTimeout(function(){ if($('.amount').html() != '$0.00'){
		location.reload(true);
		console.log("1");
	} },6000);          
	
}
	  function WPMenucart_Load_JS() {
		$('#wpmenucartli').load(wpmenucart_ajax.ajaxurl+'?action=wpmenucart_ajax&_wpnonce='+wpmenucart_ajax.nonce);
		$('div.wpmenucart-shortcode span.reload_shortcode').load(wpmenucart_ajax.ajaxurl+'?action=wpmenucart_ajax&_wpnonce='+wpmenucart_ajax.nonce);
	  }
	  setInterval(function(){ WPMenucart_Load_JS() }, 1000);
	  
     $('.selectpicker').change(function() {
    var val = $(".selectpicker option:selected").text();
    if (val == "WEB"){
		$('.androidapp_active').hide();
		$('.bounceInRight').hide();
		$('.bounceInUp').hide();
		$('.bounceInUp').show();
	}
	else if (val == "ANDROID APP"){
		$('.androidapp_active').hide();
		$('.bounceInUp').hide();
				$('.bounceInRight').show();
	}
	else if (val == "IOS APP"){ 
		$('.bounceInUp').hide();
		$('.bounceInRight').hide();
				$('.androidapp_active').show(); 
	}
	else if (val == "SELECT APP"){ 
		$('.bounceInUp').show();
		$('.bounceInRight').show();
				$('.androidapp_active').show(); 
	}
});

	  if($('.amount').html() != '$0.00'){
			$('.button').addClass("ajax_add_to_car");
			$('.button').addClass("add_to_cart_butto");
		}
	  $('.ajax_add_to_cart').click(function() {
		  	  if($('.amount').html() == '$0.00'){
				  setTimeout("location.reload(true);",2800);
			  }
		$(this).parent('li').addClass("background_blue"); 
}); 

	  function changeText(){
    $(".copy_amount").html($(".amount").html());
}
setInterval(changeText,5);

	   var d = $(this).data('product_id');      
		 $('.post-'+d).css("background","red");
		 
	  var k = $(document).height();
	  var y = $('.outer-footer').height();
	  var ht = k-y;
	  $(window).scroll(function() {
		   if ($(this).scrollTop()>ht-800){
			$('.cart_appscrip').addClass('moving_cart');
}          else{$('.cart_appscrip').removeClass('moving_cart');}
	  });
      $('.pricing_buttonold').click(function(){
         var m = $(this).siblings('.button').attr("data-product_id"); 
         $("#"+'post-'+m).trigger('click');
      });
	 });

	 
	 
</script>


<?php get_footer(); ?>
<style>
.vc_column_container>.vc_column-inner {
     width: 100%!important;
}

.woocommerce ul.products li.product a.button, .woocommerce.archive ul.products li.product a.button, .woocommerce-page.archive ul.products li.product a.button {
    background-color: transparent;
    color: #333;
    font-size: 1EM;
    border: solid 1px grey;
    border-radius: 25px;
    font-weight: 500;
    text-shadow: none;
    text-transform: uppercase;
    padding: 10px 54px;
    line-height: 110%;
    letter-spacing: .04em;
    box-shadow: none;
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
}
.vc_custom_1472116665113,.vc_custom_1472116695892,.vc_custom_1472116651968,.vc_custom_1472116687361 {
    margin-left: 1% !important;
}
.moving_cart{
	    position: absolute!important;
    right: 4px!important;
    top: auto!important;
    bottom: 0px!important;
}
.woocommerce ul.products li.product h3, .woocommerce-page ul.products li.product h3 {
    font-size: 15PX;
    font-weight: 600;
    padding: .5em 0;
}
.wpb_text_column.wpb_content_element.pricing_roadyo_titles h2 {
    font-size: 24px;
    font-weight: 600;
}
button.pricing_buttonold:hover {
    background: #92cdff;;
    transition: all 300ms ease-in-out;
	color:#fff;
}
.vc_custom_1474710736534{margin-left:0!important}
div#cart_ht {
    height: 50vh!important;
    overflow-y: auto;
    overflow-x: hidden;
}
body {
    scrollbar-face-color: #b46868;
}
tr.cart_item
</style>
