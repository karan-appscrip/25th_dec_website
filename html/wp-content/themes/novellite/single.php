<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage NovelLite
 * @since NovelLite 1.0
 */
?>
<?php get_header(); ?>
<div style="height:100px;background:transparent"></div>
<div class="page-container" style="position:relative!important">
     <div class="container">
        <div class="row">
		<div class="col-md-12">
            <div class="page-content">
                 <div class="col-md-9">
                    <div class="content-bar">  
                        <!-- Start the Loop. -->
                        <?php
                        global $post;
                        if (have_posts()) : while (have_posts()) : the_post();
                                ?>
						<!--Start post-->
						 <?php $format = get_post_format($post->ID); ?>	
						 <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="post single">			
						<h1 class="post_title"><?php the_title(); ?></h1>  
					   <ul class="post_meta">				
						<li class="posted_by vcard author"><span></span><?php the_author_posts_link(); ?></li>
						<li class="posted_on date updated"><i class="fa fa-calendar-o"></i><span></span><?php echo get_the_time('M, d, Y') ?></li>   
						<?php if ( is_singular( 'post' ) ) { ?>
						<li class="posted_in"><i class="fa fa-folder-o"></i><span></span><?php the_category(', '); ?></li>
						<?php } ?>
						<li class="post_comment"><i class="fa fa-comments-o"></i><?php comments_popup_link(NO_CMNT, ONE_CMNT, '% ' . CMNT); ?></li>
						<li class="format_sign"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>" ><span class="post_format">
					 <?php if ($format == 'video')  { ?>
					 <i class="fa fa-video-camera"></i>
					 <?php } elseif ($format == 'gallery')  { ?>
					 <i class="fa fa-camera"></i>
					 <?php } elseif ($format == 'image')  { ?>
					 <i class="fa fa-picture-o"></i>
					 <?php } elseif ($format == 'quote')  { ?>
					 <i class="fa fa-quote-left"></i>
					 <?php } elseif ($format == 'link')  { ?>
					 <i class="fa fa-link"></i>
					 <?php } else { ?>
					 <i class="fa fa-file-text"></i>
					 
					 <?php } ?>
					 </span></a></li>
						</ul>
					<div class="post_content">
						<?php
						the_content();
						$Video_embed = get_post_meta($post->ID, 'video_url', true);
						echo $Video_embed;
						?>
					</div>
					<div class="clear"></div>
                                </div>
                                </div>
                                <?php
                            endwhile;
                        else:
                            ?>
                            <div class="post">
                                <p>
                                    <?php echo SORRY_NO_POST_MATCHED; ?>
                                </p>
                            </div>
                        <?php endif; ?>
                        <div class="clear"></div>
                        <nav id="nav-single"> <span class="nav-previous">
                                <?php previous_post_link('&laquo; %link'); ?>
                            </span> <span class="nav-next">
                                <?php next_post_link('%link &raquo;'); ?>
                            </span> </nav>
                        <div class="clear"></div>
                        <?php wp_link_pages(array('before' => '<div class="clear"></div><div class="page-link"><span>' . PAGES_COLON . '</span>', 'after' => '</div>')); ?>
                        <!--Start Comment box-->
                        <?php comments_template(); ?>
                        <!--End Comment box-->
                        <!--End post-->
                    </div>
                </div>
				 <div class="col-md-3 nopadding">
				<!--Start Sidebar-->
				<?php get_sidebar(); ?>
				<!--End Sidebar-->
				</div> 
            </div>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<style>
ul#menu-main li a {
    color: black;
}
li#menu-item-3976 a {
    color: greenyellow!important;
}
ul#menu-main li a:hover{background:transparent!important}
.nopadding{padding:0px;position:static!important}
body{text-align:justify}
.moving_form_pos {
        position: absolute!important;
    top: auto!important;
    bottom: 0px!important;
}
.moving_cart_form.moving_form {
    position: fixed;
    top: 132px;
}
</style>
<!--<script>
 jQuery(document).ready(function($) {	
 var k = $(document).height(); 
	  var y = $('.outer-footer').height()*2;
	  var ht = k-y;
	$(window).scroll(function() {
		   if ($(this).scrollTop()>70){
			$('.moving_cart_form').addClass('moving_form');
}          if($(this).scrollTop()<70){$('.moving_cart_form').removeClass('moving_form');}
           if($(this).scrollTop()>k-y){$('.moving_cart_form').removeClass('moving_form');
		   $('.moving_cart_form').addClass('moving_form_pos');
		   }
		   else{
			   $('.moving_cart_form').removeClass('moving_form_pos');
			   $('.moving_cart_form').addClass('moving_form');
		   }
	  });
	  });
</script> -->
<?php get_footer(); ?>