<?php
/*
  Template Name: SALESFORCE  
 */
?>
<?php get_header(); ?>
 



<!-- SWEET ALERT FOR POPUP SWAL -->
<script src="https://unpkg.com/sweetalert2@7.17.0/dist/sweetalert2.all.js"></script>


 <style>
@font-face {
font-family: "Circular Air";
src: url("https://s3-ap-southeast-1.amazonaws.com/appscrip/Salesforce/CIRCULARAIR-LIGHT.OTF");
}

h1, h2, h3, h4, h5, h6, p, a, span, strong{font-family: Circular Air;}
.swal2-popup .swal2-title {
    display: block;
    position: relative;
    max-width: 100%;
    margin: 0 0 0.4em;
    padding: 0;
    color: #484848;
    font-size: 1.875em;
    font-weight: 600;
    text-align: center;
    text-transform: none;
    font-family: circular air;
    word-wrap: break-word;
}
.swal2-popup #swal2-content {
    text-align: center;
    font-size: 17px;
    font-weight: normal;
    color: #000;
    margin: 2% 0;
    line-height: 28px;
}
.swal2-popup {
    display: none;
    position: relative;
    flex-direction: column;
    justify-content: center;
    width: 40em;
    max-width: 100%;
    padding: 3.25em;
    border-radius: 0.3125em;
    background: #fff;
    font-family: inherit;
    font-size: 1.5rem;
    box-sizing: border-box;
}
.swal2-popup #swal2-content ul li { text-align: left;}

textarea.wpcf7-form-control.wpcf7-textarea.commonformsalesforce {
    border: solid 1px #ccc1c1;
    padding: 15px;
    font-size: 17px;
    height: 68px;
    width: 80%;
    margin-left: 10%;
    margin-bottom: 10px;
    border-radius: 10px;
    outline: 0px;
}
input.wpcf7-form-control.wpcf7-submit.btnSubmit1 {
    width: 100%;
    text-align: center!important;
    background: #4386ff;
    border: 0px;
    color: #fff!important;
    font-size: 16px;
    border-radius: 5px;
    padding: 2%;
    outline: 0px;
}
input.wpcf7-form-control.wpcf7-quiz {
    width: 80%;
    margin: 0 0 4% 12%;
}
span.wpcf7-quiz-label {
    margin: 0 0 0 11%;
}

@media (min-width:1281px) { 
    /* hi-res laptops and desktops */
    
    /* bannersection */
    #bannersection { background: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Group+1627.png');}
    .banner { margin: 15% 0 10% 8%; width: 50%; color: white;}
    #bannersection h1{ padding: 0% 0 1% 0%; font-size: 45px; }
    #bannersection p { letter-spacing: 0.5px; font-size: 18px;}

    /* balancesection */
    div#balancesection {
    background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+47.png');
    background-size: 10%; background-repeat: no-repeat; background-position: 91% 10%;
    width: 90%; border-radius: 22px; background-color: white;
    -webkit-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    -moz-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    margin: -5% 0 0 5%;}
    div#balancesection img {width: 30%; margin: -7% 0 0 5%;}
    .balance {margin: -4% 0 0 2%;}
    .balance h2 {text-align: right; color: #484848; font-size: 40px; line-height: 55px;}

    /* processection */
    #processection{margin:3% 0 0 0 ; background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+18.png');
    background-size: 10%; background-repeat: no-repeat; background-position: 3% 0%; }
    #processection .vc_column-inner { width: 100%!important; padding-left: 15px!important ; }
    .process {margin: 0 0 0 8%;}
    .process h2{color: #484848; font-size: 40px; line-height: 55px;}
    .process p {color: #484848; width:60%; font-size: 18px;}
    .process2{margin:3% 0 0 0 ; background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+25.png');
    background-size: 25%; background-repeat: no-repeat; background-position: 101% 110%; }
    .process2 img {width:90%;}

    /* Services */
    #servicesection{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+22.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+24.png');
    background-size: 7%, 25%; background-repeat: no-repeat; background-position: 96% 8%, 0 100%; }
    #servicesection .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .services { color: #FFFFFF; text-align: right; padding: 2% 8% 0 25%;}
    .services h2 {padding-bottom:1%; font-size:40px;}
    .services p {font-size:18px;}
    .businessrow {display: flex; margin: 0 0 0 8%;}
    .business { background-color: white; width: 21%; text-align: center; border-radius: 5px; padding: 2% 3%; margin: 1% 1%;}
    .business span { color: #00A1E0;} 
    .business p{font-size: 16px;}

    /* Focus areas */
    #focusarea{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+29.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+35.png');
    background-size: 7%, 24%; background-repeat: no-repeat; background-position: 5% 6%, 97% 92%; }
    #focusarea .vc_column-inner { width: 100%!important; padding-left: 15px!important ;} 
    .focus { padding: 0 8%;}
    .focus h2{ font-size:40px; padding-bottom: 1%;}
    .focus p{ width: 66%; padding-bottom: 1%;font-size: 18px;}
    .focus img {width:75%;}

    /* clientslove */
    #clientslove{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+38.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+40.png');
    background-size: 7%, 26%; background-repeat: no-repeat; background-position: 93% 9%, 1% 98%; }
    #clientslove .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .clientsloves { text-align: right; padding: 0 8% 0 19%;}
    .clientsloves h2 {padding-bottom:1%; font-size:40px; color:white;}
    .clientsloves p {color:white;font-size: 18px;}
    .clientlovepop {display: flex; margin: 0 0 0 8%;}
    .business1 { background-color: white; width: 21%; text-align: center; border-radius: 5px; padding: 2% 3%; margin: 1% 1%;}
    .business1 span { color: #00A1E0;}
    .business1 img {width: 70px; margin: 0 0 5% 0;}
    .business1 p{font-size: 16px;}



    /* Clients */
    #client{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+44.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+46.png');
        background-size: 7%, 18%; background-repeat: no-repeat;  background-position: 4% 13%, 100% 90%;  margin: 2% 0 0 0;}
    #client .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .clients h2{font-size: 40px; padding: 0 0 0 8%;}

    /* Download */
    #download .vc_column-inner { width: 100%!important; padding-left: 15px!important ;     padding-top: 0px;} 
    .downloadrow { margin: 4% 0 0 8%;}
    .downloadbtn{margin-top: -3%; margin-bottom:1%; border-right: 1px solid white;}
    .downloadbtn img { float: left;}
    .downloadbtn p {color: white; padding: 3.5% 0 0 0; font-size: 18px;}
    button.formbtn {background-color: transparent; border-radius: 38px; height: 60px; color: white; margin: 0% 0 0 0; width: 35%; border: 2px solid white; font-size: 18px;}
    .contactusbtn {margin: -2.5% 0 0 0;}
   


     }



    @media (min-width: 1025px) and (max-width: 1280px) { 
    /* big landscape tablets, laptops, and desktops */ 

     /* bannersection */
    #bannersection { background: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Group+1627.png');    margin-top: -4px;}
    .banner { margin: 10% 0 10% 8%; width: 50%; color: white;}
    #bannersection h1{ font-size: 30px; }
    #bannersection p { letter-spacing: 0.5px; font-size: 18px; line-height: 1.5;}

    /* balancesection */
    div#balancesection {
    background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+47.png');
    background-size: 10%; background-repeat: no-repeat; background-position: 91% 10%;
    width: 90%; border-radius: 22px; background-color: white;
    -webkit-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    -moz-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    margin: -5% 0 0 5%;}
    div#balancesection img {width: 30%; margin: -7% 0 0 5%;}
    .balance {margin: -4% 0 0 2%;}
    .balance h2 {text-align: right; color: #484848; font-size: 29px; line-height: 55px;}

    /* processection */
    #processection{margin:3% 0 0 0 ; background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+18.png');
    background-size: 10%; background-repeat: no-repeat; background-position: 3% 0%; }
    #processection .vc_column-inner { width: 100%!important; padding-left: 15px!important ; }
    .process {margin: 0 0 0 8%;}
    .process h2{color: #484848; font-size: 30px; line-height: 55px;}
    .process p {color: #484848; width: 70%; letter-spacing: 0.5px;  font-size: 18px; line-height: 1.5;}
    .process2{margin:3% 0 0 0 ; background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+25.png');
    background-size: 25%; background-repeat: no-repeat; background-position: 101% 110%; }
    .process2 img {width:90%;}

    /* Services */
    #servicesection{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+22.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+24.png');
    background-size: 7%, 25%; background-repeat: no-repeat; background-position: 96% 8%, 0 100%; }
    #servicesection .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .services { color: #FFFFFF; text-align: right; padding: 2% 8% 0 20%;}
    .services h2 {padding-bottom:1%; font-size:30px;}
    .services p {letter-spacing: 0.5px; font-size: 18px; line-height: 1.5;}
    .businessrow {display: flex; margin: 0 0 0 8%;}
    .business { background-color: white; width: 21%; text-align: center; border-radius: 5px; padding: 2% 3%; margin: 1% 1%;}
    .business span { color: #00A1E0;} 
    .business p{font-size: 16px;}

    /* Focus areas */
    #focusarea{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+29.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+35.png');
    background-size: 7%, 24%; background-repeat: no-repeat; background-position: 5% 6%, 97% 92%; }
    #focusarea .vc_column-inner { width: 100%!important; padding-left: 15px!important ;} 
    .focus { padding: 0 8%;}
    .focus h2{ font-size:30px; padding-bottom: 1%;}
    .focus p{ width: 75%; letter-spacing: 0.5px;  font-size: 18px; line-height: 1.5;  padding-bottom: 1%;}
    .focus img {width:75%;}

    /* clientslove */
    #clientslove{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+38.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+40.png');
    background-size: 7%, 26%; background-repeat: no-repeat; background-position: 93% 9%, 1% 98%; }
    #clientslove .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .clientsloves { text-align: right; padding: 0 8% 0 19%;}
    .clientsloves h2 {padding-bottom:1%; font-size:30px; color:white;}
    .clientsloves p {color: white; font-size: 18px;  letter-spacing: 0.5px; line-height: 1.5;}
    .clientlovepop {display: flex; margin: 0 0 0 8%;}
    .business1 { background-color: white; width: 21%; text-align: center; border-radius: 5px; padding: 2% 3%; margin: 1% 1%;}
    .business1 span { color: #00A1E0;}
    .business1 img {width: 70px; margin: 0 0 5% 0;}
    .business1 p{font-size: 16px;}



    /* Clients */
    #client{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+44.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+46.png');
        background-size: 7%, 18%; background-repeat: no-repeat;  background-position: 4% 13%, 100% 90%;  margin: 2% 0 0 0;}
    #client .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .clients h2{font-size: 30px; padding: 0 0 0 8%;}

    /* Download */
    #download .vc_column-inner { width: 100%!important; padding-left: 15px!important ;     padding-top: 0px;} 
    .downloadrow { margin: 4% 0 0 8%;}
    .downloadbtn{margin-top: -3%; margin-bottom:1%; border-right: 1px solid white;}
    .downloadbtn img { float: left;}
    .downloadbtn p {color: white; padding: 3.5% 0 0 0; font-size: 18px;}
    button.formbtn {background-color: transparent; border-radius: 38px; height: 60px; color: white; margin: 0% 0 0 0; width: 35%; border: 2px solid white; font-size: 18px;}
    .contactusbtn {margin: -2.5% 0 0 0;}



    }



/*  ##Device = Tablets, Ipads (portrait)   ##Screen = B/w 768px to 1024px */
@media (min-width: 768px) and (max-width: 1024px) { 

     /* bannersection */
     #bannersection { background: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Group+1627.png');    margin-top: -4px;}
    .banner { margin: 10% 0 10% 8%; width: 50%; color: white;}
    #bannersection h1{ font-size: 25px; }
    #bannersection p { letter-spacing: 0.5px; font-size: 16px; line-height: 1.5;}

    /* balancesection */
    div#balancesection {
    background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+47.png');
    background-size: 10%; background-repeat: no-repeat; background-position: 91% 10%;
    width: 90%; border-radius: 22px; background-color: white;
    -webkit-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    -moz-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    margin: -5% 0 0 5%;}
    div#balancesection img {width: 30%;  margin-left: auto; margin-right: auto;}
    .balance {margin: -4% 0 0 2%;}
    .balance h2 {text-align: center; color: #484848; font-size: 25px; line-height: 37px;}

    /* processection */
    #processection{margin:3% 0 0 0 ; background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+18.png');
    background-size: 10%; background-repeat: no-repeat; background-position: 3% 0%; }
    #processection .vc_column-inner { width: 100%!important; padding-left: 15px!important ; }
    .process {margin: 0 0 0 8%;}
    .process h2{color: #484848; font-size: 25px; line-height: 55px;}
    .process p {color: #484848; width: 70%; letter-spacing: 0.5px;  font-size: 16px; line-height: 1.5;}
    .process2{margin:3% 0 0 0 ; background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+25.png');
    background-size: 25%; background-repeat: no-repeat; background-position: 101% 110%; }
    .process2 img {width:90%;}

    /* Services */
    #servicesection{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+22.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+24.png');
    background-size: 7%, 25%; background-repeat: no-repeat; background-position: 96% 8%, 0 100%; }
    #servicesection .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .services { color: #FFFFFF; text-align: right; padding: 2% 8% 0 20%;}
    .services h2 {padding-bottom:1%; font-size:25px;}
    .services p {letter-spacing: 0.5px; font-size: 16px; line-height: 1.5;}
    .businessrow {display: flex; margin: 0 0 0 8%;}
    .business { background-color: white; width: 21%; text-align: center; border-radius: 5px; padding: 2% 3%; margin: 1% 1%;}
    .business span { color: #00A1E0;} 
    .business p{font-size: 16px;}

    /* Focus areas */
    #focusarea{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+29.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+35.png');
    background-size: 7%, 24%; background-repeat: no-repeat; background-position: 5% 6%, 97% 92%; }
    #focusarea .vc_column-inner { width: 100%!important; padding-left: 15px!important ;} 
    .focus { padding: 0 8%;}
    .focus h2{ font-size:25px; padding-bottom: 1%;}
    .focus p{ width: 75%; letter-spacing: 0.5px;  font-size: 16px; line-height: 1.5;  padding-bottom: 1%;}
    .focus img {width:75%;}

    /* clientslove */
    #clientslove{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+38.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+40.png');
    background-size: 7%, 26%; background-repeat: no-repeat; background-position: 93% 9%, 1% 98%; }
    #clientslove .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .clientsloves { text-align: right; padding: 0 8% 0 19%;}
    .clientsloves h2 {padding-bottom:1%; font-size:25px; color:white;}
    .clientsloves p {color: white; font-size: 16px;  letter-spacing: 0.5px; line-height: 1.5;}
    .clientlovepop {display: flex; margin: 0 0 0 8%;}
    .business1 { background-color: white; width: 21%; text-align: center; border-radius: 5px; padding: 2% 3%; margin: 1% 1%;}
    .business1 span { color: #00A1E0;}
    .business1 img {width: 70px; margin: 0 0 5% 0;}
    .business1 p{font-size: 16px;}



    /* Clients */
    #client{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+44.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+46.png');
        background-size: 7%, 18%; background-repeat: no-repeat;  background-position: 4% 13%, 100% 90%;  margin: 2% 0 0 0;}
    #client .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .clients h2{font-size: 25px; padding: 0 0 0 8%;}

    /* Download */
    #download .vc_column-inner { width: 100%!important; padding-left: 15px!important ;     padding-top: 0px;} 
    .downloadrow { margin: 4% 0 0 8%;}
    .downloadbtn{    margin-top: -3%; margin-left: 8%; margin-bottom: 7%;}
    .downloadbtn img { float: left;}
    .downloadbtn p {color: white; padding: 3.5% 0 0 0; font-size: 18px;}
    button.formbtn {background-color: transparent; border-radius: 38px; height: 60px; color: white; margin: 0% 0 0 0; width: 35%; border: 2px solid white; font-size: 18px;}
    .contactusbtn {margin: -2.5% 0 0 0;}


 }

/* ##Device = Low Resolution Tablets, Mobiles (Landscape)  ##Screen = B/w 481px to 767px */
@media (min-width: 481px) and (max-width: 767px) { 

     /* bannersection */
     #bannersection { background: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Group+1627.png');    margin-top: -4px;}
    .banner { margin: 10% 0 10% 10%; color: white;}
    #bannersection h1{ font-size: 22px; }
    #bannersection p { letter-spacing: 0.5px; font-size: 15px; line-height: 1.5;}

    /* balancesection */
    div#balancesection {
    background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+47.png');
    background-size: 10%; background-repeat: no-repeat; background-position: 91% 10%;
    width: 90%; border-radius: 22px; background-color: white;
    -webkit-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    -moz-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    margin: -5% 0 0 5%;}
    div#balancesection img {width:40%;     margin-left: auto; margin-right: auto;}
    .balance {margin: -4% 0 0 2%;}
    .balance h2 {text-align: center; color: #484848; font-size: 20px; line-height: 30px;}

    /* processection */
    #processection{margin:3% 0 0 0 ; background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+18.png');
    background-size: 10%; background-repeat: no-repeat; background-position: 3% 0%; }
    #processection .vc_column-inner { width: 100%!important; padding-left: 15px!important ; }
    .process {margin: 0 0 0 6%;}
    .process h2{color: #484848; font-size: 20px; line-height: 30px; text-align:center;}
    .process p {color: #484848; letter-spacing: 0.5px; font-size: 15px; text-align: center; padding: 0 3% 0 1%; line-height: 1.5;}
    .process2{margin:3% 0 0 0 ; background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+25.png');
    background-size: 25%; background-repeat: no-repeat; background-position: 101% 110%; }
    .process2 img {width:50%;}

    /* Services */
    #servicesection{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+22.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+24.png');
    background-size: 7%, 25%; background-repeat: no-repeat; background-position: 96% 8%, 0 100%; }
    #servicesection .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .services { color: #FFFFFF; text-align: right; padding: 2% 8% 0 8%;}
    .services h2 {padding-bottom:1%; font-size: 20px; text-align: center;}
    .services p {letter-spacing: 0.5px; font-size: 15px; text-align: center; line-height: 1.5;}
    .businessrow { margin: 0 0 0 8%;}
    .business { background-color: white; width: 90%; text-align: center; border-radius: 5px; padding: 2% 3%; margin: 3% 1%;}
    .business span { color: #00A1E0;} 
    .business p{font-size: 15px;}

    /* Focus areas */
    #focusarea{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+29.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+35.png');
    background-size: 7%, 24%; background-repeat: no-repeat; background-position: 5% 6%, 97% 92%; }
    #focusarea .vc_column-inner { width: 100%!important; padding-left: 15px!important ;} 
    .focus { padding: 0 8%;}
    .focus h2{     font-size: 20px; text-align: center; padding-bottom: 1%;}
    .focus p{ letter-spacing: 0.5px; font-size: 16px; text-align: center; line-height: 1.5;}
    .focus img {width:50%;}

    /* clientslove */
    #clientslove{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+38.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+40.png');
    background-size: 7%, 26%; background-repeat: no-repeat; background-position: 93% 9%, 1% 98%; }
    #clientslove .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .clientsloves { text-align: center; padding: 0 8% 0 8%;}
    .clientsloves h2 {padding-bottom:1%; font-size:20px; text-align:center; color:white;}
    .clientsloves p {color: white; font-size: 16px;  letter-spacing: 0.5px; line-height: 1.5;}
    .clientlovepop { margin: 0 0 0 8%;}
    .business1 { background-color: white; width: 90%; border-radius: 5px; padding: 2% 3%; margin: 1% 1%;}
    .business1 span { color: #00A1E0;}
    .business1 img {width: 70px; margin: 0 0 5% 0;}
    .business1 p{font-size: 15px;     text-align: center;}



    /* Clients */
    #client{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+44.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+46.png');
        background-size: 7%, 18%; background-repeat: no-repeat;  background-position: 4% 13%, 100% 90%;  margin: 2% 0 0 0;}
    #client .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .clients h2{font-size: 25px; padding: 0 0 0 8%;}

    /* Download */
    #download .vc_column-inner { width: 100%!important; padding-left: 15px!important ;     padding-top: 0px;} 
    .downloadrow { margin: 4% 0 0 8%;}
    .downloadbtn{    margin-top: -3%;  margin-bottom: 7%;}
    .downloadbtn img { float: left;}
    .downloadbtn p {color: white; padding: 3.5% 0 0 0; font-size: 18px;}
    button.formbtn {background-color: transparent; border-radius: 38px; height: 60px; color: white; margin: 4% 0 0 0; width: 50%; border: 2px solid white; font-size: 18px;}
    .contactusbtn {margin: -2.5% 0 0 0;}

 }

/* ##Device = Most of the Smartphones Mobiles (Portrait)  ##Screen = B/w 320px to 479px  */
@media (min-width: 320px) and (max-width: 480px) { 
    
 /* bannersection */
 #bannersection { background: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Group+1627.png');    margin-top: -4px;}
    .banner { margin: 15% 0 0% 14%; color: white;}
    #bannersection h1{ font-size: 22px; }
    #bannersection p { letter-spacing: 0.5px; font-size: 15px; line-height: 1.5;}

    /* balancesection */
    #balancesection .vc_column-inner { width: 100%!important; padding-left: 15px!important ; }
    div#balancesection {
    /* background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+47.png');
    background-size: 10%; background-repeat: no-repeat; background-position: 91% 10%; */
    
    width: 90%; border-radius: 22px; background-color: white;
    -webkit-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    -moz-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    box-shadow: 0 0px 0px rgba(0, 0, 0, 0.3), 0 0 15px rgba(0, 0, 0, 0.1) inset;
    margin: -5% 0 0 5%;}
    div#balancesection img {width:40%;        margin-left: auto; margin-right: auto;}
    .balance {margin: -4% 0 0 2%;}
    .balance h2 {text-align: center; color: #484848; font-size: 20px; line-height: 30px;}

    /* processection */
    #processection{margin:3% 0 0 0 ; background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+18.png');
    background-size: 10%; background-repeat: no-repeat; background-position: 3% 0%; }
    #processection .vc_column-inner { width: 100%!important; padding-left: 15px!important ; }
    .process {margin: 0 0 0 6%;}
    .process h2{color: #484848; font-size: 20px; line-height: 30px; text-align:center;}
    .process p {color: #484848; letter-spacing: 0.5px; font-size: 15px; text-align: center; padding: 0 3% 0 1%; line-height: 1.5;}
    .process2{margin:3% 0 0 0 ; background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+25.png');
    background-size: 25%; background-repeat: no-repeat; background-position: 101% 110%; }
    .process2 img {width:50%;}

    /* Services */
    #servicesection{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+22.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+24.png');
    background-size: 7%, 25%; background-repeat: no-repeat; background-position: 96% 8%, 0 100%; }
    #servicesection .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .services { color: #FFFFFF; text-align: right; padding: 2% 8% 0 10%;}
    .services h2 {padding-bottom:1%; font-size: 20px; text-align: center;}
    .services p {letter-spacing: 0.5px; font-size: 15px; text-align: center; line-height: 1.5;}
    .businessrow { margin: 0 0 0 8%;}
    .business { background-color: white; width: 90%; text-align: center; border-radius: 5px; padding: 2% 3%; margin: 3% 1%;}
    .business span { color: #00A1E0;} 
    .business p{font-size: 15px;}

    /* Focus areas */
    #focusarea{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+29.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+35.png');
    background-size: 7%, 24%; background-repeat: no-repeat; background-position: 5% 6%, 97% 92%; }
    #focusarea .vc_column-inner { width: 100%!important; padding-left: 15px!important ;} 
    .focus { padding: 0 8% 0 10%;}
    .focus h2{     font-size: 20px; text-align: center; padding-bottom: 1%;}
    .focus p{ letter-spacing: 0.5px; font-size: 16px; text-align: center; line-height: 1.5;}
    .focus img {width:75%;}

    /* clientslove */
    #clientslove{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+38.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+40.png');
    background-size: 7%, 26%; background-repeat: no-repeat; background-position: 93% 9%, 1% 98%; }
    #clientslove .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .clientsloves { text-align: center; padding: 0 8% 0 10%;}
    .clientsloves h2 {padding-bottom:1%; font-size:20px; text-align:center; color:white;}
    .clientsloves p {color: white; font-size: 16px;  letter-spacing: 0.5px; line-height: 1.5;}
    .clientlovepop { margin: 0 0 0 8%;}
    .business1 { background-color: white; width: 90%; border-radius: 5px; padding: 2% 3%; margin: 1% 1%;    text-align: center;}
    .business1 span { color: #00A1E0;}
    .business1 img {width: 40px; margin: 4% 0 5% 0;}
    .business1 p{font-size: 15px;  }



    /* Clients */
    #client{ background-image: url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+44.png'), url('https://dji16gnzvk5qv.cloudfront.net/Salesforce/Mask+Group+46.png');
        background-size: 7%, 18%; background-repeat: no-repeat;  background-position: 4% 13%, 100% 90%;  margin: 2% 0 0 0;}
    #client .vc_column-inner { width: 100%!important; padding-left: 15px!important ;}
    .clients h2{font-size: 25px; padding: 0 0 0 8%;}

    /* Download */
    #download .vc_column-inner { width: 100%!important; padding-left: 15px!important ;     padding-top: 0px;} 
    .downloadrow { margin: 4% 8% 0 8%;}
    .downloadbtn{   margin-top: -3%; margin-bottom: 7%;  padding-bottom: 9%; border-bottom: 1px solid white;}
    .downloadbtn img { float: left;}
    .downloadbtn p {color: white; padding: 3.5% 0 0 0; font-size: 14px;}
    button.formbtn {background-color: transparent; border-radius: 38px; height: 60px; color: white; margin: 6% 0 0 0; width: 80%; border: 2px solid white; font-size: 16px;}

}




 </style>

<div class="page-container"> 
<div class="container-fluid" id="fullpage" style="padding-right: 0px; 
 padding-left: 0px; ">
    <div class="row">
        <div class="page-content">
         <div class="col-md-12">
          <div class="fullwidth">           
        <?php if (have_posts()) : the_post(); ?>
            <?php the_content(); ?>	
        <?php endif; ?>	  
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
</div>
</div>



<?php get_template_part( 'template/section_countactus');  ?>
<?php get_footer(); ?>