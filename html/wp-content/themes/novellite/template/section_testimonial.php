    <!-- *** Testimonial Slider Starts *** -->
<?php if (get_theme_mod('testimonial_parallax_image','') != '') { ?>
<section class="testimonial-wrapper" id="section2" data-type="background" style="background: url(https://s3-ap-southeast-1.amazonaws.com/appscrip/services/fixed+background.png); ?>) center repeat fixed;">
<?php } else { ?>
 <section class="testimonial-wrapper" id="section2">
 <?php } ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1 id="paltxt1" class="wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s;font-weight:bolder"><strong>WE BUILD APRENUERS!</strong></h1>
	  <p id="paltxt2" class="wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s;">Appscrip brings all the popular business models like Social , On-demand , shopping , discovery , chat and booking at one place so you spend less time in organizing your technology and more time delighting your customers and growing your business.
We’ve mastered the process of developing mobile first products for these business models and have spent countless hours understanding and dissecting each business model to create these good looking scalable mobile apps that can accelerate your business idea multi-fold and help you build the NEXT BIG WHAT!</p>
       <a href="#" id="ideabut" class="wow fadeInLeft" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s;">DROP YOUR BILLION DOLLAR IDEA HERE</a>
            </div>
        </div>
    </div>
</section>