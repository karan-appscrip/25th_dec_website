<?php
$nameError = '';
$emailError = '';
$commentError = '';
if (isset($_POST['submitted'])) {
    if (trim($_POST['contactName']) === '') {
        $nameError = 'Please enter your name.';
        $hasError = true;
    } else {
        $name = trim($_POST['contactName']);
    }
    if (trim($_POST['email']) === '') {
        $emailError = 'Please enter your email address.';
        $hasError = true;
   } else if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $_POST['email'])) {
        $emailError = 'You entered an invalid email address.';
        $hasError = true;
    } else {
        $email = trim($_POST['email']);
    }
    if (trim($_POST['comments']) === '') {
        $commentError = 'Please enter a message.';
        $hasError = true;
    } else {
        if (function_exists('stripslashes')) {
            $comments = stripslashes(trim($_POST['comments']));
        } else {
            $comments = trim($_POST['comments']);
        }
    }
    if (!isset($hasError)) {
        $emailTo = get_option('admin_email');
        if (!isset($emailTo) || ($emailTo == '')) {
           $emailTo = get_theme_mod('cf_email_',get_option('admin_email'));
        }
        $subject = $name.'('.$email.')';
        $body = "Name: $name \n\nEmail: $email \n\nComments: $comments";
        $headers= "Reply-To: ".$name." <".$email.">";
        mail($emailTo, $subject, $body,$headers);
        $emailSent = true;
    }
}
?>
<!-- blog title -->
<!-- blog title ends -->
<?php if (get_theme_mod('cf_image','') != '') { ?>
<section id="section5" class="contact_section" style="background:#fff">
<?php } else { ?>
<section id="section5" class="contact_section">
 <?php } ?>

        <div class="container">
            <div class="row">
                <div class="col-lg-offset-2 col-lg-8 cont_sec">
                   <h3 class="section-heading">CONNECT WITH US</h3>
                   <?php echo do_shortcode( '[contact-form-7 id="16139" title="common form new Home page"]' ); ?>
               </div>
          </div>
        </div>
    </section>

<style>
h3.section-heading{
margin-top: 36px;
    margin-bottom: 15px;
    font-size: 40px;
    font-weight: 800;
    text-align: center;
}
.animated_head_home h3 {
    margin-top: 2%;
    margin-bottom: 1%!important;
    font-weight: 500;
    font-size: 30px;
    font-family: raleway!important;
}
	
	input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform1, select.wpcf7-form-control.wpcf7-select.wpcf7-validates-as-required.commonform1, input.wpcf7-form-control.wpcf7-quiz.commonform1 {
    border: solid 1px #CECECE;
    padding: 15px;
    font-size: 17px;
    width: 80%;
    margin-left: 10%;
    margin-bottom: 10px;
    border-top-left-radius: 30px;
    height: 50px;
    outline: 0px;
}
	textarea.wpcf7-form-control.wpcf7-textarea.commonform1 {
    width: 80%;
    margin-left: 10%;
    height: 50px;
    border-top-left-radius: 30px;
    border: solid 1px #ccc1c1;
    outline: 0px;
    padding-left: 20px;
}
	input.wpcf7-form-control.wpcf7-submit.btnSubmit1 {
    margin: 3% 0 0 5.5%;
    margin: 3% 10% 3% 0;
    background-color: #0088FC;
    color: white;
    float: right;
    font-family: 'Hind', sans-serif;
    font-weight: 700;
    border: 1px solid #0088FC;
    padding: 0.5% 8%;
    font-size: 18px;
    -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc;
    box-shadow: 0 8px 6px -6px #0088fc;
}
	
	span.wpcf7-quiz-label {
    margin: 0 0 0 11%;
		font-size: 14px;
    top: 0!important;
}
</style>