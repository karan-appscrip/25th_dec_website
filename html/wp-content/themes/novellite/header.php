<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<link rel="icon" href="https://www.appscrip.com/wp-content/uploads/2018/07/appscrip-cropped-favicon-32x32.png?">
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,800,900,700,300' rel='stylesheet' type='text/css'>

<!-- Global site tag (gtag.js) - Google Ads: 867220945 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-867220945"></script>
<script>

jQuery( document ).ready(function() {
setTimeout(function(){	jQuery("nav").addClass('affix-top');
	jQuery("nav").removeClass('affix');
console.log('affix-top ready');},3500)
});
</script>


<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-867220945');
</script>

        <meta charset="<?php bloginfo('charset'); ?>" />
        <?php if ( ! function_exists( '_wp_render_title_tag' ) ) :
   function novellite_render_title() {
?>
prefix="og:https://www.appscrip.com/ios-android-clone-scripts/"
<title><?php wp_title( '-', true, 'right' ); ?></title>

<?php echo do_shortcode('[lsphe-header]' ); ?>
<?php
   }
   add_action( 'wp_head', 'novellite_render_title' );
endif; ?> 	
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<meta name="theme-color" content="#0065e0">
<meta name="baidu-site-verification" content="UqiH4Oji7E" />
<meta name = "naver-site-verification" content = "987e02ee4fb833c050d1e169d41f3351977561b1" />

<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#0065e0">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        			

<!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KV3HNZ6');</script>
<!-- End Google Tag Manager -->
		
		
		
		<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '415662745953009');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=415662745953009&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
		
<!-- Mailchimp Code -->		
<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/54d9589594854b054b520363d/e26cce634fe4cf5b308f9b42e.js");</script>
<!-- End Mailchimp Code -->

         <!-- <script src="//spdrjs-13d1.kxcdn.com/speeder.js"></script>
  <script>speeder('3e3ec733','384');</script>-->
        <?php wp_head(); ?>
		<meta name="google-site-verification" content="QYcHuiaCD7ZWvVxX2O-ore3BS1hxQuVp8x_Tp6D2AJI"/>
		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-92821595-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
			
			
	gtag('config', 'AW-867220945');		

</script>
		<!--script for google recaptcha -->
<script src="https://www.google.com/recaptcha/api.js"></script>
		
<style>
#headerBannerTitle {
    height: 50px;
    background-image: url(https://www.appscrip.com/wp-content/uploads/2019/12/header.png);
    background-size: cover;
    font-size: 20px!important;
    color: #1541a1;
    animation: neonBlur 16s infinite;
    font-weight: 300;
    font-size: 30px;
    text-align: center;
    /* background: black; */
    font-weight: 600;
    letter-spacing: 1px;
    padding-top: 10px;
}
#headerBannerTitle > span:nth-child(1) {
  opacity: .5;
  font-weight: 800;
}
#headerBannerTitle > span:nth-child(2) {
  animation: neonBlink 5s infinite;
}
#headerBannerTitle > span:nth-child(3) {
  opacity: .4;
}
#headerBannerTitle > span:nth-child(4) {
  animation: neonBlink 2s infinite;
}

@keyframes neonBlink {
  0% { opacity: 1; }
  10% { opacity: .6; }
  12% { opacity: 1; }
  15% { opacity: .4; }
  17% { opacity: 1; }
  18% { opacity: .3; }
  19% { opacity: 1; }
  89% { opacity: 1; }
  91% { opacity: .7; }
  94% { opacity: 1; }
  100% { opacity: 1; }
}

@keyframes neonBlur {
  0% { text-shadow: 0px 0px 25px #FF0080; }
  50% { text-shadow: 0px 0px 75px #FF0080; filter: blur(0.5px) }
  100% { text-shadow: 0px 0px 25px #FF0080; }
}

</style>

		</head>  

	<body id="page-top"  data-spy="scroll" data-target=".navbar" data-offset="800" <?php body_class('index'); ?>>		
    <!-- Navigation -->




<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KV3HNZ6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <nav class="navbar navbar-default navbar-fixed-top">
	<div class="header_container">
        <div class="container-fluid">		
		<div class="row">
              <div id="headerBannerTitle" class="hidden-sm" style="display:none">

  <span></span>
 Appscrip is coming to Amsterdam  &nbsp; <a href="http://bit.ly/2r6o2Bf" target="_blank"><span style="color: #dd0a3a;
    border: 2px solid #0032a0;
    padding: 6px 19px;
    font-size: 16px;"> Meet Us </span></a>
</div>
            <a href="#" id="invisible_pop" style="display:none" onclick="return ulp_open('IjzhzJswyYdv8ryb');">Raise the popup</a>
            <!-- Brand and toggle get grouped for better mobile display -->
			 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 ">

            <div class="navbar-header page-scroll">               
				 <?php
              if(get_theme_mod( 'logo_upload')!=''){?>
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="/wp-content/uploads/2019/03/Group-4612.png" alt="logo" height="" width="200"></a>
              <?php }else{ ?>
              <h1><a href="<?php echo home_url('/'); ?>"><?php bloginfo('name'); ?></a></h1>
              <p><?php bloginfo('description'); ?></p>
              <?php } ?>     
				</div>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				</div>
				 
				
				 <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
				 	 <div class="dropdown-content" style="display:none;" id="sub-menu-products">
      <div class="row megamenudiv">
        <div class="column">
          <h3 class="submenuhead">Transportation Solutions</h3>
          <a href="/uber-clone-script-taxi-app-white-label-online-taxi-dispatch-software/">Taxi/Ride-Hailing | Karry</a>
          <a href="/uber-for-couriers-services-for-on-demand-delivery/">Courier Delivery | Shypr</a>
          <a href="/freight-broker-software-freight-finder-app-like-uship-convoy-uber-freight/">Freight-Forwarding | Loademup</a>
          <a href="/Uber-for-trucks-convoy-clone-cargomatic-clone-truckerpath-clone-goshare-clone-doft-clone/">Load-Hauling | Truckr</a>
          <a href="/gojek-clone-script-transport-booking-software/">All-In-One | KarryX</a>
		  <a href="/uber-for-kids-app-kid-transportation-service-software-kid-shuttle-service-solution/">Kid Taxi Services | Kidzup</a>

	      

          <h3 class="submenuhead">Booking & E-Commerce Solutions</h3>
          <a href="/social-ecommerce-script-best-etsy-clone-software-social-shopping-script/">B2C & C2C E-Commerce | Shopr</a>
          <a href="/offerup-clone-script-buy-and-sell-marketplace-script/">C2C Marketplace E-Commerce | LeOffer</a>
          <a href="/airbnb-clone-script-source-code-ios-android/">Hotel/Room Booking | GetBnB</a>
          <a href="/opentable-clone-restaurant-bookings-app/">Restaurant Table Booking | Opentable</a>

          <h3 class="submenuhead">Chat & Social Media Solutions</h3>
          <a href="/white-label-chat-software-for-business-and-personal-chat-app-script/">Chat Application | Hola</a>
          <a href="/tik-tok-clone-dubsmash-video-selfie-clone-script/">Social Video/Audio Dubbing Media Sharing | Dub.ly</a>

        </div>
        <div class="column">
          <h3 class="submenuhead">Service Booking Solutions</h3>
          <a href="/uber-for-x-platform-on-demand-services-app-software-solution-gotasker/">On-Demand Services | GoTaskey</a>
          <a href="/taskrabbit-clone-script-urbanclap-like-on-demand-app/">On-Demand Services(Job Bidding) | ServiceGenie</a>
          <a href="/uber-for-laundry-on-demand-laundry-delivery-service-like-washio-zipjet-rinse-software/">Laundry Services | Laundra</a>
          <a href="/uber-for-maids-on-demand-house-cleaning-app/">Cleaning Services | Clean.it</a>
          <a href="/uber-for-lawn-care-yard-work-services-app/">Lawn Care/Yard Maintenance | Mow.it</a>
          <a href="/uber-for-babysitters-child-care-booking-software-sittr-urbansitter-clone/">Babysitting/Nanny Services | Crèche</a>
          <a href="/designated-driver-software-solution-designated-driver-app-like-ddvip-sobrio-ddod-ivalet/">Chauffer/Valet Services | iValet</a>
          <a href="/uber-for-doctors-app-on-demand-healthcare-software-solution/">Healthcare Services | DruidX</a>
	 
          
		
		  <h3 class="submenuhead">Food Pickup & Delivery Solutions</h3>
          <a href="/postmates-clone-on-demand-delivery/">Food Ordering & Delivery | iDeliver</a>
          <a href="/grocery-shopping-software-grocery-delivery-business-software/">Multi-Store Grocery Pickup & Delivery | Grocer</a>
          <a href="/mutlivendor-delivery-business-software-rappi-clone/">Pickup & Deliver Anything | DelivX</a>
		  <a href="/cannabis-delivery-service-software/">Cannabis Dispensary Delivery | BongMe</a>
		  <a href="/medical-shop-software-online-pharmacy-delivery/">Medicine Delivery | Med.Me</a>
		  <a href="/fuel-delivery-software/">Fuel Delivery | Fuel.Up</a>


        </div>
        <div class="column">
          <h3 class="submenuhead">Clone Apps</h3>
          <a href="/uber-clone-script-taxi-app-white-label-online-taxi-dispatch-software/">Uber/Lyft Clone</a>
          <a href="/gojek-clone-script-services-booking/">Go-Jek Clone</a>
          <a href="/postmates-clone-on-demand-delivery/">Postmates/Deliveroo Clone</a>
          <a href="/grocery-shopping-software-grocery-delivery-business-software/">Instacart/Grofers Clone</a>
          <a href="/social-ecommerce-script-best-etsy-clone-software-social-shopping-script/">Fancy/Etsy Clone</a>
          <a href="/offerup-clone-script-buy-and-sell-marketplace-script/">Offerup/LetGo Clone</a>
          <a href="/dating-software-for-online-dating/">Tinder/Grindr Clone</a>
          <a href="/white-label-chat-software-for-business-and-personal-chat-app-script/">Whatsapp/WeChat Clone</a>
          <a href="/instagram-clone-ios-web-android/">Instagram Clone</a>
          <a href="/tik-tok-clone-dubsmash-video-selfie-clone-script/">TikTok/Dubsmash Clone</a>
          <a href="/uber-for-laundry-on-demand-laundry-delivery-service-like-washio-zipjet-rinse-software/">Washio/Zipjet Clone</a>
          <a href="/taskrabbit-clone-script-urbanclap-like-on-demand-app/">Taskrabbit/Airtasker Clone</a>
          <a href="/airbnb-clone-script/">Airbnb Clone</a>
          <a href="/mutlivendor-delivery-business-software-rappi-clone/">Rappi Clone</a>
          <a href="/real-estate-listing-app-development/">Zillow/Trulia Clone</a>
        </div>
      </div>
    </div>
				<?php if (is_front_page()) { ?>



				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

   

				<?php NovelLitemenu_frontpage_nav() ?>
				</div>
				<?php } else { ?>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<?php NovelLitemenu_nav(); ?>
				</div>
				<?php } ?>      
				</div>

            <!-- Collect the nav links, forms, and other content for toggling -->
			</div>

        </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
		</div>
<div class="aq"><div class="borderlne"></div></div>
    </nav>

<div id="pop1" class="simplePopup row" style="position: absolute; top: -100%;">
  <!-- <div class="col-xs-12" style="text-align:center" id="popup_head">
    <h6>LETS CREATE SOMETHING TOGETHER</h6>
  </div> -->
  <div class=" col-md-6 hidden-xs hidden-sm">
    <!-- <h2 class="popup_sub">MEET US HERE</h2> -->
    <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/index/map.png" class="img-responsive" id="map_image">

    <div class='pin bounce'></div>
    <div class='pulse'></div>
    <div class='pin bounce' id="pin2_marker"></div>
    <div class='pulse' id="pin2b_marker"></div>
  </div>
  <div class=" col-md-4 col-md-offset-1">
    <div style="width:95%" class="slidedown_popup">
      <h2 class="popup_sub">Transforming your ideas to reality.</h2>
      <?php echo do_shortcode( '[contact-form-7 id="16189" title="Slide down popup new Home page"]' ); ?>
    </div>
  </div>
</div>

	 <?php if (current_user_can('manage_options')) { ?>
	<style>
.navbar-shrink {
    background: #fff !important;
}
.navbar .sf-menu li:hover, .navbar .sf-menu li.sfHover {
    background-color: transparent !important;
    -webkit-transition: none;
    transition: none;
}
.brand-color-element.fifth {
    background-image: linear-gradient(-225deg,#755dd5 35%,#3788d1);
    margin-right: 0!important;
    display: none !important;
    position: relative;
    display: inline-grid;
}




	.navbar-default {
	margin-top: 32px;
	}
a#fb_connect img {
    margin-left: 9px;
}    
@media only screen and (min-width: 200px) and (max-width: 767px) {
	
	
#headerBannerTitle {
    height: 55px;
    font-size: 13px!important;
    color: #ffffff;
    /* animation: neonBlur 16s infinite; */
    font-weight: 300;
    font-size: 30px;
    text-align: center;
    background: #0088fc;
    font-weight: 600;
    letter-spacing: 1px;
    padding-top: 4px;
    line-height: 23px;
padding: 4px 20px!important;
}
div#headerBannerTitle span {
    padding: 8px!important;
    border: 0px!important;
    font-size: 13px!important;
    color: #00ff50!important;
}
div#pop1{
max-height:100vh!important;
overflow:auto!important;
z-index:9999999!important;
background:#f9f9f9!important;
}
div#contact_fb_but{border: 2px solid #4caf50!important;
    background: #ffffff!important;}
div#contact_fb_but img {
    margin: 19% 0 0 15%;
}

h2.popup_sub{
display:none!important
}
}     


div#footer_social ul {
    padding-top: 100px !important;
}   

.navbar .sf-menu li:hover, .navbar .sf-menu li.sfHover {
    background-color: transparent !important;
    -webkit-transition: none;
    transition: none;
}
span.wpcf7-form-control-wrap.classcommonform12 input {
background: #0088fc;
    color: #fff;
    border: 1px solid #fff;
    margin-top: 2px;
    padding: 0px;
    border-radius: 23px 0px 0px 0px;
    width: 100%;
margin:0px
}

span.wpcf7-quiz-label{
font-size: 13px!important;
top: 0px!important;
}
	</style>


	<?php } ?>


<script type="text/javascript">
    //GA Event Tracker Script. Licensed under MIT. Free for any use by all. Written by Paul Seal from codeshare.co.uk

    // Get the category, action and label from the element and send it to GA. The action is optional, because mostly it will be a click event.
    var trackClickEvent = function () {
        var eventCategory = this.getAttribute("data-event-category");
        var eventAction = this.getAttribute("data-event-action");
        var eventLabel = this.getAttribute("data-event-label");
        var eventValue = this.getAttribute("data-event-value");
        ga('send', 'event', eventCategory, (eventAction != undefined && eventAction != '' ? eventAction : 'click'), eventLabel, eventValue);
    };

    // Find all of the elements on the page which have the class 'ga-event'
    var elementsToTrack = document.getElementsByClassName("ga-event");

    // Add an event listener to each of the elements you found
    var elementsToTrackLength = elementsToTrack.length;
    for (var i = 0; i < elementsToTrackLength; i++) {
        elementsToTrack[i].addEventListener('click', trackClickEvent, false);
    }


</script>
	



<style>




.dropdown-content {
    /* display: none; */
    position: absolute;
    margin: 60px 0 0 0;
    width: 85%;
   
    z-index: 1;
}

.dropdown-content .header {
    background: red;
    padding: 1px 16px;
    color: white;
}

.dropdown:hover .dropdown-content {
    display: block;
}

/* Create three equal columns that floats next to each other */
h3.submenuhead {
    font-size: 15px;
    background: #ececec;
    padding: 5% 0 5% 3%;
    color: #0088fc;
    margin: 0px;
}

.column {
    float: left;
    width: 33.33%;
    padding: 0 2px;
    background-color: #fff;
    height: 575px;
}

.column a {
    float: none;
    color: #000!important;
    padding: 8px 3%;
    text-decoration: none;
    display: block;
    text-align: left;
    font-size: 12px!important;
    font-weight: 600;
    border-bottom: 1px solid #f1f1f1;
    border-left: 3px solid transparent;
}

.column a:hover {
    background-color: #ddd;
    border-left: 3px solid #0088fc;
}



/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
    .column {
        width: 100%;
        height: auto;
    }
}
</style>

<style>
/* Css for Slider down Popup*/
.simplePopup{background: #0088FC;}
h2.popup_sub {
    font-weight: normal;
    font-size: 25px!important;
    line-height: 30px!important;
    text-align: left;
    color: white;
    margin-top: 10px!important;
    margin-bottom: 35px!important;}
    input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform12::-webkit-input-placeholder{color:white!important;} /* Chrome/Opera/Safari */
    input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform12::-moz-placeholder{color:white!important;} /* Firefox 19+ */
    input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform12:-ms-input-placeholder{color:white!important;} /* IE 10+ */
    input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform12:-moz-placeholder{color:white!important;} /* Firefox 18- */

    textarea.wpcf7-form-control.wpcf7-textarea.commonform12::-webkit-input-placeholder{color:white;} /* Chrome/Opera/Safari */
    textarea.wpcf7-form-control.wpcf7-textarea.commonform12::-moz-placeholder{color:white;} /* Firefox 19+ */
    textarea.wpcf7-form-control.wpcf7-textarea.commonform12:-ms-input-placeholder{color:white;} /* IE 10+ */
    textarea.wpcf7-form-control.wpcf7-textarea.commonform12:-moz-placeholder{color:white;} /* Firefox 18- */

    input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform12, select.wpcf7-form-control.wpcf7-select.wpcf7-validates-as-required.commonform12, span.wpcf7-form-control-wrap.classcommonform12 input {
    border: solid 1px #ffffff;
    padding: 0% 0 0 6%;
    font-size: 15px;
    background: #0088fc;
    width: 100%;
    margin-bottom: 10px;
    border-top-left-radius: 20px;
    height: 60px!important;
    outline: 0px;
    color: white;
    margin: 0px;
    }
    textarea.wpcf7-form-control.wpcf7-textarea.commonform12{
    width: 100%;
    max-width: 100%;
    background: #0088fc!important;
    height: 90px!important;
    border-top-left-radius: 20px;
    padding: 3% 0 0 6%;
    border: solid 1px #ffffff;
    outline: 0px;
    color: white;
font-size:15px!important
    }
input.wpcf7-form-control.wpcf7-submit.btnSubmit12{
    margin: 0px!important;
    background-color: #fff;
    color: #0088fc;
    float: right;
    font-weight: 700;
    border: 1px solid #0088FC;
    padding: 2% 10%!important;
    font-size: 16px;
    box-shadow: 0 19px 25px rgba(0,0,0,0.30), 0 4px 8px rgba(0,0,0,0.22);
    }
span.wpcf7-form-control-wrap.classcommonform12 span {
    color: white;
	font-size: 15px!important; top:0;
}	
	

div#pop1 {
    overflow: hidden;
    left: 0px!important;
}
.simplePopupClose {
    float: left;
    cursor: pointer;
    margin-left: 15px;
    padding: 0% 1%;
    margin-bottom: 10px;
    border: 1px solid white;
    color: white;
    margin: 2% 0 0 2%;
}
.simplePopupClose:hover {
    background: white;
    color: #0088fc;
}
@media only screen and (min-width: 200px) and (max-width: 767px) {
	span.wpcf7-form-control-wrap.classcommonform12 span {
    color: red;
	font-size: 15px!important; top:0;
}
#headerBannerTitle {
    height: 55px;
    font-size: 13px!important;
    color: #ffffff;
    /* animation: neonBlur 16s infinite; */
    font-weight: 300;
    font-size: 30px;
    text-align: center;
    background: #0088fc;
    font-weight: 600;
    letter-spacing: 1px;
    padding-top: 4px;
    line-height: 23px;
padding : 4px 20px!important
}
div#headerBannerTitle span {
    padding: 8px!important;
    border: 0px!important;
    font-size: 13px!important;
    color: #00ff50!important;
}
}
/* End Slider Popup */
</style>
