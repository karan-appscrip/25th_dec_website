<!-- <div class="outer-footer kks">
<div class="container">
<div class="footer-widget-area">
 <h6 style="color:#fff;text-align:center">APP ARSENAL</h6>
       /* <?php
        get_sidebar('footer');
        ?> */
		</div>
    </div>
    </div> -->
    <div id="seventhslider1" class="vc_row wpb_row vc_row-fluid vc_custom_1542176111565 vc_row-has-fill">
			<div class="container-fluid">
    <div class="row footerelement">
      <div class="col-md-3">
        <img src="/wp-content/uploads/2019/03/Appscrip-new-logo-white.png" width="200px">
        <ul class="footersocialmenu list-inline">
          <li>
            <a href="https://www.youtube.com/channel/UC7U65c_o1hNwiwTkWXG32fg" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true" style="color:white;"></i></a>
          </li>
          <li>
              <a href="https://www.instagram.com/appscrip01/" target="_blank"><i class="fa fa-instagram" aria-hidden="true" style="color:white;"></i></a>
          </li>
          <li>
              <a href="https://www.linkedin.com/company/appscrip" target="_blank"><i class="fa fa-linkedin" aria-hidden="true" style="color:white;"></i></a>
          </li>
          <li>
              <a href="https://www.facebook.com/appscrip" target="_blank"><i class="fa fa-facebook" aria-hidden="true" style="color:white;"></i></a>
          </li>
          <li>
              <a href="https://twitter.com/appscrip" target="_blank"><i class="fa fa-twitter" aria-hidden="true" style="color:white;"></i></a>
          </li>
  
        </ul>
        <!-- <ul class="footersocialmenu list-inline">
          <li>
              <p class="copyright"><a href="https://www.appscrip.com/careers/" style="color:white;"> Careers</a></p>
          </li>
          <li>
              <p class="copyright"><a href="https://www.appscrip.com/appscrip-reviews-customer-testimonials-videos/"> Testimonials</a></p>
          </li>
  
        </ul> -->

        <ul class="footersocialmenu list-inline">
          <li>
              <img src="https://dji16gnzvk5qv.cloudfront.net/appscriphome/companyrank+(3).png" class="img-responsive" style="width:120px"/>
          </li>
          <li>
              <img src="https://dji16gnzvk5qv.cloudfront.net/appscriphome/companyrank+(2).png" class="img-responsive" style="width:120px"/>
          </li>
        </ul>
        <ul class="footersocialmenu list-inline">
          <li>
              <img src="/wp-content/uploads/2018/06/Clutch-Top-B2B-India-200x216.png" class="img-responsive" style="width:120px"/>
          </li>
          <li>
              <img src="/wp-content/uploads/2016/07/gf1200x216.png" class="img-responsive" style="width:120px"/>
          </li>
        </ul>

        <p class="copyright">Copyright © 2019 Appscrip . All rights reserved </p>
      </div>
      <div class="col-md-9">
      <div class="col-md-3">
        <ul class="footersubmenu">
          <h4>On-Demand Apps</h4>
          <li>
            <a href="/uber-clone-mytaxi-clone-lyft-clone-taxi-booking-software/">Uber Clone</a>
          </li>
          <li>
            <a href="/mutlivendor-delivery-business-software-rappi-clone/">Uber for Delivery</a>
          </li>
          <li>
            <a href="/gojek-clone-script-services-booking/">Uber for Services</a>
          </li>
          <li>
            <a href="/uber-for-couriers-services-for-on-demand-delivery/">Uber for Couriers</a>
          </li>
          <li>
            <a href="/Uber-for-trucks-convoy-clone-cargomatic-clone-truckerpath-clone-goshare-clone-doft-clone/">Uber for Trucks</a>
          </li>
          <li>
            <a href="/uber-for-babysitters-child-care-booking-software-sittr-urbansitter-clone/">Uber for Childcare</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3">
        <ul class="footersubmenu">
          <h4>On-Demand Apps</h4>
          <li>
            <a href="/uber-for-tow-trucks-uber-for-roadside-assistance-urgently-clone-honk-clone-clone-rapitow-clone-aaa-clone/">Uber for Tow Trucks</a>
          </li>
          <li>
            <a href="/groceries-on-demand-instacart-clone-boxed-clone-script/">Uber for Groceries</a>
          </li>
          <li>
            <a href="/uber-for-x-platform-on-demand-services-app-software-solution-gotasker/">Uber for X</a>
          </li>
          <li>
            <a href="/uber-for-lawn-care-yard-work-services-app/">Uber for Lawn Care</a>
          </li>
          <li>
            <a href="/easyvan-source-code-shyp-source-code-lalamove-source-code-on-demand-delivery-clones/">Uber for Movers</a>
          </li>
          <li>
            <a href="/uber-for-babysitters-child-care-booking-software-sittr-urbansitter-clone/">Uber for Babysitters</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3">
        <ul class="footersubmenu">
          <h4>On-Demand Apps</h4>
          <li>
            <a href="/uber-for-kids-app-kid-transportation-service-software-kid-shuttle-service-solution/">Uber for Kids</a>
          </li>
          <li>
            <a href="/uber-for-laundry-on-demand-laundry-delivery-service-like-washio-zipjet-rinse-software/">Uber for Laundry</a>
          </li>
          <li>
            <a href="/designated-driver-software-solution-designated-driver-app-like-ddvip-sobrio-ddod-ivalet/">Uber for Designated Drivers</a>
          </li>
          <li>
            <a href="/freight-broker-software-freight-finder-app-like-uship-convoy-uber-freight/">Freight Broker Software</a>
          </li>
          <li>
            <a href="/taskrabbit-clone-script-urbanclap-like-on-demand-app/">TaskRabbit Clone</a>
          </li>
			<li>
            <a href="/ubereats-clone-app-script/">UberEats Clone</a>
          </li>
        </ul>
      </div>
  
      <div class="col-md-3">
        <ul class="footersubmenu">
          <h4>Social and Messaging</h4>
          <li>
            <a href="/white-label-chat-software-for-business-and-personal-chat-app-script/">Whatsapp Clone</a>
          </li>
          <li>
            <a href="https://www.appscrip.com/dating-software-for-online-dating/">Bumble Clone</a>
          </li>
          <li>
            <a href="/instagram-clone-ios-web-android/">Instagram Clone</a>
          </li>
          <li>
            <a href="/dating-software-dating-script-for-online-dating-business-best-online-dating-script/">Tinder Clone</a>
          </li>
          <li>
            <a href="/best-video-chat-dating-app-script/">Azar Clone</a>
          </li>
	   <li>
            <a href="/tik-tok-clone-dubsmash-video-selfie-clone-script/">Tiktok Clone</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 footersecondrow">
        <ul class="footersubmenu">
          <h4>Booking &amp; E-Commerce</h4>
          <li>
            <a href="/social-ecommerce-script-best-etsy-clone-software-social-shopping-script/">Wish / Fancy Clone</a>
          </li>
          <li>
            <a href="/opentable-clone-restaurant-bookings-app/">Opentable Clone</a>
          </li>
          <li>
            <a href="/grubhub-clone-seamless-clone-deliveryhero-clone-deliveroo-clone-eat24-clone-justeat-clone-foodpanda-clone-free-mobile-food-ordering-pos-software-for-restaurants/">Grubhub Clone</a>
          </li>
          <li>
            <a href="/offerup-clone-script-buy-and-sell-marketplace-script/">Offerup Clone</a>
          </li>
          <li>
            <a href="/real-estate-listing-app-development/">Zillow / Trulia Clone</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 footersecondrow">
        <ul class="footersubmenu">
          <h4>Other Apps</h4>
			<li>
            <a href="/mutlivendor-delivery-business-software-rappi-clone/">All-in-one Delivery Solution  </a>
          </li>
          <li>
            <a href="/gojek-clone-script-services-booking/">GoJek Clone Script</a>
          </li>
          <li>
            <a href="/medical-shop-software-online-pharmacy-delivery/">Medicine Delivery Script</a>
          </li>
          <li>
            <a href="/snow-plow-software-snow-removal/">Snow Plow Script</a>
          </li>
          <li>
            <a href="/doordash-clone-script/">DoorDash Clone Script</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 footersecondrow">
        <ul class="footersubmenu">
          <h4>Sitemap</h4>
          <li>
            <a href="/about/">About Us</a>
          </li>
          <li>
            <a href="/ios-android-clone-scripts/">Products</a>
          </li>
          <li>
            <a href="/custom-mobile-app-development/">Services</a>
          </li>
          <li>
            <a href="/appscrip-reviews-customer-testimonials-videos/">Testimonials</a>
          </li>
          <li>
            <a href="/careers/">Careers</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 footersecondrow">
        <ul class="footersubmenu">
          <h4>Sitemap</h4>
          <li>
            <a href="/hire-salesforce-developer-force-com-development/">Salesforce</a>
          </li>
          <li>
            <a href="/get-rich-with-mobile-app-clones-how-to-raise-funding-for-mobile-apps-appscrip-reviews/">Products</a>
          </li>
          <li>
            <a href="/get-rich-with-mobile-app-clones-how-to-raise-funding-for-mobile-apps-appscrip-reviews/">Success Stories</a>
          </li>
          <li>
            <a href="/best-software-demo-videos/">Videos</a>
          </li>
          <li>
            <a href="/contact-us/">Contact-Us</a>
          </li>
          <li>
            <a href="https://blog.appscrip.com/">Blog</a>
          </li>
        </ul>
      </div>

    </div>
  </div>
  </div>
		</div>
	






<!-- <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                                  
				<?php if (get_theme_mod('footertext','') != '') { ?>
		<span class="copyright"><?php echo get_theme_mod('footertext',''); ?></span> 
			<?php } else { ?>
                    <p><a href="<?php echo esc_url('http://www.themehunk.com'); ?>"><?php _e('NovelLite Theme', 'novellite'); ?></a> <?php _e('Powered By ', 'novellite'); ?><a href="http://www.wordpress.org"><?php _e(' WordPress', 'novellite'); ?></a></p>
					<?php } ?>
			                </div>
                    </div>
        </div>
    </footer> -->

    
	<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
    
	<?php wp_footer(); ?>
	<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"8cKBj1aEsk00OH", domain:"appscrip.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>


<!-- Linkedin javascript -->
<script type="text/javascript"> _linkedin_partner_id = "305291"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=305291&fmt=gif" /> </noscript>



<script>
var width = jQuery( document ).width();
if(width > 767){
jQuery('#menu-item-2516').hover(function(){
  jQuery('.dropdown-content').show();
console.log('onhover');
},function(e){
    console.log('function',e.toElement.className);
    if(e.toElement.className == 'row'){
        jQuery('.dropdown-content').show();
    }else{
        jQuery('.dropdown-content').hide();
    }
})

jQuery('.dropdown-content').mouseleave(function(e){
  jQuery('.dropdown-content').hide();
console.log('outhover');
})
}
// jQuery(document).hover(".dropdown-content").hide();
</script>

<!-- 	script for webp and png images -->
	<script>
		function canUseWebP() {
			var elem = document.createElement('canvas');

			if (!!(elem.getContext && elem.getContext('2d'))) {
				// was able or not to get WebP representation
				return elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
			}

			// very old browser like IE 8, canvas not supported
			return false;
		}
		if(!canUseWebP()){
			console.log('webp working', jQuery('.child'));
					jQuery('.child').removeClass('webpEnabled');	

	
		}
	</script>
<!-- 	End script for webp and png images -->





<!-- <style>
div#footer_social ul {
    padding-top: 100px !important;

}
	div#contact_fb_but {
    position: absolute;                                             
    background: #1d76b5;       
    width: 10px;
    width: 40px;
    height: 43px;
    right: 0px;
    top: 78px;
    border-radius: 2px 0px 0px 0px;	
}a#fb_connect img {
    margin-left: 9px;
}
</style> -->


<style>

@media (min-width: 1281px) {
div#seventhslider1 {background-color: black; padding: 4% 3% 4% 3%;}
.footersecondrow { margin: 4% 0 0 0;}
#seventhslider .vc_column-inner {width:100%!important;}
ul.footersubmenu { padding: 0; list-style: none;}
ul.footersubmenu h4 { color: white; font-size: 17px; line-height: 19px;  margin: 0 0 6% 0;} 
ul.footersubmenu li {margin: 5% 0;}
ul.footersubmenu li a { color: white; font-size: 14px;/*font-family: 'Roboto', Regular;*/}

ul.footersocialmenu li {  color: white;  margin: 0 3%; font-size: 22px;}
ul.footersocialmenu { margin: 6% 0 0 0;}
ul.footersocialmenu li a {color:white;}
ul.footersocialmenu li a:hover { color: #4a4a4a;}
p.copyright { font-size: 14px; color: white; font-family: 'Hind', sans-serif;      margin: 9% 0 0 4%;}
}

@media (min-width: 1025px) and (max-width: 1280px) {
    div#seventhslider1 {background-color: black; padding: 4% 3% 4% 3%;}
    .footersecondrow { margin: 4% 0 0 0;}
#seventhslider .vc_column-inner {width:100%!important;}
ul.footersubmenu { padding: 0; list-style: none;}
ul.footersubmenu h4 { color: white; font-size: 17px; line-height: 19px;  margin: 0 0 6% 0;} 
ul.footersubmenu li {margin: 5% 0;}
ul.footersubmenu li a { color: white; font-size: 14px;/*font-family: 'Roboto', Regular;*/}

ul.footersocialmenu li {  color: white;  margin: 0 2%; font-size: 21px;}
ul.footersocialmenu { margin: 6% 0 0 0;}
ul.footersocialmenu li a {color:white;}
ul.footersocialmenu li a:hover { color: #4a4a4a;}
p.copyright { font-size: 14px; color: white; font-family: 'Hind', sans-serif;      margin: 9% 0 0 4%;}
}


@media (min-width: 993px) and (max-width: 1024px) {
    div#seventhslider1 {background-color: black; padding: 4% 3% 4% 3%;}
    .footersecondrow { margin: 4% 0 0 0;}
#seventhslider .vc_column-inner {width:100%!important;}
ul.footersubmenu { padding: 0; list-style: none;}
ul.footersubmenu h4 { color: white; font-size: 17px; line-height: 19px;  margin: 0 0 6% 0;} 
ul.footersubmenu li {margin: 5% 0;}
ul.footersubmenu li a { color: white; font-size: 11px;/*font-family: 'Roboto', Regular;*/}

ul.footersocialmenu li {  color: white;  margin: 0 2%; font-size: 21px;}
ul.footersocialmenu { margin: 6% 0 0 0;}
ul.footersocialmenu li a {color:white;}
ul.footersocialmenu li a:hover { color: #4a4a4a;}
p.copyright { font-size: 14px; color: white; font-family: 'Hind', sans-serif;      margin: 9% 0 0 4%;}  
}


@media (min-width: 768px) and (max-width: 992px) {
    div#seventhslider1 {background-color: black; padding: 4% 3% 4% 3%;}
#seventhslider .vc_column-inner {width:100%!important;}
.footerelement {text-align: center;}
ul.footersubmenu { padding: 0; text-align: center; list-style: none; margin: 6% 0 0 0; }
ul.footersubmenu h4 { color: white; font-size: 16px; line-height: 19px;  margin: 0 0 3% 0;} 
ul.footersubmenu li {margin: 2% 0;}
ul.footersubmenu li a { color: white; font-size: 13px;/*font-family: 'Roboto', Regular;*/}

ul.footersocialmenu li {  color: white;  margin: 0 5%; font-size: 18px;}
ul.footersocialmenu { text-align: center; margin: 6% 0 0 0;}
ul.footersocialmenu li a {color:white;}
ul.footersocialmenu li a:hover { color: #4a4a4a;}
p.copyright { font-size: 14px; color: white; font-family: 'Hind', sans-serif;      margin: 6% 0 0 0%; text-align:center;}
}

@media (min-width: 450px) and (max-width: 767px) {
    div#seventhslider1 {background-color: black; padding: 4% 3% 4% 3%;}
#seventhslider .vc_column-inner {width:100%!important;}
.footerelement {text-align: center;}
ul.footersubmenu { padding: 0; text-align: center; list-style: none; margin: 6% 0 0 0; }
ul.footersubmenu h4 { color: white; font-size: 16px; line-height: 19px;  margin: 0 0 3% 0;} 
ul.footersubmenu li {margin: 2% 0;}
ul.footersubmenu li a { color: white; font-size: 12px;/*font-family: 'Roboto', Regular;*/}

ul.footersocialmenu li {  color: white;  margin: 0 5%; font-size: 18px;}
ul.footersocialmenu { text-align: center; margin: 6% 0 0 0;}
ul.footersocialmenu li a {color:white;}
ul.footersocialmenu li a:hover { color: #4a4a4a;}
p.copyright { font-size: 14px; color: white; font-family: 'Hind', sans-serif;      margin: 6% 0 0 0%; text-align:center;}  
  
}

@media (min-width: 280px) and (max-width: 449px) {
    div#seventhslider1 {background-color: black; padding: 4% 3% 4% 3%;}
#seventhslider .vc_column-inner {width:100%!important;}
.footerelement {text-align: center;}
ul.footersubmenu { padding: 0; text-align: center; list-style: none; margin: 6% 0 0 0; }
ul.footersubmenu h4 { color: white; font-size: 16px; line-height: 19px;  margin: 0 0 3% 0;} 
ul.footersubmenu li {margin: 2% 0;}
ul.footersubmenu li a { color: white; font-size: 12px;/*font-family: 'Roboto', Regular;*/}

ul.footersocialmenu li {  color: white;  margin: 0 5%; font-size: 18px;}
ul.footersocialmenu { text-align: center; margin: 6% 0 0 0;}
ul.footersocialmenu li a {color:white;}
ul.footersocialmenu li a:hover { color: #4a4a4a;}
p.copyright { font-size: 14px; color: white; font-family: 'Hind', sans-serif;      margin: 6% 0 0 0%; text-align:center;}  
}

</style>



<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=8cKBj1aEsk00OH" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->

<!-- Start of HubSpot Embed Code -->
  <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4767285.js"></script>
<!-- End of HubSpot Embed Code -->


</body>
</html>