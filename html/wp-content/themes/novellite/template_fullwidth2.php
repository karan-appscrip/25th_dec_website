<?php
/*
  Template Name: Fullwidth Page2
 */
?>
<?php get_header(); ?>
<div class="page-container">
    <div class="container">
        <div class="row">
            <div class="page-content">
              <div class="fullwidth">           
            <div class="content-bar gallery"> 
                        <?php the_content(); ?>
                        <?php
                        $limit = get_option('posts_per_page');
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        query_posts('showposts=' . $limit . '&paged=' . $paged);
                        $wp_query->is_archive = true;
                        $wp_query->is_home = false;
                        ?>
                        <!--Start Post-->
                        <?php get_template_part('loop', 'blog'); ?>   +
                        <?php NovelLite_pagination(); ?> 
						</div>
                    </div>  
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php get_footer(); ?>

