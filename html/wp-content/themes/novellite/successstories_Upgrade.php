<?php
/*
  Template Name: Success stories upgrade
 */
?>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css'></script>
<script type='text/javascript' src='https://cdn.jsdelivr.net/jquery.slick/1.5.0/slick.css'></script>

<style>
button#slick-slide-control00:before {
    content: "\2022";
    font-size: 32px;
}
button#slick-slide-control01:before {
    content: "\2022";
    font-size: 32px;
}
button.slick-next.slick-arrow:before {
    content: "\003e";
color: #0c0c0c;
    font-size: 22px;
    background: #fff !important;
}
button.slick-prev.slick-arrow:before {
    content: "\003c ";
color: #0c0c0c;
    font-size: 22px;
    background: #fff !important;

}
li#menu-item-2439 a {
    background:none!important;
	color:greenyellow!important;
}
nav.navbar.navbar-default.navbar-fixed-top.navbar-shrink li#menu-item-2439 a {
    color: greenyellow!important;
}
   #succ_div1{background:#fecc0f;height:100%}
   #succ_div2{background:#3f88a9;height:100%}
   #succ_div3{background:#fddd58;height:100%}
   #succ_div4{background:#0875ba;height:100%}
  #suc_img1 {
    float: left;
    width: 38%;
    margin-top: 22%;
    margin-left: 13%;
}
   #suc_img2 {
    float: left;
    width: 53%;
    margin-left: -12%;
    margin-top: 28%;
}
   img#roadyo {
       float: right;
    margin-top: 18%;
    width: 25%;
    margin-right: 16%;}
   img#primelogo{
	   margin-top: 80px;
    width: 50%;
   }
   #suc_para1 {
   margin-top: 40%;
    width: 84%;
    font-size: 18px;
    font-weight: 800;
    line-height: 32px;
    color: #4b4a4b;
}
   #suc_para2{
	   margin-top: 15%;
    WIDTH: 90%;
    font-size: 18px;
    font-weight: 800;
    line-height: 32px;
    color: #fff;
   }
   a#succ_button {
    font-size: 18px;
    color: #54514c;
    font-weight: 800;
    border-radius: 60px;
    border: solid 2px;
	position:absolute;
    padding: 10px 20px;
    margin-top: 31px;
	}
	a#succ_button2 {
    font-size: 18px;
    color: #fff;
    font-weight: 800;
    border-radius: 60px;
    border: solid 2px #fff;
	position:absolute;
    padding: 10px 20px;
    margin-top: 31px;
	}
#success_downloadbut{margin-left: 56%;}
	 #app_buttons,#app_buttonsb{font-size: 18px;
    font-weight: bolder;
	text-align:center;
    line-height: 32px;
    color: #4b4a4b;}
	#app_buttons2{font-size: 18px;
    font-weight: bolder;
	text-align:center;
    line-height: 32px;
    color: #fff;}
	a#succ_button2:hover{    background: #fff;
    color: #4b4a4b;
    -webkit-transition: background 0.5s linear;
    -moz-transition: background 0.3s linear;
    -ms-transition: background 0.3s linear;
    -o-transition: background 0.3s linear;
    transition: background 0.4s ease-in-out;}
	.succ_app_buttons{
		font-size: 18px;
    color: #54514c;
    font-weight: 800;
    border-radius: 40px;
    border: solid 2px;
    padding: 4px 24px;
	}
	.succ_app_buttonsb{
		font-size: 18px;
    color: #fff;
    font-weight: 800;
    border-radius: 40px;
    border: solid 2px #fff;
    padding: 4px 24px;
	}
	.succ_app_buttons2{font-size: 18px;
    color: #54514c;
    font-weight: 800;
    border-radius: 40px;
    border: solid 2px;
    padding: 2px 24px;}
	.succ_app_buttons2b{font-size: 18px;
    color: #fff;
    font-weight: 800;
    border-radius: 40px;
    border: solid 2px #fff;
    padding: 2px 24px;}
	a#succ_button:hover{        background: rgb(75, 74, 75);
    color: #fff;
	border-color:rgb(75, 74, 75);
    -webkit-transition: background 0.5s linear;
    -moz-transition: background 0.3s linear;
    -ms-transition: background 0.3s linear;
    -o-transition: background 0.3s linear;
    transition: background 0.4s ease-in-out;}
	.succ_app_buttons:hover,.succ_app_buttons2:hover{
		background:rgba(255, 255, 255, 0.4);
    color: #54514c;
	border:#fff solid 2px;
    -webkit-transition: background 0.5s linear;
    -moz-transition: background 0.3s linear;
    -ms-transition: background 0.3s linear;
    -o-transition: background 0.3s linear;
    transition: background 0.4s ease-in-out;
-webkit-box-shadow: 0px 0px 16px 5px rgba(156,156,156,0.56);
-moz-box-shadow: 0px 0px 16px 5px rgba(156,156,156,0.56);
box-shadow: 0px 0px 16px 5px rgba(156,156,156,0.56);
	}
	.succ_app_buttonsb:hover,.succ_app_buttons2b:hover{
		background:rgba(255, 255, 255, 0.2);
    color: #54514c;
	border:#fff solid 2px;
    -webkit-transition: background 0.5s linear;
    -moz-transition: background 0.3s linear;
    -ms-transition: background 0.3s linear;
    -o-transition: background 0.3s linear;
    transition: background 0.4s ease-in-out;
-webkit-box-shadow: 0px 0px 16px 5px rgba(42,171,253,0.56);
-moz-box-shadow: 0px 0px 16px 5px rgba(42,171,253,0.56);
box-shadow: 0px 0px 16px 5px rgba(42,171,253,0.56);
	}
#succ_div2,#succ_div4{padding-left:8%}

.wpb_single_image.wpb_content_element.vc_align_left.margin_left {
    margin-left: -26%;
}
.vc_btn3-container.success_but.vc_btn3-right.vc_custom_1468310203121 button {
    /* padding: 3%; */
           padding-left: 62px;
    padding-right: 45px;
    font-weight: bold;
}
.move_left_success{margin-left:-8%}
.wpb_text_column.wpb_content_element.success_font_size p {
    /* font-size: 20px!important; */
    font-size: 16px;
    font-weight: 400;
    line-height: 35px;
    margin-top: 10%;
}
.vc_general.vc_btn3{font-weight:400!important}
.wpb_text_column.wpb_content_element.success_font_size2 p {
    /* font-size: 20px!important; */
    font-size: 16px;
    font-weight: 800;
    line-height: 35px;
	color:#fff;
    margin-top: 10%;
}.wpb_text_column.wpb_content_element.success_heading h2 {
    font-weight: bolder;
    text-align: center;
    line-height: 32px;
    font-size: 20px;
    margin-bottom: -18px;
    margin-right: 6%;
    margin-top: -5%;
}.wpb_text_column.wpb_content_element.success_heading2 h2 {
	color:#fff;
    font-weight: bolder;
    text-align: center;
    line-height: 32px;
    font-size: 20px;
    margin-bottom: -18px;
    margin-right: 6%;
    margin-top: -5%;
}
button{font-weight:bold!important}
.vc_btn3-container.success_but2.vc_btn3-right.vc_custom_1468320743055 button {
    /* padding-left: 15%; */
    padding-left: 62px;
    padding-right: 45px;
}.success_btn_resize button{width:125%}
.success_buttonhead h3{    font-size: 16px;
    font-weight: bold;margin-top:0%}
	.wpb_button, .wpb_content_element, ul.wpb_thumbnails-fluid>li {
    margin-bottom: 10px!important;
}
.vc_btn3-container.vc_btn3-center {
    text-align: center;
    margin-bottom: 11px;
}
@media only screen and (min-width: 320px) and (max-width: 480px) {
	.wpb_single_image img {
    height: auto;
    max-width: 47%!important;
    vertical-align: top;
	    margin-top: 1%;
}
button{font-size:14px!important}
.wpb_text_column.wpb_content_element.success_font_size p {
    /* font-size: 20px!important; */
    font-size: 14px;
    font-weight: 400;
    line-height:18px;
    margin-top: 1%; 
}
.vc_row.wpb_row.vc_inner.vc_row-fluid.success_btngroup {
    margin-top: -6%;
}
.wpb_text_column.wpb_content_element.success_buttonhead h3 {
    font-size: 16px;
}
.vc_row-has-fill>.vc_column_container>.vc_column-inner{padding-top:0px!important}
.success_button_padding {
    padding-top: 1%!important;
}.vc_btn3-container.vc_btn3-left {
    text-align: center!important;
}
.header_container {
    text-align: center;
    margin-top: 0px!important; 
}
.wpb_single_image.wpb_content_element.vc_align_center.margin_left2.wow.bounceInLeft.resize img {
    width: 30%;
    margin-top: 16%;
}
}
@media only screen and (min-width: 481px) and (max-width: 767px) {
	.wpb_single_image img {
    height: auto;
    max-width: 47%!important;
    vertical-align: top;
	    margin-top: 5%;
} 
.wpb_text_column.wpb_content_element.success_font_size p {
    /* font-size: 20px!important; */
    font-size: 16px;
    font-weight: 400;
    line-height: 28px;
    margin-top: 1%;
}
.vc_row.wpb_row.vc_inner.vc_row-fluid.success_btngroup {
    margin-top: -6%;
}
.wpb_text_column.wpb_content_element.success_buttonhead h3 {
    font-size: 16px;
}
.vc_row-has-fill>.vc_column_container>.vc_column-inner{padding-top:0px!important}
.success_button_padding {
    padding-top: 1%!important;
}.vc_btn3-container.vc_btn3-left {
    text-align: center!important;
}
}
@media only screen and (min-width: 768px) and (max-width: 992px) {
	.wpb_single_image img {
    height: auto;
    max-width: 47%!important;
    vertical-align: top;
	    margin-top: 5%;
}
.wpb_text_column.wpb_content_element.success_font_size p {
    /* font-size: 20px!important; */
    font-size: 16px;
    font-weight: 400;
    line-height: 28px;
    margin-top: 1%;
}
.vc_row.wpb_row.vc_inner.vc_row-fluid.success_btngroup {
    margin-top: -6%;
}
.wpb_text_column.wpb_content_element.success_buttonhead h3 {
    font-size: 16px;
}
.vc_row-has-fill>.vc_column_container>.vc_column-inner{padding-top:0px!important}
.success_button_padding {
    padding-top: 1%!important;
}.vc_btn3-container.vc_btn3-left {
    text-align: center!important;
}
}
@media only screen and (min-width: 993px) and (max-width: 1200px) {
	.success_button_padding {
    padding-top: 1%!important;
}.vc_btn3-container.vc_btn3-left {
    text-align: center!important;
}
	
	.wpb_single_image img {
    height: auto;
    max-width: 47%!important;
    vertical-align: top;
	    margin-top: 5%;
}
.wpb_text_column.wpb_content_element.success_font_size p {
    /* font-size: 20px!important; */
    font-size: 16px;
    font-weight: 400;
    line-height: 28px;
    margin-top: 1%;
}
.vc_row.wpb_row.vc_inner.vc_row-fluid.success_btngroup {
    margin-top: -6%;
}
.wpb_text_column.wpb_content_element.success_buttonhead h3 {
    font-size: 16px;
}
.vc_row-has-fill>.vc_column_container>.vc_column-inner{padding-top:0px!important}
.success_button_padding {
    padding-top: 1%;
}
}
@media only screen and (min-width: 1200px) and (max-width: 1371px) {
	.roadyo_logo{text-align:right!important}
	.primemd_logo{text-align:left!important}
} 
@media only screen and (min-width: 1371px) and (max-width: 3300px) {
	.roadyo_logo{text-align:right!important}
	.primemd_logo{text-align:left!important}
}
@media only screen and (min-width: 1700px) and (max-width: 3300px) {
	.wpb_text_column.wpb_content_element.success_font_size p{font-size:18px}
	.vc_btn3.vc_btn3-size-md{font-size:16px!important}
}
.vc_btn3-container {
    display: block;
margin-bottom: 11.74px!important;}

.wpcr3_button_1.wpcr3_show_btn {
    display: inherit;
    width: 180px;
    text-align: center!important;
    margin: 0px auto;
    padding: 11px;
    background: #328bfb;
    color: #fff;
    font-size: 14px;
    border-bottom: 0px solid #e0dddd;
}
.wpcr3_item.wpcr3_business {
    text-align: center;
    font-size: 16px;
}
.wpcr3_leave_text {
    text-align: center;
    font-size: 18px!important;
    padding: 30px;
    color: #a7a1a1;
}
.wpcr3_div_2 {
    width: 300px!important;
    /* max-width: 500px; */
margin:0px auto!important;
}
.wpcr3_div_2 {
    width: 460px!important;
}
.wpcr3_div_2 {
    width: 550px!important;
    padding: 10px;
    background: #f9f9f9;
    border: 1px solid #e2e1e1;
    border-radius: 5px;
    margin-top: 20px!important;
}
.wpcr3_button_1 {
    width: 47%;
    padding: 10px!important;
    margin-top: 21px;
    background: #8BC34A!important;
    color: #fff;
}
tr.wpcr3_review_form_text_field td input {
    margin-bottom: 16px;
    width: 100%;
    padding: 10px;
    border-radius: 5px;
    border: 1px solid #c1c1c1;
    outline: 0px;
}
.wpcr3_respond_2 textarea {
    height: 100px !important;
    min-width: 100%!important;
    resize: none;
    border: 1px solid #c1c1c1;
    border-radius: 5px;
    outline:0px
}
label.comment-field {
    font-size: 14px;
    color: grey;
    font-weight: 600;
}
.wpcr3_leave_text {
    font-weight: 400!important;
    color: #0fc2ea;
    border-bottom: 1px dotted #e6dede;
    margin-bottom: 20px;
    padding: 17px;
    padding-top: 5px;
}
h2.inner-heading {
    font-weight: 800;
    letter-spacing: 1px;
    font-size: 41px;
    color: #1d76b5;
}
.apct-testim-wrapper #template-3 .apct-testimonial-content {
    line-height: 1.5;
    font-size: 14px!important;
}

/* slick */
button.slick-prev.slick-arrow {
    position: absolute;
    left: -49px;
   border:0 !important;
background:#fff !important;
    z-index: 99999;
    
}

.vc_row.wpb_row.vc_row-fluid.popop .vc_column-inner {
    width: 100% !important;
}
.slick-slide img {
    display: inline !important;
}
.cdd.wpb_column.vc_column_container.vc_col-sm-12 .vc_column-inner {
    width: 100% !important;
}
.vc_column_container>.vc_column-inner {
    width: 100%!important;
}

.mm_img {
    text-align: center !important;
    width: 100px;
    margin-top: 23px;
}
.normal {
    background: #0875ba !important;
    margin-right: 25px;
   
   
    border-radius: 20px;
}
.ee {
    background: #0b0b0b !important;
}
.ff {
    background: #3b51a3 !important;
}
img.st_hh {
    width: 200px;
    margin-top:20px;
}
.nndfg {
    width: 150px;
    text-align: center;
}
i.fa.fa-apple.nndfg:before {
    padding-right: 19px !important;
}
i.fa.fa-apple.nndfg:before {
    padding-right: 20px;
}
.img_start {
    text-align: center;
}
.gdfdfg {
    text-align:center;
}
.dgfdfggret {
    text-align: center;
margin-bottom:35px;
   
}
section.regular.slider.slick-initialized.slick-slider.slick-dotted {
    height: 80% !important;
}
.nndfg {
    border: 2px solid #fff;
    padding: 10px 15px 10px 15px;
    color: #fff;
    font-size: 12px !important;
    border-radius: 24px;
    margin-bottom: 15px;
}
i.fa.fa-android.nndfg:before {
    padding-right: 10px;
}
.normal {
   
    margin-right: 25px;
   
    height: auto !important;
}
/* slick */


@charset 'UTF-8';
/* Slider */
.slick-loading .slick-list
{
    background: #fff url('./ajax-loader.gif') center center no-repeat;
}

/* Icons */
@font-face
{
    font-family: 'slick';
    font-weight: normal;
    font-style: normal;

    src: url('./fonts/slick.eot');
    src: url('./fonts/slick.eot?#iefix') format('embedded-opentype'), url('./fonts/slick.woff') format('woff'), url('./fonts/slick.ttf') format('truetype'), url('./fonts/slick.svg#slick') format('svg');
}
/* Arrows */
.slick-prev, .slick-next {
    font-size: 0;
    line-height: 0;
    position: absolute;
    top: 50%;
    display: block;
    width: 20px;
    height: 20px;
    padding: 0;
    -webkit-transform: translate(0, -50%);
    -ms-transform: translate(0, -50%);
    transform: translate(0, -50%);
    cursor: pointer;
    color: #333 !important;
    border: none;
    outline: none;
    
}
.slick-prev:hover,
.slick-prev:focus,
.slick-next:hover,
.slick-next:focus
{
    color: transparent;
    outline: none;
    background: transparent;
}
.slick-prev:hover:before,
.slick-prev:focus:before,
.slick-next:hover:before,
.slick-next:focus:before
{
    opacity: 1;
}
.slick-prev.slick-disabled:before,
.slick-next.slick-disabled:before
{
    opacity: .25;
}

.slick-prev:before,
.slick-next:before
{
    font-family: 'slick';
    font-size: 20px;
    line-height: 1;

    opacity: .75;
    color: white;

    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

.slick-prev
{
    left: -25px;
}
[dir='rtl'] .slick-prev
{
    right: -25px;
    left: auto;
}
.slick-prev:before
{
    content: 'â†';
}
[dir='rtl'] .slick-prev:before
{
    content: 'â†’';
}

.slick-next
{
    right: -25px;
background:#fff !important;
}
[dir='rtl'] .slick-next
{
    right: auto;
    left: -25px;
}
.slick-next:before
{
    content: 'â†’';
}
[dir='rtl'] .slick-next:before
{
    content: 'â†';
}

/* Dots */
.slick-dotted.slick-slider
{
    margin-bottom: 30px;
}

.slick-dots
{
    position: absolute;
    bottom: -25px;

    display: block;

    width: 100%;
    padding: 0;
    margin: 0;

    list-style: none;

    text-align: center;
}
.slick-dots li
{
    position: relative;

    display: inline-block;

    width: 20px;
    height: 20px;
    margin: 0 5px;
    padding: 0;

    cursor: pointer;
}
.slick-dots li button
{
    font-size: 0;
    line-height: 0;

    display: block;

    width: 20px;
    height: 20px;
    padding: 5px;

    cursor: pointer;

    color: transparent;
    border: 0;
    outline: none;
    background: transparent;
}
.slick-dots li button:hover,
.slick-dots li button:focus
{
    outline: none;
}
.slick-dots li button:hover:before,
.slick-dots li button:focus:before
{
    opacity: 1;
}
.slick-dots li button:before
{
    font-family: 'slick';
    font-size: 6px;
    line-height: 20px;

    position: absolute;
    top: 0;
    left: 0;

    width: 20px;
    height: 20px;

    content: 'â€¢';
    text-align: center;

    opacity: .25;
    color: black;

    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
.slick-dots li.slick-active button:before
{
    opacity: .75;
    color: black;
}



</style>
<?php get_header(); ?>
<div class="section-wrapper">
<div class="page-container">
    <div class="container-fluid" style="padding-right: 0px; 
     padding-left: 0px; ">
        <div class="row">
            <div class="page-content">
             <div class="col-md-12">
              <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
				<?php the_content(); ?>	
			<?php endif; ?>	  
				</div>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>
</div>

<?php get_template_part( 'template/section_pricing');  ?>
<?php get_template_part( 'template/section_countactus');  ?>
<?php get_footer(); ?>
</div>
<script type="text/javascript" src="https://s3-ap-southeast-1.amazonaws.com/appscrip/js/slick-theme.css"></script> 
<link rel="stylesheet" type="text/css" href="https://s3-ap-southeast-1.amazonaws.com/appscrip/js/slick.css"/>
<script type="text/javascript" src="https://s3-ap-southeast-1.amazonaws.com/appscrip/js/slick.min.js"></script> 



<script type='text/javascript'>
 jQuery('.autoplay').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
});




jQuery('.regular').slick({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
	
</script>
<!--<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
 <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script>
!function(e,t){"use strict";"function"==typeof define&&define.amd?define(["jquery"],function(n){return t(n,e,e.document)}):"object"==typeof module&&module.exports?module.exports=function(n,o){return void 0===o&&(o="undefined"!=typeof window?require("jquery"):require("jquery")(n)),t(o,e,e.document),o}:t(jQuery,e,e.document)}("undefined"!=typeof window?window:this,function(e,t,n,o){"use strict";function i(n,o,i){if(y===n&&(i=!1),x===!0)return!0;if(f[n]){if(E=!1,i&&j.before(n,p),v=1,j.sectionName&&(I!==!0||0!==n))
if(history.pushState)try{history.replaceState(null,null,f[n])}catch(s){t.console&&console.warn("Scrollify warning: This needs to be hosted on a server to manipulate the hash value.")}else t.location.hash=f[n];if(o)e(j.target).stop().scrollTop(d[n]),i&&j.after(n,p);else{if(M=!0,e().velocity?e(j.target).stop().velocity("scroll",{duration:j.scrollSpeed,easing:j.easing,offset:d[n],mobileHA:!1}):e(j.target).stop().animate({scrollTop:d[n]},j.scrollSpeed,j.easing),t.location.hash.length&&j.sectionName&&t.console)try{e(t.location.hash).length&&console.warn("Scrollify warning: There are IDs on the page that match the hash value - this will cause the page to anchor.")}catch(s){console.warn("Scrollify warning:",t.location.hash,"is not a valid jQuery expression.")}
e(j.target).promise().done(function(){y=n,M=!1,I=!1,i&&j.after(n,p)})}}}
function s(e){function t(t){for(var n=0,o=e.slice(Math.max(e.length-t,1)),i=0;i<o.length;i++)n+=o[i];return Math.ceil(n / t)}
var n=t(10),o=t(70);return n>=o?!0:!1}
function r(e,t){for(var n=f.length;n>=0;n--)"string"==typeof e?f[n]===e&&(m=n,i(n,t,!0)):n===e&&(m=n,i(n,t,!0))}
var c,a,l,u,h,d=[],f=[],p=[],g=[],m=0,y=0,v=1,w=!1,b=e(t),S=b.scrollTop(),E=!1,M=!1,H=!1,x=!1,T=[],D=(new Date).getTime(),I=!0,L=!1,N="onwheel"in n?"wheel":n.onmousewheel!==o?"mousewheel":"DOMMouseScroll",j={section:".section",sectionName:"section-name",interstitialSection:"",easing:"easeOutExpo",scrollSpeed:1100,offset:0,scrollbars:!0,axis:"y",target:"html,body",standardScrollElements:!1,setHeights:!0,overflowScroll:!0,before:function(){},after:function(){},afterResize:function(){},afterRender:function(){}};e.scrollify=function(o){function r(t){e().velocity?e(j.target).stop().velocity("scroll",{duration:j.scrollSpeed,easing:j.easing,offset:t,mobileHA:!1}):e(j.target).stop().animate({scrollTop:t},j.scrollSpeed,j.easing)}
function y(){var t=j.section;g=[],j.interstitialSection.length&&(t+=","+j.interstitialSection),e(t).each(function(t){j.setHeights?e(this).is(j.interstitialSection)?g[t]=!1:e(this).css("height","auto").outerHeight()<b.height()||"hidden"===e(this).css("overflow")?(e(this).css({height:b.height()}),g[t]=!1):(e(this).css({height:e(this).height()}),j.overflowScroll?g[t]=!0:g[t]=!1):e(this).outerHeight()<b.height()||j.overflowScroll===!1?g[t]=!1:g[t]=!0})}
function I(n){var o=j.section;j.interstitialSection.length&&(o+=","+j.interstitialSection),d=[],f=[],p=[],e(o).each(function(n){n>0?d[n]=parseInt(e(this).offset().top)+j.offset:d[n]=parseInt(e(this).offset().top),j.sectionName&&e(this).data(j.sectionName)?f[n]="#"+e(this).data(j.sectionName).replace(/ /g,"-"):e(this).is(j.interstitialSection)===!1?f[n]="#"+(n+1):(f[n]="#",n===e(o).length-1&&n>1&&(d[n]=d[n-1]+parseInt(e(this).height()))),p[n]=e(this);try{e(f[n]).length&&t.console&&console.warn("Scrollify warning: Section names can't match IDs on the page - this will cause the browser to anchor.")}catch(i){}
t.location.hash===f[n]&&(m=n,w=!0)}),!0===n?i(m,!1,!1):j.afterRender()}
function k(){return g[m]?(S=b.scrollTop(),S>parseInt(d[m])?!1:!0):!0}
function z(){return g[m]?(S=b.scrollTop(),S<parseInt(d[m])+(p[m].outerHeight()-b.height())-28?!1:!0):!0}
L=!0,e.easing.easeOutExpo=function(e,t,n,o,i){return t==i?n+o:o*(-Math.pow(2,-10*t / i)+1)+n},l={calculateNearest:function(){S=b.scrollTop();for(var e,t=1,n=d.length,o=0,s=Math.abs(d[0]-S);n>t;t++)e=Math.abs(d[t]-S),s>e&&(s=e,o=t);(z()||k())&&(m=o,i(o,!1,!0))},wheelHandler:function(n,o){if(x===!0)return!0;if(j.standardScrollElements&&(e(n.target).is(j.standardScrollElements)||e(n.target).closest(j.standardScrollElements).length))return!0;g[m]||n.preventDefault();var r=(new Date).getTime();n=n||t.event;var c=n.originalEvent.wheelDelta||-n.originalEvent.deltaY||-n.originalEvent.detail,o=Math.max(-1,Math.min(1,c));if(T.length>149&&T.shift(),T.push(Math.abs(c)),r-D>200&&(T=[]),D=r,M)return!1;if(0>o){if(m<d.length-1&&z()){if(!s(T))return!1;n.preventDefault(),m++,M=!0,i(m,!1,!0)}}else if(o>0&&m>0&&k()){if(!s(T))return!1;n.preventDefault(),m--,M=!0,i(m,!1,!0)}},keyHandler:function(e){return x===!0?!0:M===!0?!1:void(38==e.keyCode?m>0&&k()&&(m--,i(m,!1,!0)):40==e.keyCode&&m<d.length-1&&z()&&(m++,i(m,!1,!0)))},init:function(){j.scrollbars?(b.bind("mousedown",l.handleMousedown),b.bind("mouseup",l.handleMouseup),b.bind("scroll",l.handleScroll)):e("body").css({overflow:"hidden"}),e(n).bind(N,l.wheelHandler),e(n).bind("keydown",l.keyHandler)}},u={touches:{touchstart:{y:-1,x:-1},touchmove:{y:-1,x:-1},touchend:!1,direction:"undetermined"},options:{distance:30,timeGap:800,timeStamp:(new Date).getTime()},touchHandler:function(t){if(x===!0)return!0;if(j.standardScrollElements&&(e(t.target).is(j.standardScrollElements)||e(t.target).closest(j.standardScrollElements).length))return!0;var n;if("undefined"!=typeof t&&"undefined"!=typeof t.touches)switch(n=t.touches[0],t.type){case"touchstart":u.touches.touchstart.y=n.pageY,u.touches.touchmove.y=-1,u.touches.touchstart.x=n.pageX,u.touches.touchmove.x=-1,u.options.timeStamp=(new Date).getTime(),u.touches.touchend=!1;case"touchmove":u.touches.touchmove.y=n.pageY,u.touches.touchmove.x=n.pageX,u.touches.touchstart.y!==u.touches.touchmove.y&&Math.abs(u.touches.touchstart.y-u.touches.touchmove.y)>Math.abs(u.touches.touchstart.x-u.touches.touchmove.x)&&(t.preventDefault(),u.touches.direction="y",u.options.timeStamp+u.options.timeGap<(new Date).getTime()&&0==u.touches.touchend&&(u.touches.touchend=!0,u.touches.touchstart.y>-1&&Math.abs(u.touches.touchmove.y-u.touches.touchstart.y)>u.options.distance&&(u.touches.touchstart.y<u.touches.touchmove.y?u.up():u.down())));break;case"touchend":u.touches[t.type]===!1&&(u.touches[t.type]=!0,u.touches.touchstart.y>-1&&u.touches.touchmove.y>-1&&"y"===u.touches.direction&&(Math.abs(u.touches.touchmove.y-u.touches.touchstart.y)>u.options.distance&&(u.touches.touchstart.y<u.touches.touchmove.y?u.up():u.down()),u.touches.touchstart.y=-1,u.touches.touchstart.x=-1,u.touches.direction="undetermined"))}},down:function(){m<=d.length-1&&(z()&&m<d.length-1?(m++,i(m,!1,!0)):Math.floor(p[m].height()/ b.height())>v?(r(parseInt(d[m])+b.height()*v),v+=1):r(parseInt(d[m])+(p[m].height()-b.height())))},up:function(){m>=0&&(k()&&m>0?(m--,i(m,!1,!0)):v>2?(v-=1,r(parseInt(d[m])+b.height()*v)):(v=1,r(parseInt(d[m]))))},init:function(){n.addEventListener&&(n.addEventListener("touchstart",u.touchHandler,!1),n.addEventListener("touchmove",u.touchHandler,!1),n.addEventListener("touchend",u.touchHandler,!1))}},h={refresh:function(e){clearTimeout(a),a=setTimeout(function(){y(),I(!0),e&&j.afterResize()},400)},handleUpdate:function(){h.refresh(!1)},handleResize:function(){h.refresh(!0)}},j=e.extend(j,o),y(),I(!1),!0===w?i(m,!1,!0):setTimeout(function(){i(0,!1,!0)},200),d.length&&(l.init(),u.init(),b.bind("resize",h.handleResize),n.addEventListener&&t.addEventListener("orientationchange",h.handleResize,!1))},e.scrollify.move=function(t){return t===o?!1:(t.originalEvent&&(t=e(this).attr("href")),void r(t,!1))},e.scrollify.instantMove=function(e){return e===o?!1:void r(e,!0)},e.scrollify.next=function(){m<f.length&&(m+=1,i(m,!1,!0))},e.scrollify.previous=function(){m>0&&(m-=1,i(m,!1,!0))},e.scrollify.instantNext=function(){m<f.length&&(m+=1,i(m,!0,!0))},e.scrollify.instantPrevious=function(){m>0&&(m-=1,i(m,!0,!0))},e.scrollify.destroy=function(){return L?(j.setHeights&&e(j.section).each(function(){e(this).css("height","auto")}),b.unbind("resize",h.handleResize),j.scrollbars&&(b.unbind("mousedown",l.handleMousedown),b.unbind("mouseup",l.handleMouseup),b.unbind("scroll",l.handleScroll)),e(n).unbind(N,l.wheelHandler),e(n).unbind("keydown",l.keyHandler),n.addEventListener&&(n.removeEventListener("touchstart",u.touchHandler,!1),n.removeEventListener("touchmove",u.touchHandler,!1),n.removeEventListener("touchend",u.touchHandler,!1)),d=[],f=[],p=[],void(g=[])):!1},e.scrollify.update=function(){return L?void h.handleUpdate():!1},e.scrollify.current=function(){return p[m]},e.scrollify.disable=function(){x=!0},e.scrollify.enable=function(){x=!1},e.scrollify.isDisabled=function(){return x},e.scrollify.setOptions=function(n){return L?void("object"==typeof n?(j=e.extend(j,n),h.handleUpdate()):t.console&&console.warn("Scrollify warning: Options need to be in an object.")):!1}});</script>
<style>
.vc_column_container>.vc_column-inner {
    width: 100%!important;
}
</style>
<script>
$(function() {
  $(".scroll-target").css({"height":$(window).height()});
  $.scrollify({
    section:".scroll-target",
	scrollSpeed: 600,
    easing: "easeInOutQuart",
	offset : 0,
	setHeights: false,
	sectionName : false,  
  });
  
$(window).scroll(function() {
	var het = $( window ).height();
	var wid = $( window ).width();
     var cal = parseInt(het);
	 var wid2 = parseInt(wid);
	  var hmt = cal * 7;
	 if($(this).height()>hmt){
		 $.scrollify({
		 standardScrollElements: false,
		  });
	 }
	 else{
		 $.scrollify.enable()
	 } 
	 if ($(this).width()<767){
		 $.scrollify.destroy();
	 }
});
});
</script> -->


