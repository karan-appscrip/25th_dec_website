<?php
/*
  Template Name: cart_page
 */
?>
<style>

.navbar .sf-menu li a {
    color: #000!important;
}
span#cart_page_head i {
    color: #cecdcd;
}
span#cart_page_head {
    font-size: 30px;
}
.woocommerce #content table.cart th, .woocommerce table.cart th, .woocommerce-page #content table.cart th, .woocommerce-page table.cart th {
    font-size: 17px!important;
     font-variant: none!important; 
    font-weight: 600!important;
}
input#coupon_code {
    width: 160px;
    border-bottom: solid 1px #d0c9c9;
    padding-bottom: 9px;
}
input.button {
    padding: 10px;
    font-size: 15px!important;
}
input.button {
    background: #176a8a!important;
    outline: 0px;
}
input.button:hover {
        background: #083344!important;
	transition: all 300ms ease-in-out;
    outline: 0px;
}
.woocommerce a.remove {
    color: #7d7373!important;
}
a.checkout-button.button.alt.wc-forward {
    background: #227cbb!important;
}
a.checkout-button.button.alt.wc-forward :hover{
    background: greenyellow!important;
}
.woocommerce-cart table.cart img, .woocommerce-checkout table.cart img {
width: 0px!important;}
tr.cart_item {
    background: #f7f7f7;
}
@media only screen and (min-width: 200px) and (max-width: 767px){
	table.shop_table.shop_table_responsive.cart {
    margin-left: 10%;
}
td.product-remove{width:10%;float:left}
tr.cart_item td.product-name a {
    font-size: 13px;
    font-weight: 600;
}
.col-md-10.col-md-offset-2 {
    text-align: center;
}
td.product-quantity {
    display: none!important;
}
span.woocommerce-Price-amount.amount {
    font-family: sans-serif;
    font-weight: 600;
}
.cart_totals.calculated_shipping {
    margin-left: 8%;
}
span#cart_page_head {
    font-size: 23px;
    font-weight: 500;
}
}
</style>
<?php get_header(); ?>
  <div class="container-fluid">
        <div class="row">
		<div class="col-md-10 col-md-offset-2">
		<div style="height:150px"></div>
		<span id="cart_page_head"><i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;SHOPPING CART</span>
		<hr style="width:100%">
</div>
</div>
<div class="clear"></div>
</div>


<div class="page-container">
    <div class="container-fluid" style="padding-right: 0px; 
     padding-left: 0px; ">
        <div class="row">
            <div class="page-content">
             <div class="col-md-12">
              <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
				<?php the_content(); ?>	
			<?php endif; ?>	  
				</div>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<?php get_footer(); ?>

