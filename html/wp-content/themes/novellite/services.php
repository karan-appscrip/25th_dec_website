<?php
/*
  Template Name: services
 */
?>
<style>
	.page_heading_container4 {
    padding: 40px 0 22px 0;
    background: #eee;
    height:100%;
    border-top: 1px solid #ddd;
    background-image: url(https://s3-ap-southeast-1.amazonaws.com/appscrip/services/services.png);
    background-repeat: no-repeat;
    background-position: center; 
    background-size:cover;
    }
    #services_txt{color:#fff;
	text-transform:uppercase;
	font-size:25px;
	text-align:center;
	font-weight:700;
	line-height:47px;
	margin-top:300px;
	}
	#services_fix{
		    background: url(https://s3-ap-southeast-1.amazonaws.com/appscrip/services/fixed+background.png) center repeat fixed;
			background-size:cover;
			height:100%;
			font-weight:800;
			text-align:center;
			font-size:25px;
			color:#fff;
	}
	#services_sec1,#services_sec3{
 height:100%;
padding-top:5%
	}
#services_sec5 {
    height: 100%;
    padding-top: 5%;
}
	#services_sec2,#services_sec4,#services_sec6{background:#fff;
		    height: 100%;
    padding-top: 5%;}
#services_img_para1{width:100%}
	#services_img_para1,#services_img_para3{
    margin-top: 15%;
    width: 100%;
text-align:justify;
    font-size: 18px;
    line-height: 30px;
    font-weight: 700;}
#services_fix_para {
    width:80%;
margin-left:10%
}
	#services_sec7{margin-bottom:5%;    padding-top: 7%;}
	#services_sec7 h1,#services_sec7 p{ text-align:center}
	.services_indivs{
		width:30%;
		height:auto;
		padding-top:30px;
		padding-left:30px;
		padding-right:30px;
		background:#fff;
		margin-left:2.5%;
		text-align:center;
		margin-top:5%;
		float:left;
padding-bottom:12px
	}
	.services_tags{font-weight:800;
	font-size:23px}
	.services_tag_para{margin-top:50px;
	font-weight:600}

img#services_img2 {
       margin-left: 9%;
    margin-top: 30%;
}
#services_indvis_bottom{margin-top:15%}

</style>

<?php get_header(); ?>
<div class="section-wrapper">
<div class="page_heading_container4 section" id="sectiona1">
  <div class="container">
        <div class="row scroll-target">
		<div class="col-md-12">
		<div class="page_heading_content">
		<h1 class="wow fadeInUp" id="services_txt" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s;">Do you need a custom , native iOS or Android mobile app ?<br>
Or do you need to implement Salesforce in your organization? <br>
If you want it , we can build it. Where others struggle , we thrive.</h1>
                 </div>
</div>
<div class="clear"></div>
</div>
</div>
</div>
<div id="services_fix" class="scroll-target section" id="sectiona2">
   <p style="" id="services_fix_para">When you need to move fast and want the best, you may not have the time or money to recruit internal hires. You may have a great vision, but are you going to build a team from the ground up? Can you wait 3 months to get a line of code? Risk losing momentum if someone leaves? Put all your eggs in a tiny basket? We've already found the talent, built the work process and have pre-built products to suit many business models and if you already have some internal folks - that's great. We play nicely with others, and leave our egos firmly in place to make sure you get project results that make you proud.</p>
</div>
    <div class="container-fluid">
     <div id="services_sec1" class="row scroll-target section"  id="sectiona3">
	     <div class="col-md-12 col-lg-4 text_align_image"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/services/service-image-1.png" class="img-responsive animate_left" id="services_img1"></div>
		 <div class="col-md-12 col-lg-offset-1 col-lg-6 animate_right">
		 <h1 id="services_h1">IOS DEVELOPMENT</h1><p id="services_img_para1">Apple is an undoubted leader in the mobile sector, which has made app development an industry in itself. Today, there are apps for everything. Every business needs an iPhone and an iPad app. Everything that Apple does has a huge impact on businesses and lives of people. We can expedite your go to market strategy by leveraging our pre-built iOS mobile platforms and components to reduce your time and costs. We write code on Objective-C and Swift both so if you need a great mobile app development team please contact us for iOS app development services.
</p></div>
	 </div>
	 <div id="services_sec2"  class="row scroll-target section" id="sectiona4">
       <div class="col-xs-12 col-lg-4 col-lg-push-7"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/services/service-image-2.png" class="img-responsive animate_right" id="services_img2"></div>
	    <div class="col-xs-12 col-lg-offset-1 col-lg-6 col-lg-pull-4 animate_left">
		 <h1  id="services_h2">ANDROID DEVELOPMENT</h1><p id="services_img_para3">Android apps live in a highly fragmented multi-device world. The API rules are less strict, but quality assurance testing is more intensive. We handle it. We’ve developed hundreds of cool Android apps on releases from 2.X to 5.X. From Gingerbread to Lollipop, simple phones to custom devices to Android wearables — we’ve done it all. We can expedite your go to market strategy by leveraging our pre-built Android mobile platforms and components to reduce your time and costs. We write code on Java and use Android Studio so if you need a great mobile app development team please contact us for Android app development services.</p></div>
		</div>
	  <div id="services_sec3"  class="row scroll-target section" id="sectiona5">
	    <div class="col-xs-12 col-lg-4"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/services/service-image-3.png" class="img-responsive animate_left" id="services_img1"></div>
		 <div class="col-xs-12 col-lg-offset-1 col-lg-6 animate_right">
		 <h1 id="services_h3">RESPONSIVE WEB</h1><p id="services_img_para3">If your website/web app's not responsive, you're doing it wrong. We create a dynamic viewing experience for easy reading and navigation. We’ve been making our websites and web apps responsive and adaptive for years now. That’s part of our mobile-first, design-first mantra. We build fast-loading, highly optimized experiences that will work across a wide range of devices. This responsive website is a perfect example. Play with the window size to see what it looks like on mobile screens. We'll be here when you're done. So if you need any sort of Node JS development , Java development , Ruby on rails development , PHP development , Angular JS development , Java script development , HTML5 development , R Development or any other web development service , feel free to contact us and we can surely assist you.
         </p></div>
</div>
	 <div id="services_sec4"  class="row scroll-target section" id="sectiona6">
<div class="col-xs-12 col-lg-4 col-lg-push-7"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/services/service-image-4.png" id="services_img4" class="img-responsive animate_right"></div>
	    <div class="col-xs-12 col-lg-offset-1 col-lg-6 col-lg-pull-4 animate_left">
		 <h1 id="services_h4">UI/UX</h1><p id="services_img_para4">From fully fleshed-out concepts to one-sentence pitches, this is where our team stress-tests every project that comes through Appscrip’s doors. We want to know why your idea makes sense, why it’s going to be successful, why anyone would care about it. This is where our expert strategists will challenge assumptions and overhaul business plans.
Working hand-in-hand with our clients, we take your initial concept and merge it with everything the Appscrip team knows about mobile, about startups, about what does and doesn't work. We turn ideas into actual products.
Once we all agree on a general direction, Appscrip’s team fleshes out all the details. One of the great benefits of this process is that minutes in strategy translate to an hour in wireframing, a few hours in design, and days or even weeks in development. So contact us for any mobile app design services.</p></div>
	 </div>
         
         <div id="services_sec5" class="row scroll-target section" id="sectiona7">
	    <div class="col-md-12 col-lg-4"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/services/service-image-5.png" id="services_img5" class="img-responsive animate_left"></div>
		 <div class="col-md-12 col-lg-7 animate_right">
		 <h1 id="services_h5">INTERNET OF THINGS(IOT)</h1><p id="services_img_para5">What will your house say, when it talks back to you? And your car, and your bus, and your restaurant table, and your shopping cart. Let's find out! The Internet is connecting to almost everything around us, and transforming it. The office whiteboard, your refrigerator and home thermostat, even your plants - they can all be plugged into the digital world. Mobile apps will play a huge role in making them work together. Appscrip can help you bring your IOT experience to life. Next-gen wearable solutions in health, security, retail and more are coming soon. We've been on the cutting edge of wearable interfaces for years. If it’s wearable technology — it’s mobile. That means it needs apps to bring the power of smart glasses and watches and fitbands and yet-to-be-made devices to users. screens, mobile biometrics, holographic imaging, and facial and eye recognition.Let's play with iBeacons, build exciting interfaces, analyze new data and create the future - together! We have a pre-built real time data relay platform which we can leverage to transmit data from any hardware device live in real-time. So if you need any work on iBeacon Development , Real time dashboard development , smart bulb development then please feel to contact us.
         </p></div>
	 </div>

         <div id="services_sec6" class="row scroll-target section" id="sectiona8">
    <div class="col-lg-4 col-xs-12 col-lg-push-7"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/services/service-image-6.png" class="img-responsive animate_right" id="services_img6"></div>
	    <div class="col-md-12 col-lg-offset-1 col-lg-6 col-lg-pull-4 animate_left">
		 <h1 id="services_h6">SALESFORCE DEVELOPMENT</h1><p id="services_img_para4">We love to transform your ideas into salesforce solutions. We are expert in customising Salesforce beyond "point and click" i.e. via Apex, Visualforce, Flows, and Triggers.Our USP is gorgeous and responsive visualforce pages, using HTML5, CSS3, Bootstrap, AngularJS and Jquery. We master Salesforce integrations with other systems 'securely', using SOAP/REST with XML/JSON. We can help you in shaping the product, by discussing your ideas and enrich them with our Salesforce expertise. So give us a holler if you are looking out for Salesforce application development services in USA or India.</p></div>
		 
	 </div>
	 </div>
	 </div>
	 <div id="services_sec7" class="scroll-target">
	          <h1>OUR PROCESS</h1>
			  <p style="font-weight:800;font-size:18px" id="services_para_head">If you are building a platform and in it for a long haul we are here to help you through different stages of evolution validation efficeincy and scale</p>
			  <div>
			    <div class="row">
				   <div class="col-lg-offset-1 col-lg-10" id="ser_proc_blocks" style="background:#f4f4f4;height: auto;
    padding-bottom: 5%">  <div class="">
				      <div class="services_indivs" id="services_indvis_bottom">
							<img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/services/process-image1.png">
							<h2 class="services_tags">Security</h2>
							<p class="services_tag_para">Your IP is of utmost importent to us. All of out discussions begin with an NDA</p>
					  </div>
					  <div class="services_indivs">
					        <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/services/process-image-2.png" >
							<h2 class="services_tags">Intial Chat</h2>
							<p class="services_tag_para">You can discuss your idea without any commitment. We understand you might just be window shoping</p>
					  </div>
					  <div class="services_indivs" id="services_indvis_bottom">
					        <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/services/process-image-3.png" >
							<h2 class="services_tags">Consultation</h2>
							<p class="services_tag_para">Using your expertise we would help you brainstrom all aspects of your business model from customer acquisition to unit economics.</p>
					  </div>
</div>
					  <div class="clear"></div>
                                          <div class="scroll-target">
					  <div class="services_indivs" id="services_indvis_bottom">
					       <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/services/process-image-4.png">
						   <h2 class="services_tags">Follow-Up chat</h2>
						   <p class="services_tag_para">Based on the business model, we finalise the nessessory features of the technology to help bring your idea of life.</p>
					  </div>
					  <div class="services_indivs">
					       <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/services/process-image-5.png">
						   <h2 class="services_tags">Total Control</h2>
						   <p class="services_tag_para">Zero Outsourcing. Complete control and maximum flexibility allow you and your team to call the shots at ever level.</p>
					  </div>
					  <div class="services_indivs" id="services_indvis_bottom">
					       <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/services/process-image-6.png
">
						   <h2 class="services_tags">Post Launch Service</h2>
						   <p class="services_tag_para">We stick with you after launch, as we understand that this is just the first step in the process</p>
					  </div>
</div>
				   </div>
				</div>
			  </div>
	 </div>
    </div>
<div class="scroll-target"><?php get_template_part( 'template/section_pricing');  ?></div>
<div class="scroll-target"><?php get_template_part( 'template/section_countactus');  ?></div>
<div class="scroll-target"><?php get_footer(); ?></div>
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>

<script>
!function(r){function a(r,a,n,t){if(n.match(/^[\+\-]?[0-9]{1,3}%{1}$/)){var s=n.replace("%","");s>=-100&&100>=s||(n="-50%")}else n="-50%";switch(a){case"x":r.css({WebkitTransform:"translateX("+n+")",MozTransform:"translateX("+n+")",OTransform:"translateX("+n+")",MsTransform:"translateX("+n+")",Transform:"translateX("+n+")"}),t="a";break;case"y":r.css({WebkitTransform:"translateY("+n+")",MozTransform:"translateY("+n+")",OTransform:"translateY("+n+")",MsTransform:"translateY("+n+")",Transform:"translateY("+n+")"}),t="b";break;case"none":t="c"}return t}function n(r,a,n){switch(a){case"fadeIn":r.css({opacity:0}),n+="1";break;case"none":n+="5"}return n}function t(a,n){return a.match(/^[\+\-]?[0-9]{1,3}%{1}$/)?(a=a.replace("%",""),scrollarea=a>=-100&&100>=a?a*r(n).height()/100:r(n).height()):scrollarea=r(n).height(),scrollarea}function s(r,a,n){switch(a){case"a1":return r.css({WebkitTransition:"transform "+n+", opacity "+n,MozTransition:"transform "+n+", opacity "+n,OTransition:"transform "+n+", opacity "+n,MsTransition:"transform "+n+", opacity "+n,Transition:"transform "+n+", opacity "+n}),function(){r.css({WebkitTransform:"translateX(0)",MozTransform:"translateX(0)",OTransform:"translateX(0)",MsTransform:"translateX(0)",Transform:"translateX(0)",opacity:1})};case"a5":return r.css({WebkitTransition:"transform "+n,MozTransition:"transform "+n,OTransition:"transform "+n,MsTransition:"transform "+n,Transition:"transform "+n}),function(){r.css({WebkitTransform:"translateX(0)",MozTransform:"translateX(0)",OTransform:"translateX(0)",MsTransform:"translateX(0)",Transform:"translateX(0)"})};case"b1":return r.css({WebkitTransition:"transform "+n+", opacity "+n,MozTransition:"transform "+n+", opacity "+n,OTransition:"transform "+n+", opacity "+n,MsTransition:"transform "+n+", opacity "+n,Transition:"transform "+n+", opacity "+n}),function(){r.css({WebkitTransform:"translateY(0)",MozTransform:"translateY(0)",OTransform:"translateY(0)",MsTransform:"translateY(0)",Transform:"translateY(0)",opacity:1})};case"b5":return r.css({WebkitTransition:"transform "+n,MozTransition:"transform "+n,OTransition:"transform "+n,MsTransition:"transform "+n,Transition:"transform "+n}),function(){r.css({WebkitTransform:"translateY(0)",MozTransform:"translateY(0)",OTransform:"translateY(0)",MsTransform:"translateY(0)",Transform:"translateY(0)"})};case"c1":return r.css({WebkitTransition:"opacity "+n,MozTransition:"opacity "+n,OTransition:"opacity "+n,MsTransition:"opacity "+n,Transition:"opacity "+n}),function(){r.css({opacity:1})};case"c5":return r.css({WebkitTransition:"transform"+n+", opacity "+n,MozTransition:"transform"+n+", opacity "+n,OTransition:"transform"+n+", opacity "+n,MsTransition:"transform"+n+", opacity "+n,Transition:"transform"+n+", opacity "+n}),function(){r.css({WebkitTransform:"translateX(0)",MozTransform:"translateX(0)",OTransform:"translateX(0)",MsTransform:"translateX(0)",Transform:"translateX(0)",opacity:1})}}}function o(r,a,n){var t=null;return t=a==window?0:n.parent().offset().top,r=n.offset().top-t}r.fn.boxLoader=function(i){var e=r.extend({direction:"x",position:"-50%",effect:"fadeIn",duration:"1s",windowarea:"50%",container:window},i);return this.each(function(){var i,c,f=r(this),T=null,m=null;m=o(m,e.container,f),c=a(f,e.direction,e.position,c),c=n(f,e.effect,c),T=t(e.windowarea,e.container);var l=s(f,c,e.duration);r(window).resize(function(){m=o(m,e.container,f)}),r(e.container).scroll(function(){i=r(e.container).scrollTop()+T,i>m&&l()})})}}(jQuery);
</script>
<script>
!function(e){var t,n="mousewheel DOMMouseScroll wheel MozMousePixelScroll";"undefined"!=typeof Lethargy&&null!==Lethargy&&(t=new Lethargy);var i=function(){return Math.max(window.pageYOffset,window.document.body.scrollTop,window.document.documentElement.scrollTop)};e.smartscroll=function(o){var r=e.extend({},e.smartscroll.defaults,o);if(r.sectionSelector||(r.sectionSelector="."+r.sectionClass),("undefined"==typeof EventEmitter||null===EventEmitter||r.eventEmitter&&r.eventEmitter.constructor!==EventEmitter)&&(r.eventEmitter=null),r.bindSwipe)var a=null,l=null,s=function(e){var e=e.originalEvent||e;a=e.touches[0].clientX,l=e.touches[0].clientY},c=function(e){var e=e.originalEvent||e;if(a&&l){var t=e.touches[0].clientX,n=e.touches[0].clientY,i=a-t,o=l-n;Math.abs(i)>Math.abs(o)?i>0?r.eventEmitter.emitEvent("swipeLeft"):r.eventEmitter.emitEvent("swipeRight"):o>0?r.eventEmitter.emitEvent("swipeUp"):r.eventEmitter.emitEvent("swipeDown"),a=null,l=null}};var d,h,u=!1,w=[],v=!1,p=!1,f=window.location.hash,m=e(r.sectionWrapperSelector+":first"),E=function(){var t=i(),n=t+e(window).height();return n>d&&h>=t?!0:!1},g=function(t,n){u||(u=!0,e("body,html").stop(!0,!0).animate({scrollTop:t},n,function(){u=!1,r.eventEmitter&&r.eventEmitter.emitEvent("scrollEnd")}))};this.scroll=function(t){if(w){var n=i();if(r.eventEmitter){var o=y(n+e(window).height()/2),a=t?o+1:o-1;r.eventEmitter.emitEvent("scrollStart",[a])}for(var l=0;l<w.length;l++)if(n<w[l])return t?g(w[l],700):g(w[l-1]-e(window).height(),700),r.eventEmitter&&r.eventEmitter.emitEvent("scrollEnd"),!1}};var S=function(){var t=[];d=Math.round(m.position().top+parseInt(m.css("paddingTop"),10)+parseInt(m.css("borderTopWidth"),10)+parseInt(m.css("marginTop"),10)),h=Math.round(d+m.height(),10),t.push(d),e(r.sectionSelector).each(function(n,i){t.push(Math.round(d+e(i).position().top+e(i).outerHeight()))}),w=t},b=function(e){if(t)var n=t.check(e);if(!u)if(t){if(1===n)return"up";if(-1===n)return"down"}else{if(e.originalEvent.wheelDelta>0||e.originalEvent.detail<0)return"up";if(e.originalEvent.wheelDelta<0||e.originalEvent.detail>0)return"down"}return!1},y=function(e){for(var t=0;t<w.length;t++)if(e<=w[t])return t;return w.length},k=function(){e(window).bind(n,function(t){var n=b(t);r.dynamicHeight&&S(),E();var o=i(),a=o+e(window).height();if(a>d&&h>=o){var l=y(o),s=y(o+e(window).height()/2),c=y(a);l===c&&r.innerSectionScroll||(t.preventDefault(),t.stopPropagation(),n&&("up"===n?(r.toptotop?g(w[s-2]+1,r.animationSpeed):g(w[s-1]-e(window).height(),r.animationSpeed),r.eventEmitter&&r.eventEmitter.emitEvent("scrollStart",[s-1])):"down"===n&&(g(w[s]+1,r.animationSpeed),r.eventEmitter&&r.eventEmitter.emitEvent("scrollStart",[s+1]))))}})},H=function(){e(window).unbind(n)},M=function(){var t;if(i()+e(window).height()/2<d)t=r.headerHash;else{var n=y(i()+e(window).height()/2);void 0!==n&&(t=e(r.sectionSelector+":nth-of-type("+(n+1)+")").data("hash"))}("undefined"==typeof t||window.location.hash!=="#"+t)&&("undefined"==typeof t&&(t=r.headerHash),r.keepHistory?window.location.hash=t:window.location.replace(window.location.href.split("#")[0]+"#"+t))};if(m.css({position:"relative"}),setTimeout(function(){if(S(),r.autoHash&&(null===r.eventEmitter||r.hashContinuousUpdate?e(window).bind("scroll",M):r.eventEmitter.addListener("scrollEnd",M)),r.initialScroll&&f.length>0){var t=e('[data-hash="'+f.substr(1)+'"]');t.length>0&&g(t[0].offsetTop+d,0)}},50),e(window).bind("resize",S),null!==r.breakpoint&&r.breakpoint===parseInt(r.breakpoint,10)&&r.breakpoint>0&&(v=!0),"vp"==r.mode)if(r.ie8){var T=function(){e(r.sectionSelector).css({height:e(window).height()})};T(),e(window).bind("resize",T)}else e(r.sectionSelector).css({height:"100vh"});if(r.sectionScroll&&(v&&e(window).bind("resize",function(t){if(e(window).width()<r.breakpoint){if(!p)return H(),p=!0,!1}else p&&(k(),p=!1)}),k()),r.bindSwipe&&(e(window).on("touchstart",s),e(window).on("touchmove",c)),r.bindKeyboard){var D=function(t){t=t.originalEvent||t,r.dynamicHeight&&S();var n=i(),o=n+e(window).height();if(E()){var a=y(n),l=y(n+e(window).height()/2),s=y(o);if(a!==s||!r.innerSectionScroll)switch(t.which){case 38:t.preventDefault(),t.stopPropagation(),r.toptotop?g(w[l-2]+1,r.animationSpeed):g(w[l-1]-e(window).height(),r.animationSpeed),r.eventEmitter&&r.eventEmitter.emitEvent("scrollStart",[l-1]);break;case 40:t.preventDefault(),t.stopPropagation(),g(w[l]+1,r.animationSpeed),r.eventEmitter&&r.eventEmitter.emitEvent("scrollStart",[l+1]);break;default:return}}};e(window).on("keydown",D)}return this},e.smartscroll.defaults={animationSpeed:700,autoHash:!0,breakpoint:null,initialScroll:!0,headerHash:"header",keepHistory:!1,mode:"vp",sectionClass:"section",sectionSelector:null,sectionScroll:!0,sectionWrapperSelector:".section-wrapper",eventEmitter:null,dynamicHeight:!1,ie8:!1,hashContinuousUpdate:!0,innerSectionScroll:!0,toptotop:!1,bindSwipe:!0,bindKeyboard:!0}}(jQuery);
</script>
<script>
 jQuery(document).ready(function($){
$(".animate_left").boxLoader({
	    direction:"x",
	    position: "-50%",
	    effect: "fadeIn",
	    duration: "1s",
	    windowarea: "50%"
});
$(".animate_right").boxLoader({
	    direction:"x",
	    position: "50%",
	    effect: "fadeIn",
	    duration: "1s",
	    windowarea: "50%"
});
$(".services_indivs").boxLoader({
	    direction:"y",
	    position: "100%",
	    effect: "none",
	    duration: "1s",
	    windowarea: "50%"
});
});

</script>
<script>
jQuery(document).ready(function($){
		$.smartscroll({
			sectionWrapperSelector: ".section-wrapper",
			sectionClass: "section",
			headerHash: "a",
			breakpoint: 780
		});});
	</script>