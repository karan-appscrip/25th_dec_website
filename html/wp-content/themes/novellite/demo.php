<?php
/*
  Template Name: Fullwidth Page YOUTUBE
 */
?>
<?php get_header(); ?>
<div class="page_heading_container">
  <div class="container">
        <div class="row">
		<div class="col-md-12">
		<div class="page_heading_content">
		<h1 class="wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s;">Why Uber when you can <span style="color:#e4d315">Roadyo</span> ? Roadyo is a fully integrated
taxi booking software for your taxi business on the cloud!</h1>
                 <a href ="#" id="products_button1" class="wow fadeInRight" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInRight;">GET IN TOUCH</a>
		</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>


<div class="page-container">
    <div class="container-fluid" style="padding-right: 0px; 
     padding-left: 0px; ">
        <div class="row">
            <div class="page-content">
             <div class="col-md-12">
              <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
				<?php the_content(); ?>	
			<?php endif; ?>	  
				</div>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<?php get_footer(); ?>
