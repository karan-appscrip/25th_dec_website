<?php
/*
  Template Name: Fullwidth Page
 */
?>

<?php get_header(); ?>
<div class="page_heading_container">
  <div class="container">
        <div class="row">
		<div class="col-md-12">
		<div class="page_heading_content">
		<h1 class="wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s;">Why Uber when you can <span style="color:#e4d315">Roadyo</span> ? Roadyo is a fully integrated
taxi booking software for your taxi business on the cloud!</h1>
                 <a href ="#" id="products_button1" class="wow fadeInRight" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInRight;">GET IN TOUCH</a>
		</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>


<div class="page-container">
    <div class="container-fluid" style="padding-right: 0px; 
     padding-left: 0px; ">
        <div class="row">
            <div class="page-content">
             <div class="col-md-12">
              <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
				<?php the_content(); ?>	
			<?php endif; ?>	  
				</div>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php get_template_part( 'template/section_pricing');  ?>
<?php get_template_part( 'template/section_countactus');  ?>
<?php get_footer(); ?>
<script type="text/javascript">
     jQuery.noConflict();

var lastId,
    topMenu = jQuery(".listApp"),
    topMenuHeight = topMenu.outerHeight()+20,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = jQuery(jQuery(this).attr("href"));
      if (item.length) { return item; }
    });
// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
  var href = jQuery(this).attr("href"),
      offsetTop = href === "#" ? 0 : jQuery(href).offset().top-topMenuHeight+1;
  jQuery('html, body').stop().animate({ 
      scrollTop: offsetTop
  }, 500);
  e.preventDefault();
});
// Bind to scroll
jQuery(window).scroll(function(){
   // Get container scroll position
   var fromTop = jQuery(this).scrollTop()+topMenuHeight
   // Get id of current scroll item
   var cur = scrollItems.map(function(){
     if (jQuery(this).offset().top < fromTop)
       return this;
   });
   // Get the id of the current element
   cur = cur[cur.length-1];   var id = cur && cur.length ? cur[0].id : "";
   if (lastId !== id) {
       lastId = id;
       // Set/remove active class
       menuItems
         .parent().removeClass("active")
         .end().filter("[href='#"+id+"']").parent().addClass("active");
   }                   
});
</script>
<script type="text/javascript">
function changeImage(element) {
alert("hi");
document.getElementById('app_overimg').src = element;
}
</script>