<?php
/*
  Template Name: checkout_page
 */
?>

<?php get_header(); ?>
<style>
nav.woocommerce-MyAccount-navigation li:hover {
    border-bottom: 2px solid grey;
   
    transition: width 2s, height 4s;
}
.woocommerce-MyAccount-content {
    margin-top: 5%;
}
nav.woocommerce-MyAccount-navigation li {
    list-style: none;
    display: inline;
    font-size: 20px;
    border-bottom: 2px solid;
    margin: 0% 8% 0% 0%;
    margin-bottom: 15%;
    padding: 0% 0% 1% 0%;
}
.page-container {
    margin-bottom: 10px!important;    
    margin-left: 10%;
    margin-right: 10%;   
}
.navbar-default .nav li a{color:#000} 
.navbar-default.navbar-fixed-top {
    z-index: 999999!important;
}

input.woocommerce-Button.button {
    background: #108ffd;
    width: 30%;
    outline: 0px;
}
form.login {
    display: none;
}
.woocommerce .woocommerce-info, .woocommerce-page .woocommerce-info {
    border-top: 3px solid #1e85be;
    border-radius: 0;
    box-shadow: none;
}
nav.woocommerce-MyAccount-navigation li {
    list-style: square!important;
    display: inline;
    font-size: 17px;
    border-bottom: 0px solid;
    margin: 0% 8% 0% 0%;
    margin-bottom: 1%;
    padding: 0% 0% 1% 0%;
    margin-left: -4%;
    width: 100%;
    /* right: 0; */
}
.woocommerce form .form-row .input-text, .woocommerce-page form .form-row .input-text {
    border: none;
    border-bottom: 1px solid #cfc9c9;
}
span.woocommerce-Price-amount.amount {
    font-family: sans-serif;
}
@media only screen and (min-width: 200px) and (max-width: 767px){
	
	nav.woocommerce-MyAccount-navigation li {
     list-style: square; 
     display: block; 
     font-size: 14px; 
    margin: 0% 8% 0% 0%;
    margin-bottom: 4%;
    padding: 0% 0% 1% 0%;
    border-bottom: transparent;
}
.vc_column_container>.vc_column-inner {
    box-sizing: border-box;
    padding-left: 15px;
    padding-right: 15px;
    width: 100%!important;
}
.woocommerce form .form-row .input-text, .woocommerce-page form .form-row .input-text {
    border: none;
    border-bottom: 1px solid #bbb9b9;
}
h2 {
    font-size: 20px;
}
td.product-name {
    font-size: 12px;
    font-weight: 600;
}
span.woocommerce-Price-amount.amount {
    font-family: sans-serif;
}
p {
    font-size: 14px;   
}
}

.vc_column_container>.vc_column-inner {
    width: 98%!important;
}
.wpb_raw_code.wpb_content_element.wpb_raw_html.buy_button {
    float: right;
}
#place_order{    display: block;
    position: relative;
    padding: 0 12px;
    height: 30px;
    line-height: 30px;
    background: #1275ff;
    background-image: -webkit-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
    background-image: -moz-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
    background-image: -ms-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
    background-image: -o-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
    background-image: -webkit-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
    background-image: -moz-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
    background-image: -ms-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
    background-image: -o-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
    background-image: linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
    font-size: 14px;
    color: #fff;
    font-weight: bold;
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
    -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.25);
    -moz-box-shadow: inset 0 1px 0 rgba(255,255,255,0.25);
    -ms-box-shadow: inset 0 1px 0 rgba(255,255,255,0.25);
    -o-box-shadow: inset 0 1px 0 rgba(255,255,255,0.25);
    box-shadow: inset 0 1px 0 rgba(255,255,255,0.25);
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    -ms-border-radius: 4px;
    -o-border-radius: 4px;
    border-radius: 4px;
	text-transform: capitalize;}
</style>
<h1 style="    text-align: center;
    font-weight: bolder;
    margin-top: 100px;
    font-size: 28px;
    letter-spacing: 1px;
    color: #7d7bab;
    padding-bottom: 10px;
    border-bottom: solid 1px #ececec;">CHECKOUT </h1>

   


<div class="page-container">
    <div class="container-fluid" style="padding-right: 0px; 
     padding-left: 0px; ">
        <div class="row">
            <div class="page-content">
             <div class="col-md-12">
              <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
				<?php the_content(); ?>	
			<?php endif; ?>	  
				</div>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>
</div>
<script>
 jQuery(document).ready(function($) {
  function changeText(){
    $("button.button").html($("tr.order-total span.woocommerce-Price-amount.amount").html());
}
 setInterval(changeText,5);});
</script>

<?php get_footer(); ?>
