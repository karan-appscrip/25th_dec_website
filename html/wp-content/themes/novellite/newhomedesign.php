<?php
/*
  Template Name: NEW HOMEPAGE
 */
?> 
<?php get_header(); ?>
<div id="pop1" class="simplePopup row" style="position: absolute; top: -100%;">
  <!-- <div class="col-xs-12" style="text-align:center" id="popup_head">
    <h6>LETS CREATE SOMETHING TOGETHER</h6>
  </div> -->
  <div class=" col-md-6 hidden-xs hidden-sm">
    <!-- <h2 class="popup_sub">MEET US HERE</h2> -->
    <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/index/map.png" class="img-responsive" id="map_image">

    <div class='pin bounce'></div>
    <div class='pulse'></div>
    <div class='pin bounce' id="pin2_marker"></div>
    <div class='pulse' id="pin2b_marker"></div>
  </div>
  <div class=" col-md-4 col-md-offset-1">
    <div style="width:95%" class="slidedown_popup">
      <h2 class="popup_sub">Transforming your ideas to reality.</h2>
      <?php echo do_shortcode( '[contact-form-7 id="16189" title="Slide down popup new Home page"]' ); ?>
    </div>
  </div>
</div>







 <!-- SWEET ALERT FOR POPUP SWAL -->
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->

<script src="https://unpkg.com/sweetalert2@7.17.0/dist/sweetalert2.all.js"></script>


<style>

    /* @font-face {

font-family: "Circular book";
src: url("https://s3-ap-southeast-1.amazonaws.com/appscrip/appscriphome/fonts/CIRCULARAIR-BOOK.OTF");

font-family: "Circular Bold";
src: url("https://s3-ap-southeast-1.amazonaws.com/appscrip/appscriphome/fonts/CIRCULARAIR-BOLD.OTF"); */




}

@import url('https://fonts.googleapis.com/css?family=Hind:400,700');
/* @import url('https://fonts.googleapis.com/css?family=Abril+Fatface');
@import url('https://fonts.googleapis.com/css?family=Roboto'); */

/* Css for Slick Slider*/

 .slick-list,.slick-slider,.slick-track{position:relative;display:block}.slick-loading .slick-slide,.slick-loading .slick-track{visibility:hidden}.slick-slider{box-sizing:border-box;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;-webkit-touch-callout:none;-khtml-user-select:none;-ms-touch-action:pan-y;touch-action:pan-y;-webkit-tap-highlight-color:transparent}.slick-list{overflow:hidden;margin:0;padding:0}.slick-list:focus{outline:0}.slick-list.dragging{cursor:pointer;cursor:hand}.slick-slider .slick-list,.slick-slider .slick-track{-webkit-transform:translate3d(0,0,0);-moz-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);-o-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}.slick-track{top:0;left:0;margin-left:auto;margin-right:auto}.slick-track:after,.slick-track:before{display:table;content:''}.slick-track:after{clear:both}.slick-slide{display:none;float:left;height:auto;min-height:1px}[dir=rtl] .slick-slide{float:right}.slick-slide img{display:block}.slick-slide.slick-loading img{display:none}.slick-slide.dragging img{pointer-events:none}.slick-initialized .slick-slide{display:block}.slick-vertical .slick-slide{display:block;height:auto;border:1px solid transparent}.slick-arrow.slick-hidden{display:none}.slider{width:85%;margin:50px auto}.slick-slide{margin:40px 20px;transition:all ease-in-out .3s;opacity:.2}.slick-slide img{width:100%}.slick-next:before,.slick-prev:before{color:#000}.slick-active{opacity:.9}.slick-current{opacity:1;background-color:#0088fc;font-size:18px;color:#fff;border-radius:1rem}button.slick-next.slick-arrow,button.slick-prev.slick-arrow{display:none!important}.slick-list.draggable{padding:0 17px!important}

/* div#seventhslider1{display:none;} */

/* css for swal */

.swal2-popup{width: 50em;}
.swal2-popup .swal2-title{font-family: 'Hind', sans-serif;
    font-weight: 700;
    color: #484848;
    font-size: 25px;
    text-transform: none;
    position: relative;
    display: block;
    padding: 13px 16px;
    line-height: normal;
    text-align: center;
    margin-bottom: 0;}
.swal2-popup #swal2-content { font-family: 'Hind', sans-serif;
    font-size: 14px;
    color: #787C88;
    line-height: 25px;
    vertical-align: top;
    text-align: left;
    display: inline-block;
    margin: 0;
    padding: 0 10px 0 20px;
    font-weight: 400;
    max-width: calc(100% - 20px);
    overflow-wrap: break-word;
    box-sizing: border-box;}
.swal2-popup .swal2-image { max-width: 100px; margin: 1.25em auto;}
.swal2-popup .swal2-styled.swal2-confirm {background-color: #0088FC!important;  border-radius:0px; color: white; font-family: 'Hind', sans-serif; font-weight: 700; border: 1px solid #0088FC;  font-size: 18px;
    -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc;
    box-shadow: 0 8px 6px -6px #0088fc; padding: 10px 24px;    margin-bottom: 20%;}

img.testimg {    width: auto;    box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0); margin: -12% 0 0 14%; border-radius: 5px; background: white;}
.rankimg { width: 280px;    margin: 8% 0 0 0;}
p.testi { font-size: 12px;letter-spacing: 0.5px; padding: 4% 2% 0% 7%;}
p.testiname { font-size: 10px; text-align: right; padding: 0 5% 5% 0;}
.slick-slide.slick-current.slick-active.slick-center p { color: white; font-size:15px;} 
.slick-slide.slick-current.slick-active.slick-center p.testiname { color: white; font-size:12px;}
.slick-slide.slick-current.slick-active.slick-center .testimonial {box-shadow: none; border-radius: 1rem;}
.testimonial {     box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0); border-radius: 1rem;}
.slick-slide.slick-current.slick-active.slick-center i.fa.fa-quote-right { color: white;}
i.fa.fa-quote-right { float: right; padding: 18% 16% 0 0; font-size: 30px; color: #0088fc;}

/* End Slick Slider */

/* Css for Slider down Popup*/
.simplePopup{background: #0088FC;}
h2.popup_sub {
    font-weight: normal;
    font-size: 50px;
    line-height: 74px;
    text-align: left;
    color: white;
    margin-top: 50px; margin-bottom: 39px;}
    input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform12::-webkit-input-placeholder{color:white!important;} /* Chrome/Opera/Safari */
    input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform12::-moz-placeholder{color:white!important;} /* Firefox 19+ */
    input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform12:-ms-input-placeholder{color:white!important;} /* IE 10+ */
    input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform12:-moz-placeholder{color:white!important;} /* Firefox 18- */

    textarea.wpcf7-form-control.wpcf7-textarea.commonform12::-webkit-input-placeholder{color:white;} /* Chrome/Opera/Safari */
    textarea.wpcf7-form-control.wpcf7-textarea.commonform12::-moz-placeholder{color:white;} /* Firefox 19+ */
    textarea.wpcf7-form-control.wpcf7-textarea.commonform12:-ms-input-placeholder{color:white;} /* IE 10+ */
    textarea.wpcf7-form-control.wpcf7-textarea.commonform12:-moz-placeholder{color:white;} /* Firefox 18- */

    input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform12, select.wpcf7-form-control.wpcf7-select.wpcf7-validates-as-required.commonform12 {
    border: solid 1px #ffffff;
    padding: 0% 0 0 6%;
    font-family: 'Hind', sans-serif;
    font-size: 16px;
    background: #0088fc;
    width: 100%;
    margin-bottom: 10px;
    border-top-left-radius: 20px;
    height: 58px;
    outline: 0px;
    color: white;
    }
    textarea.wpcf7-form-control.wpcf7-textarea.commonform12{
    width: 100%;
    max-width: 100%;
    background: #0088fc!important;
    height: 134px;
    border-top-left-radius: 20px;
    padding: 3% 0 0 6%;
    border: solid 1px #ffffff;
    outline: 0px;
    font-family: 'Hind', sans-serif;
    color: white;
    }
input.wpcf7-form-control.wpcf7-submit.btnSubmit12{
    margin: 7% 0 0 0;

    background-color: #fff;
    color: #0088fc;
    float: right;
    font-family: 'Hind', sans-serif;     font-weight: 700;
    border: 1px solid #0088FC;
    padding: 0% 5%;
    font-size: 24px;
    box-shadow: 0 19px 25px rgba(0,0,0,0.30), 0 4px 8px rgba(0,0,0,0.22);
    }

div#pop1 {
    overflow: hidden;
    left: 0px!important;
}
.simplePopupClose {
    float: left;
    cursor: pointer;
    margin-left: 15px;
    padding: 0% 1%;
    margin-bottom: 10px;
    border: 1px solid white;
    color: white;
    margin: 2% 0 0 2%;
}
.simplePopupClose:hover {
    background: white;
    color: #0088fc;
}

/* End Slider Popup */

.navbar-default .nav li a{color: #787C88;font-family: 'Hind', sans-serif;}
.outer-footer.kks {display: none;}
footer{display:none;}





 /* Media query for 1281 */
@media (min-width: 1281px) {
  
/* h1{font-family: 'Hind', sans-serif; font-weight: 700; width: 60%; font-size: 50px; color: #787C88;  padding: 0 0 0 5.5%;  margin: 10% 0 0 0; } */
h1 {
    font-family: 'Hind', sans-serif;
    width: 55%;
    padding: 0 0 0 5.5%;
    margin: 10% 0 0 0;
    font-size: 40px;
    font-weight: 300;
    line-height: 50px;
    color: #000;
}
h2 { font-family: 'Hind', sans-serif;     font-weight: 700; font-size: 40px; color: #484848; }
p { font-family: 'Hind', sans-serif; font-size: 18px;  color: #787C88;}

/****************************** First Division ****************************/
div#firstbanner{background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+236.png") , url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); 
    background-size: 60% 100% , auto; background-repeat: no-repeat , no-repeat; background-position: right, 4% 22%; }
iframe { border: 15px solid white;    box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0);}
.appscripvideo{ margin: 0 0 0 5.5%;     width: 30%;}
button.gtchbtn { margin: 3% 0 0 5.5%; background-color: #0088FC; color: white; font-family: 'Hind', sans-serif;     font-weight: 700; 
    border: 1px solid #0088FC; padding: 0.5% 1%;    font-size: 18px;
    -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc;
    box-shadow: 0 8px 6px -6px #0088fc;}
ul.list-inline.sociallogo { margin: 4% 0 0 5.5%;}    
ul.list-inline.sociallogo li {padding: 0 4% 0 0; font-size: 20px; color: #484848;}
ul.list-inline.sociallogo li a:hover { color: #0088fc!important;}

/****************************** Second Division ****************************/
.scndslidertext {padding: 0 0 0 5.5%;}
.scndslidertext h2 { margin: 8% 0 6% 0; font-family: 'Hind', sans-serif;     font-weight: 700;  font-size: 40px; color: #fff;}
.scndslidertext p {  font-family: 'Hind', sans-serif; font-size: 18px;color: #ffffff;}
img.apprening { margin: 15% 0 0 0;}
.col-md-6.scndrwimg { margin-top: 2%;}
.scndsliderslider.wpb_column.vc_column_container.vc_col-sm-6 {
    background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+22.png);
    background-repeat: no-repeat; background-size: auto; background-position: 88% 97%; padding: 0 0 4% 0;}

/****************************** Third Division ****************************/
div#thirdslider {padding: 2% 5.5%; background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); background-size: auto; background-repeat:  no-repeat; background-position: 4% 0%;     margin: 1% 0 0 0; }
#thirdslider .vc_column-inner{width:100%!important;}
#thirdslider h3{font-family: 'Hind', sans-serif;     font-weight: 700; color:#484848;font-size: 25px;}
#thirdslider p{font-family: 'Hind', sans-serif; font-size: 14px;color: #787C88; line-height: 28px;}
#thirdslider h2{margin:0 0 0 5%;}
button.newbtn {
    margin: 0 -21% 0 0%; border: 1px solid #0088fc; float: right; padding: 1% 6%;  font-size: 20px;  font-family: 'Hind', sans-serif;     font-weight: 700; color: white; background-color: #0088fc;}

.parent{display: flex;}
.parentChild{ display: flex; flex-direction: column; justify-content: space-between; }
.child{ flex: 1;  background-size: contain; background-repeat: no-repeat; background-position: center center;}
.child1{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+172.png); }
.child1.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+172.webp);}

.child2{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+171.png);}
.child2.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+171.webp);}

.child3{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+174.png); }
.child3.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+174.webp);}

.child4{ height: 480px; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+175.png); }
.child4.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+175.webp);}

.pro2, .pro3 { padding: 8% 25% 0 12%;}
.pro1, .pro4 {padding: 6% 25% 0 20%;}

.child21{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+176.png);}
.child21.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+176.webp);}

.child31{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+178.png); }
.child31.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+178.webp);}

.child11{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+177.png); }
.child11.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+177.webp);}

.child41{ height: 478px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+179.png); }
.child41.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+179.webp);}

.child22{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+182.png);}
.child22.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+182.webp);}

.child32{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+183.png); }
.child32.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+183.webp);}

.child12{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+180.png); }
.child12.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+180.webp);}

.child42{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+181.png); }
.child42.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+181.webp);}

.child13{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+194.png); }
.child13.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+194.webp);}

.child23{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+193.png);}
.child23.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+193.webp);}

.child33{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+195.png); }
.child33.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+195.webp);}

.child43{ height: 480px; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+196.png); }
.child43.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+196.webp);}

 

/****************************** forth Division ****************************/
div#frthslider { padding: 5% 0 5% 5.5%; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+25.png); background-size: auto; background-repeat: no-repeat; background-position: 61% 2%;}
.frthslidervd iframe { height: 100%!important;  width: 100%!important;}
.frthslidertext{padding:0 8% 0 1%;}
.frthslidertext h2 {  margin-bottom: 10%;}

/****************************** fifth Division ****************************/
div#fifthslider { background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+27.png); background-size: 17%; background-repeat: no-repeat; padding: 4% 0 0 0; background-position: 36% 0;}
#fifthslider .vc_column-inner {width:100%!important;}
#fifthslider h2{margin: 0 0 0 10%;}

/****************************** sixth Division ****************************/
div#sixthslider {padding: 5% 3% 5% 5.5%; background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); background-size: auto; background-repeat:  no-repeat; background-position: 95% 0%; }
.usaaddress, .indaddress {box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0); padding: 6% 0 6% 0; background-color:white; }
.usaaddress{margin: 0 0 10% 0; display: flex;}
.indaddress{margin:0px; display: flex;}
.addimg { display: flex; align-items: center;  justify-content: center;}
#sixthslider h2{margin: 4% 0 5% 10%;}
#sixthslider h4{color: #484848; font-size: 22px;  font-weight: 700; font-family: 'Hind', sans-serif;}
#sixthslider p{line-height: 30px;}
.contactform {box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0);background-color: white;}

input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform1, select.wpcf7-form-control.wpcf7-select.wpcf7-validates-as-required.commonform1, input.wpcf7-form-control.wpcf7-quiz.commonform1 {
    border: solid 1px #CECECE; padding: 15px; font-size: 17px; width: 80%; margin-left: 10%; margin-bottom: 10px; border-top-left-radius: 30px; height: 50px; outline: 0px; }
textarea.wpcf7-form-control.wpcf7-textarea.commonform1{ width: 80%; margin-left: 10%; height: 50px;  border-top-left-radius: 30px; border: solid 1px #ccc1c1; outline: 0px; padding-left: 20px;}
input.wpcf7-form-control.wpcf7-submit.btnSubmit1{margin: 3% 0 0 5.5%;   margin: 3% 10% 3% 0; background-color: #0088FC; color: white; float: right;
    font-family: 'Hind', sans-serif;     font-weight: 700; border: 1px solid #0088FC; padding: 0.5% 8%; font-size: 18px; -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc; box-shadow: 0 8px 6px -6px #0088fc; }

/****************************** Seventh Division ****************************/
/* div#seventhslider {    margin: -15% 0 0 0; padding: 6% 3% 2% 3%;}
#seventhslider .vc_column-inner {width:100%!important;}
ul.footersubmenu { padding: 0; list-style: none;}
ul.footersubmenu h4 { color: white; font-size: 14px; line-height: 19px; font-family: 'Roboto', medium; margin: 0 0 15% 0;} 
ul.footersubmenu li {margin: 10% 0;}
ul.footersubmenu li a { color: white; font-size: 13px;font-family: 'Roboto', Regular;}

ul.footersocialmenu li {  color: white;  margin: 0 3%; font-size: 22px;}
ul.footersocialmenu { margin: 6% 0 0 0;}
ul.footersocialmenu li a {color:white;}
ul.footersocialmenu li a:hover { color: #4a4a4a;}
p.copyright { font-size: 14px; color: white; font-family: 'Hind', sans-serif;      margin: 27% 0 0 4%;} */

/****************************** Eight Division ****************************/
div#eightslider { padding: 1% 3% 3% 3%; margin: 4% 0 0 0;}
#eightslider .vc_column-inner {width:100%!important;}
#eightslider h2{font-family: 'Hind', sans-serif; width:60%;
    font-weight: 700;    margin-bottom: 5%;
    font-size: 40px;
    color: #fff;}

div#eightslider p { font-family: 'Hind', sans-serif; font-size: 14px; color: #787C88; line-height: 28px; text-align: center; margin: -8% 0 0 0;}
.businessfirstrow{margin-bottom:4%}
.businessfirstrow, .businesssecondrow{ display: flex; }

.business {border-radius: 10px; background-color: white; margin: 0 2%; width: 16%;}
.business p {padding: 4% 0 6% 0;}
.business img {     width: 50px;     padding: 10% 0; }

}

/* Media query for 1025 to 1280 */
@media (min-width: 1025px) and (max-width: 1280px) {


h1{font-family: 'Hind', sans-serif;     font-weight: 700; width: 60%; font-size: 45px; color: #787C88;  padding: 0 0 0 5.5%;  margin: 10% 0 0 0; }
h2 { font-family: 'Hind', sans-serif;     font-weight: 700; font-size: 35px; color: #484848; }
p { font-family: 'Hind', sans-serif; font-size: 14px;  color: #787C88;}

/****************************** First Division ****************************/
div#firstbanner{background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+236.png") , url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); 
    background-size: 60% 100% , auto; background-repeat: no-repeat , no-repeat; background-position: right, 4% 22%; }
iframe { border: 15px solid white;    box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0);}
.appscripvideo{ margin: 0 0 0 5.5%;     width: 40%;}
button.gtchbtn { margin: 3% 0 0 5.5%; background-color: #0088FC; color: white; font-family: 'Hind', sans-serif;     font-weight: 700; 
    border: 1px solid #0088FC; padding: 0.5% 1%;    font-size: 18px;
    -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc;
    box-shadow: 0 8px 6px -6px #0088fc;}
ul.list-inline.sociallogo { margin: 4% 0 0 5.5%;}    
ul.list-inline.sociallogo li {padding: 0 4% 0 0; font-size: 20px; color: #484848;}
ul.list-inline.sociallogo li a:hover { color: #0088fc!important;}

/****************************** Second Division ****************************/
.scndslidertext {padding: 0 0 0 5.5%;}
.scndslidertext h2 { margin: 8% 0 6% 0; font-family: 'Hind', sans-serif;     font-weight: 700;  font-size: 35px; color: #fff;}
.scndslidertext p {  font-family: 'Hind', sans-serif; font-size: 16px;color: #ffffff;}
img.apprening { margin: 15% 0 0 0;}
.col-md-6.scndrwimg { margin-top: 2%;}
.scndsliderslider.wpb_column.vc_column_container.vc_col-sm-6 {
    background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+22.png);
    background-repeat: no-repeat; background-size: auto; background-position: 88% 97%; padding: 0 0 4% 0;}

/****************************** Third Division ****************************/
div#thirdslider {padding: 1% 5.5%; background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); background-size: auto; background-repeat:  no-repeat; background-position: 4% 0%;     margin: 1% 0 0 0; }
#thirdslider .vc_column-inner{width:100%!important;}
#thirdslider h3{font-family: 'Hind', sans-serif;     font-weight: 700; color:#484848;font-size: 22px;}
#thirdslider p{font-family: 'Hind', sans-serif; font-size: 14px;color: #787C88; line-height: 28px;}
#thirdslider h2{margin:0 0 0 5%;}

button.newbtn {margin: 0 -20% 0 0%; border: 1px solid #0088fc; float: right; padding: 1% 6%;  font-size: 17px;  font-family: 'Hind', sans-serif;     font-weight: 700; color: white; background-color: #0088fc;}

.parent{display: flex;}
.parentChild{ display: flex; flex-direction: column; justify-content: space-between; }
.child{ flex: 1;  background-size: contain; background-repeat: no-repeat; background-position: center center;}





.child1{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+172.png); }
.child1.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+172.webp);}

.child2{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+171.png);}
.child2.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+171.webp);}

.child3{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+174.png); }
.child3.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+174.webp);}

.child4{ height: 480px; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+175.png); }
.child4.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+175.webp);}

.pro2, .pro3 { padding: 11% 25% 0 12%;}
.pro1, .pro4 {padding: 12% 21% 0 14%;}

.child21{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+176.png);}
.child21.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+176.webp);}

.child31{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+178.png); }
.child31.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+178.webp);}

.child11{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+177.png); }
.child11.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+177.webp);}

.child41{ height: 478px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+179.png); }
.child41.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+179.webp);}

.child22{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+182.png);}
.child22.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+182.webp);}

.child32{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+183.png); }
.child32.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+183.webp);}

.child12{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+180.png); }
.child12.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+180.webp);}

.child42{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+181.png); }
.child42.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+181.webp);}

.child13{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+194.png); }
.child13.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+194.webp);}

.child23{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+193.png);}
.child23.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+193.webp);}

.child33{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+195.png); }
.child33.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+195.webp);}

.child43{ height: 480px; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+196.png); }
.child43.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+196.webp);}
 

/****************************** forth Division ****************************/
div#frthslider { padding: 5% 0 5% 5.5%; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+25.png); background-size: auto; background-repeat: no-repeat; background-position: 70% -14%;}
.frthslidervd iframe { height: 100%!important;  width: 100%!important;}
.frthslidertext{padding:0 8% 0 1%;}
.frthslidertext h2 {  margin-bottom: 10%;}

/****************************** fifth Division ****************************/
div#fifthslider { background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+27.png); background-size: 17%; background-repeat: no-repeat; padding: 5% 0 0 0; background-position: 36% 0;}
#fifthslider .vc_column-inner {width:100%!important;}
#fifthslider h2{margin: 0 0 0 10%;}

/****************************** sixth Division ****************************/
div#sixthslider {padding: 5% 3% 5% 5.5%; background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); background-size: auto; background-repeat:  no-repeat; background-position: 95% 0%; }
.usaaddress, .indaddress {box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0); padding: 6% 0 6% 0; background-color:white; }
.usaaddress{margin: 0 0 10% 0; display: flex;}
.indaddress{margin:0px; display: flex;}
.addimg { display: flex; align-items: center;  justify-content: center;}
#sixthslider h2{margin: 4% 0 5% 10%;}
#sixthslider h4{color: #484848; font-size: 18px;  font-weight: 700; font-family: 'Hind', sans-serif;}
#sixthslider p{line-height: 24px; font-size:15px;}
.contactform {box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0);background-color: white;}

input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform1, select.wpcf7-form-control.wpcf7-select.wpcf7-validates-as-required.commonform1, input.wpcf7-form-control.wpcf7-quiz.commonform1 {
    border: solid 1px #CECECE; padding: 15px; font-size: 15px; width: 80%; margin-left: 10%; margin-bottom: 10px; border-top-left-radius: 30px; height: 50px; outline: 0px; }
textarea.wpcf7-form-control.wpcf7-textarea.commonform1{ width: 80%; margin-left: 10%; height: 50px;  border-top-left-radius: 30px; border: solid 1px #ccc1c1; outline: 0px; padding-left: 20px; font-size:15px;}
input.wpcf7-form-control.wpcf7-submit.btnSubmit1{margin: 3% 0 0 5.5%;   margin: 3% 10% 3% 0; background-color: #0088FC; color: white; float: right;
    font-family: 'Hind', sans-serif;     font-weight: 700; border: 1px solid #0088FC; padding: 0.5% 8%; font-size: 18px; -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc; box-shadow: 0 8px 6px -6px #0088fc; }

/****************************** Seventh Division ****************************/
/* div#seventhslider {    margin: -15% 0 0 0; padding: 6% 3% 2% 3%;}
#seventhslider .vc_column-inner {width:100%!important;}
ul.footersubmenu { padding: 0; list-style: none;}
ul.footersubmenu h4 { color: white; font-size: 14px; line-height: 19px; font-family: 'Roboto', medium; margin: 0 0 15% 0;} 
ul.footersubmenu li {margin: 10% 0;}
ul.footersubmenu li a { color: white; font-size: 13px;font-family: 'Roboto', Regular;}

ul.footersocialmenu li {  color: white;  margin: 0 2%; font-size: 21px;}
ul.footersocialmenu { margin: 6% 0 0 0;}
ul.footersocialmenu li a {color:white;}
ul.footersocialmenu li a:hover { color: #4a4a4a;}
p.copyright { font-size: 14px; color: white; font-family: 'Hind', sans-serif;      margin: 27% 0 0 4%;} */
 

 /****************************** Eight Division ****************************/
 div#eightslider { padding: 1% 3% 3% 3%; margin: 4% 0 0 0;}
#eightslider .vc_column-inner {width:100%!important;}
#eightslider h2{font-family: 'Hind', sans-serif; width:60%;
    font-weight: 700;
    font-size: 35px;    margin-bottom: 5%;
    color: #fff;}

div#eightslider p { font-family: 'Hind', sans-serif; font-size: 14px; color: #787C88; line-height: 28px; text-align: center; margin: -8% 0 0 0;}
.businessfirstrow{margin-bottom:4%}
.businessfirstrow, .businesssecondrow{ display: flex; }

.business {border-radius: 10px; background-color: white; margin: 0 2%; width: 16%;}
.business p {padding: 4% 0 6% 0;}
.business img {     width: 50px;     padding: 10% 0; }

   
}


/* Media query for 993 to 1024 */
@media (min-width: 993px) and (max-width: 1024px) {
  


h1{font-family: 'Hind', sans-serif;     font-weight: 700; width: 60%; font-size: 40px; color: #787C88;  padding: 0 0 0 5.5%;  margin: 10% 0 0 0; }
h2 { font-family: 'Hind', sans-serif;     font-weight: 700; font-size: 30px; color: #484848; }
p { font-family: 'Hind', sans-serif; font-size: 14px;  color: #787C88;}

/****************************** First Division ****************************/
div#firstbanner{background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+236.png") , url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); 
    background-size: 60% 100% , auto; background-repeat: no-repeat , no-repeat; background-position: right, 4% 22%; }
iframe { border: 15px solid white;    box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0);}
.appscripvideo{ margin: 0 0 0 5.5%;     width: 40%;}
button.gtchbtn { margin: 3% 0 0 5.5%; background-color: #0088FC; color: white; font-family: 'Hind', sans-serif;     font-weight: 700; 
    border: 1px solid #0088FC; padding: 0.5% 1%;    font-size: 18px;
    -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc;
    box-shadow: 0 8px 6px -6px #0088fc;}
ul.list-inline.sociallogo { margin: 4% 0 0 5.5%;}    
ul.list-inline.sociallogo li {padding: 0 4% 0 0; font-size: 20px; color: #484848;}
ul.list-inline.sociallogo li a:hover { color: #0088fc!important;}

/****************************** Second Division ****************************/
.scndslidertext {padding: 0 0 0 5.5%;}
.scndslidertext h2 { margin: 8% 0 6% 0; font-family: 'Hind', sans-serif;     font-weight: 700;  font-size: 35px; color: #fff;}
.scndslidertext p {  font-family: 'Hind', sans-serif; font-size: 16px;color: #ffffff;}
img.apprening { margin: 15% 0 0 0;}
.col-md-6.scndrwimg { margin-top: 3%;}
.scndsliderslider.wpb_column.vc_column_container.vc_col-sm-6 {
    background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+22.png);
    background-repeat: no-repeat; background-size: auto; background-position: 88% 97%; padding: 0 0 4% 0;}

/****************************** Third Division ****************************/
div#thirdslider {padding: 0% 5.5%; background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); background-size: auto; background-repeat:  no-repeat; background-position: 4% 0%;     margin: 1% 0 0 0; }
#thirdslider .vc_column-inner{width:100%!important;}
#thirdslider h3{font-family: 'Hind', sans-serif;     font-weight: 700; color:#484848;font-size: 18px;}
#thirdslider p{font-family: 'Hind', sans-serif; font-size: 13px;color: #787C88; line-height: 20px;}
#thirdslider h2{margin:0 0 0 5%}
button.newbtn {margin: 0 -18% 0 0%; border: 1px solid #0088fc; float: right; padding: 1% 6%;  font-size: 17px;  font-family: 'Hind', sans-serif;     font-weight: 700; color: white; background-color: #0088fc;}

.parent{display: flex;}
.parentChild{ display: flex; flex-direction: column; justify-content: space-between; }
.child{ flex: 1;  background-size: contain; background-repeat: no-repeat; background-position: center center;}


.child1{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+172.png); }
.child1.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+172.webp);}

.child2{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+171.png);}
.child2.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+171.webp);}

.child3{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+174.png); }
.child3.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+174.webp);}

.child4{ height: 480px; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+175.png); }
.child4.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+175.webp);}

.pro2, .pro3 { padding: 18% 25% 0 12%;}
.pro1, .pro4 {padding: 24% 19% 0 17%;}

.child21{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+176.png);}
.child21.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+176.webp);}

.child31{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+178.png); }
.child31.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+178.webp);}

.child11{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+177.png); }
.child11.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+177.webp);}

.child41{ height: 478px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+179.png); }
.child41.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+179.webp);}

.child22{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+182.png);}
.child22.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+182.webp);}

.child32{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+183.png); }
.child32.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+183.webp);}

.child12{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+180.png); }
.child12.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+180.webp);}

.child42{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+181.png); }
.child42.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+181.webp);}

.child13{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+194.png); }
.child13.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+194.webp);}

.child23{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+193.png);}
.child23.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+193.webp);}

.child33{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+195.png); }
.child33.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+195.webp);}

.child43{ height: 480px; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+196.png); }
.child43.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+196.webp);}

/****************************** forth Division ****************************/
div#frthslider { padding: 5% 0 5% 5.5%; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+25.png); background-size: auto; background-repeat: no-repeat; background-position: 70% -14%;}
.frthslidervd iframe { height: 100%!important;  width: 100%!important;}
.frthslidertext{padding:0 8% 0 1%;}
.frthslidertext h2 {  margin-bottom: 10%;}

/****************************** fifth Division ****************************/
div#fifthslider { background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+27.png); background-size: 17%; background-repeat: no-repeat; padding: 5% 0 0 0; background-position: 46% 0;}
#fifthslider .vc_column-inner {width:100%!important;}
#fifthslider h2{margin: 0 0 0 10%;}

/****************************** sixth Division ****************************/
div#sixthslider {padding: 8% 3% 8% 5.5%; background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); background-size: auto; background-repeat:  no-repeat; background-position: 95% 0%; }
.usaaddress, .indaddress {box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0); padding: 6% 0 6% 0; background-color:white; }
.usaaddress{margin: 0 0 10% 0; display: flex;}
.indaddress{margin:0px; display: flex;}
.addimg { display: flex; align-items: center;  justify-content: center;}
#sixthslider h2{margin: 4% 0 5% 10%;}
#sixthslider h4{color: #484848; font-size: 18px;  font-weight: 700; font-family: 'Hind', sans-serif;}
#sixthslider p{line-height: 24px; font-size:13px;}
.contactform {box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0);background-color: white;}

input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform1, select.wpcf7-form-control.wpcf7-select.wpcf7-validates-as-required.commonform1, input.wpcf7-form-control.wpcf7-quiz.commonform1 {
    border: solid 1px #CECECE; padding: 15px; font-size: 13px; width: 80%; margin-left: 10%; margin-bottom: 10px; border-top-left-radius: 30px; height: 50px; outline: 0px; }
textarea.wpcf7-form-control.wpcf7-textarea.commonform1{ width: 80%; margin-left: 10%; height: 50px;  border-top-left-radius: 30px; border: solid 1px #ccc1c1; outline: 0px; padding-left: 20px;  padding-top: 10px;}
input.wpcf7-form-control.wpcf7-submit.btnSubmit1{  margin: 3% 10% 5% 0; background-color: #0088FC; color: white; float: right;   
    font-family: 'Hind', sans-serif;     font-weight: 700; border: 1px solid #0088FC; padding: 0.5% 8%; font-size: 18px; -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc; box-shadow: 0 8px 6px -6px #0088fc; }


.commonform1::-webkit-input-placeholder{font-size: 13px;} /* Chrome/Opera/Safari */
.commonform1::-moz-placeholder{font-size: 13px;} /* Firefox 19+ */
.commonform1:-ms-input-placeholder{font-size: 13px;} /* IE 10+ */
.commonform1:-moz-placeholder{font-size: 13px;} /* Firefox 18- */


/****************************** Seventh Division ****************************/
/* div#seventhslider {    margin: -15% 0 0 0; padding: 6% 3% 2% 3%;}
#seventhslider .vc_column-inner {width:100%!important;}
ul.footersubmenu { padding: 0; list-style: none;}
ul.footersubmenu h4 { color: white; font-size: 14px; line-height: 19px; font-family: 'Roboto', medium; margin: 0 0 15% 0;} 
ul.footersubmenu li {margin: 10% 0;}
ul.footersubmenu li a { color: white; font-size: 10px;font-family: 'Roboto', Regular;}

ul.footersocialmenu li {  color: white;  margin: 0 2%; font-size: 21px;}
ul.footersocialmenu { margin: 6% 0 0 0;}
ul.footersocialmenu li a {color:white;}
ul.footersocialmenu li a:hover { color: #4a4a4a;}
p.copyright { font-size: 14px; color: white; font-family: 'Hind', sans-serif;      margin: 27% 0 0 4%;}   */
  
/****************************** Eight Division ****************************/
div#eightslider { padding: 0% 3% 5% 3%; }
#eightslider .vc_column-inner {width:100%!important;}
#eightslider h2{font-family: 'Hind', sans-serif; width:60%; font-weight: 700;    margin-bottom: 5%; font-size: 40px;color: #fff;}
div#eightslider p { font-family: 'Hind', sans-serif; font-size: 14px; color: #787C88; line-height: 28px; text-align: center; margin: -8% 0 0 0;}
.businessfirstrow{margin-bottom:4%}
.businessfirstrow, .businesssecondrow{ display: flex; }
.business {border-radius: 10px; background-color: white; margin: 0 2%; width: 16%;}
.business p {padding: 4% 0 6% 0;}
.business img {     width: 50px;     padding: 10% 0; }



}


/* Media query for 768 to 992 */
@media (min-width: 768px) and (max-width: 992px) {
  

h1{font-family: 'Hind', sans-serif;     font-weight: 700;  font-size: 35px; color: #787C88;  padding: 0 0 0 5.5%;  margin: 10% 0 0 0; }
h2 { font-family: 'Hind', sans-serif;     font-weight: 700; font-size: 25px; color: #484848; }
p { font-family: 'Hind', sans-serif; font-size: 14px;  color: #787C88;}

/****************************** First Division ****************************/
div#firstbanner{background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+236.png") , url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); 
    background-size: 60% 100% , auto; background-repeat: no-repeat , no-repeat; background-position: right, 6% 38%; }
iframe { border: 15px solid white;    box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0);}
.appscripvideo{ margin: 0 0 0 5.5%;     width: 40%;}
button.gtchbtn { margin: 3% 0 0 5.5%; background-color: #0088FC; color: white; font-family: 'Hind', sans-serif;     font-weight: 700; 
    border: 1px solid #0088FC; padding: 0.5% 1%;    font-size: 18px;
    -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc;
    box-shadow: 0 8px 6px -6px #0088fc;}
ul.list-inline.sociallogo { margin: 4% 0 0 5.5%;}    
ul.list-inline.sociallogo li {padding: 0 4% 0 0; font-size: 20px; color: #484848;}
ul.list-inline.sociallogo li a:hover { color: #0088fc!important;}

/****************************** Second Division ****************************/
.scndslidertext {padding: 0 0 0 5.5%;}
.scndslidertext h2 { margin: 8% 0 6% 0; font-family: 'Hind', sans-serif;     font-weight: 700;  font-size: 25px; color: #fff;}
.scndslidertext p {  font-family: 'Hind', sans-serif; font-size: 14px;color: #ffffff;}
img.apprening { margin: 15% 0 0 0;}
.scndsliderslider.wpb_column.vc_column_container.vc_col-sm-6 {
    background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+22.png);
    background-repeat: no-repeat; background-size: auto; background-position: 88% 97%; padding: 0 0 4% 0;}

/****************************** Third Division ****************************/
div#thirdslider {padding: 0% 5.5%; background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); background-size: auto; background-repeat:  no-repeat; background-position: 4% 0%;     margin: 1% 0 0 0; }
#thirdslider .vc_column-inner{width:100%!important;}
#thirdslider h3{font-family: 'Hind', sans-serif;     font-weight: 700; color:#484848;font-size: 20px;}
#thirdslider p{font-family: 'Hind', sans-serif; font-size: 13px;color: #787C88; line-height: 20px;}

button.newbtn {border: 1px solid #0088fc; float: right; padding: 1% 6%;  font-size: 17px;  font-family: 'Hind', sans-serif;     font-weight: 700; color: white; background-color: #0088fc;}

/* .parent{display: flex;} */
.parentChild{ display: flex; flex-direction: column; justify-content: space-between; }
.child{ flex: 1; height: 450px; background-size: contain; background-repeat: no-repeat; background-position: center center;}


.child1{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+172.png); }
.child1.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+172.webp);}

.child2{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+171.png);}
.child2.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+171.webp);}

.child3{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+174.png); }
.child3.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+174.webp);}

.child4{ height: 480px; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+175.png); }
.child4.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+175.webp);}

.pro2, .pro3 { padding: 6% 31% 0 27%}
.pro1, .pro4 {padding: 4% 31% 0 32%;}

.child21{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+176.png);}
.child21.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+176.webp);}

.child31{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+178.png); }
.child31.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+178.webp);}

.child11{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+177.png); }
.child11.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+177.webp);}

.child41{ height: 478px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+179.png); }
.child41.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+179.webp);}

.child22{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+182.png);}
.child22.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+182.webp);}

.child32{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+183.png); }
.child32.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+183.webp);}

.child12{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+180.png); }
.child12.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+180.webp);}

.child42{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+181.png); }
.child42.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+181.webp);}

.child13{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+194.png); }
.child13.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+194.webp);}

.child23{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+193.png);}
.child23.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+193.webp);}

.child33{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+195.png); }
.child33.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+195.webp);}

.child43{ height: 480px; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+196.png); }
.child43.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+196.webp);}

 

/****************************** forth Division ****************************/
div#frthslider { padding: 5% 0 5    % 5.5%; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+25.png); background-size: auto; background-repeat: no-repeat; background-position: 70% -14%;}
.frthslidervd iframe { height: 100%!important;  width: 100%!important;}
.frthslidertext{padding:0 8% 0 1%;}
.frthslidertext h2 {  margin-bottom: 10%;}

/****************************** fifth Division ****************************/
div#fifthslider { background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+27.png); background-size: 17%; background-repeat: no-repeat; padding: 5% 0 0 0; background-position: 41% 0;}
#fifthslider .vc_column-inner {width:100%!important;}
#fifthslider h2{margin: 0 0 0 10%;}

/****************************** sixth Division ****************************/
div#sixthslider {padding: 8% 3% 8% 5.5%; background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); background-size: auto; background-repeat:  no-repeat; background-position: 95% 0%; }
.usaaddress, .indaddress {box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0); padding: 6% 0 6% 0; background-color:white; }
.usaaddress{margin: 0 0 10% 0; display: flex;}
.indaddress{margin:0px; display: flex;}
.addimg { display: flex; align-items: center;  justify-content: center;}
.addimg img {width:64px;}

#sixthslider h2{margin: 4% 0 5% 10%;}
#sixthslider h4{color: #484848; font-size: 18px;  font-weight: 700; font-family: 'Hind', sans-serif;}
#sixthslider p{line-height: 24px; font-size:13px;}
.contactform {box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0);background-color: white;}

input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform1, select.wpcf7-form-control.wpcf7-select.wpcf7-validates-as-required.commonform1, input.wpcf7-form-control.wpcf7-quiz.commonform1 {
    border: solid 1px #CECECE; padding: 15px; font-size: 13px; width: 100%; margin-bottom: 10px; border-top-left-radius: 30px; height: 50px; outline: 0px; }
textarea.wpcf7-form-control.wpcf7-textarea.commonform1{ width:100%; height: 106px;  border-top-left-radius: 30px; border: solid 1px #ccc1c1; outline: 0px; padding-left: 20px;  padding-top: 10px;}
input.wpcf7-form-control.wpcf7-submit.btnSubmit1{  margin: 5% 10% 5% 0; background-color: #0088FC; color: white; float: right;   
    font-family: 'Hind', sans-serif;     font-weight: 700; border: 1px solid #0088FC; padding: 0.5% 8%; font-size: 18px; -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc; box-shadow: 0 8px 6px -6px #0088fc; }


.commonform1::-webkit-input-placeholder{font-size: 13px;} /* Chrome/Opera/Safari */
.commonform1::-moz-placeholder{font-size: 13px;} /* Firefox 19+ */
.commonform1:-ms-input-placeholder{font-size: 13px;} /* IE 10+ */
.commonform1:-moz-placeholder{font-size: 13px;} /* Firefox 18- */


/****************************** Seventh Division ****************************/
/* div#seventhslider {    margin: -15% 0 0 0; padding: 6% 3% 2% 3%;}
#seventhslider .vc_column-inner {width:100%!important;}
.footerelement {text-align: center;}
ul.footersubmenu { padding: 0; text-align: center; list-style: none; margin: 6% 0 0 0; }
ul.footersubmenu h4 { color: white; font-size: 16px; line-height: 19px; font-family: 'Roboto', medium; margin: 0 0 3% 0;} 
ul.footersubmenu li {margin: 2% 0;}
ul.footersubmenu li a { color: white; font-size: 12px;font-family: 'Roboto', Regular;}

ul.footersocialmenu li {  color: white;  margin: 0 5%; font-size: 18px;}
ul.footersocialmenu { text-align: center; margin: 6% 0 0 0;}
ul.footersocialmenu li a {color:white;}
ul.footersocialmenu li a:hover { color: #4a4a4a;}
p.copyright { font-size: 14px; color: white; font-family: 'Hind', sans-serif;      margin: 6% 0 0 0%; text-align:center;}   */
  

/****************************** Eight Division ****************************/
div#eightslider { padding: 3% 3%; margin: 4% 0 0 0;}
#eightslider .vc_column-inner {width:100%!important;}
#eightslider h2{font-family: 'Hind', sans-serif; width:60%;
    font-weight: 700;    margin: -2% 0 3% 0;
    font-size: 25px;
    color: #fff;}

div#eightslider p { font-family: 'Hind', sans-serif; font-size: 14px; color: #787C88; line-height: 28px; text-align: center; margin: -8% 0 0 0;}
.business {
    border-radius: 10px;
    background-color: white;
    margin: 2% 2%;
    width: 46;
}
.business p {padding: 4% 0 6% 0;}
.business img {     width: 50px;     padding: 10% 0; }





}


/* Media query for 450 to 767 */
@media (min-width: 450px) and (max-width: 767px) {
  

h1{font-family: 'Hind', sans-serif;     font-weight: 700;  font-size: 30px; color: #787C88;  padding: 0 0 0 8.5%;  margin: 16% 0 0 0; }
h2 { font-family: 'Hind', sans-serif;     font-weight: 700; font-size: 25px; color: #484848; }
p { font-family: 'Hind', sans-serif; font-size: 14px;  color: #787C88;}

/****************************** First Division ****************************/
div#firstbanner{    background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png); background-size: auto; background-repeat: no-repeat; background-position: 18% 85%;}
iframe { border: 15px solid white;    box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0);}
.appscripvideo{ margin: 0 0 0 8.5%;     width: 75%;}
button.gtchbtn { margin: 7% 0 0 8.5%; background-color: #0088FC; color: white; font-family: 'Hind', sans-serif;     font-weight: 700; 
    border: 1px solid #0088FC; padding: 0.5% 1%;    font-size: 18px;
    -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc;
    box-shadow: 0 8px 6px -6px #0088fc;}
ul.list-inline.sociallogo { margin: 5% 0 0 8.5%;}    
ul.list-inline.sociallogo li {padding: 0 8% 0 0; font-size: 20px; color: #484848;}
ul.list-inline.sociallogo li a:hover { color: #0088fc!important;}

/****************************** Second Division ****************************/
.scndslidertext {padding: 0 0 0 8.5%;}
.scndslidertext h2 { margin: 8% 0 6% 0; font-family: 'Hind', sans-serif;     font-weight: 700;  font-size: 25px; color: #fff;}
.scndslidertext p {  font-family: 'Hind', sans-serif; font-size: 14px;color: #ffffff;}
img.apprening { margin: 15% 0 0 0;}
.scndsliderslider.wpb_column.vc_column_container.vc_col-sm-6 {
    background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+22.png);
    background-repeat: no-repeat; background-size: auto;     background-position: 88% 113%; padding: 0 10%; margin: 0 0 0 5%;}

/****************************** Third Division ****************************/
div#thirdslider {padding: 0% 5.5%; background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); background-size: auto; background-repeat:  no-repeat; background-position: 4% 0%;    }
#thirdslider .vc_column-inner{width:100%!important;}
#thirdslider h3{font-family: 'Hind', sans-serif;     font-weight: 700; color:#484848;font-size: 20px;}
#thirdslider p{font-family: 'Hind', sans-serif; font-size: 13px;color: #787C88; line-height: 20px;}
div#thirdslider h2 { padding: 3% 0 0 3.5%;}

button.newbtn {border: 1px solid #0088fc; float: right; padding: 1% 6%;  font-size: 17px;  font-family: 'Hind', sans-serif;     font-weight: 700; color: white; background-color: #0088fc;}

/* .parent{display: flex;} */
.parentChild{ display: flex; flex-direction: column; justify-content: space-between; }
.child{ flex: 1; height: 450px; background-size: contain; background-repeat: no-repeat; background-position: center center;}


.child1{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+172.png); }
.child1.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+172.webp);}

.child2{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+171.png);}
.child2.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+171.webp);}

.child3{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+174.png); }
.child3.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+174.webp);}

.child4{ height: 480px; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+175.png); }
.child4.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+175.webp);}

.pro2, .pro3 { padding: 4% 18% 0 21%;}
.pro1, .pro4 {padding: 5% 27% 0;}

.child21{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+176.png);}
.child21.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+176.webp);}

.child31{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+178.png); }
.child31.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+178.webp);}

.child11{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+177.png); }
.child11.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+177.webp);}

.child41{ height: 478px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+179.png); }
.child41.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+179.webp);}

.child22{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+182.png);}
.child22.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+182.webp);}

.child32{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+183.png); }
.child32.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+183.webp);}

.child12{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+180.png); }
.child12.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+180.webp);}

.child42{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+181.png); }
.child42.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+181.webp);}

.child13{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+194.png); }
.child13.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+194.webp);}

.child23{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+193.png);}
.child23.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+193.webp);}

.child33{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+195.png); }
.child33.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+195.webp);}

.child43{ height: 480px; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+196.png); }
.child43.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+196.webp);}

/****************************** forth Division ****************************/
div#frthslider { padding: 5% 0 5% 5.5%; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+25.png); background-size: auto; background-repeat: no-repeat; background-position: 23% 72%;}
.frthslidervd iframe { height: 100%!important;  width: 100%!important;}
.frthslidertext{padding:0 8% 0 1%;}
.frthslidertext h2 { margin: 0 0 10% 4%;}
.frthslidertext p { margin: 0 0 0 4%;}


/****************************** fifth Division ****************************/
div#fifthslider { background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+27.png); background-size: 25; background-repeat: no-repeat; padding: 5% 0 0 0; background-position: 48% 0;}
#fifthslider .vc_column-inner {width:100%!important;}
#fifthslider h2{margin: 0 0 0 10%;}

/****************************** sixth Division ****************************/
div#sixthslider {padding: 8% 3% 8% 5.5%; background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); background-size: auto; background-repeat:  no-repeat; background-position: 87% -4%; }
div#sixthslider .vc_column-inner{    width: 100%!important;}
.usaaddress, .indaddress {box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0); padding: 6% 0 6% 0; background-color:white; }
.usaaddress{margin: 0 0 10% 0; display: flex;}
.indaddress{margin:0px; display: flex;}
.addimg { display: flex; align-items: center;  justify-content: center;}

#sixthslider h2{margin: 4% 0 5% 10%;}
#sixthslider h4{color: #484848; font-size: 18px;  font-weight: 700; font-family: 'Hind', sans-serif;}
#sixthslider p{line-height: 24px; font-size:13px;}
.contactform {box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0);background-color: white; margin: 0!important;}

input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform1, select.wpcf7-form-control.wpcf7-select.wpcf7-validates-as-required.commonform1, input.wpcf7-form-control.wpcf7-quiz.commonform1 {
    border: solid 1px #CECECE; padding: 15px; font-size: 13px; width: 100%;     margin-left: 0%; margin-bottom: 10px; border-top-left-radius: 30px; height: 58px!important; outline: 0px; }
textarea.wpcf7-form-control.wpcf7-textarea.commonform1{ width:100%; height: 106px;  border-top-left-radius: 30px; border: solid 1px #ccc1c1; outline: 0px; padding-left: 20px;  padding-top: 10px;}
input.wpcf7-form-control.wpcf7-submit.btnSubmit1{  margin: 5% 5% 5% 0; background-color: #0088FC; color: white; float: right;   
    font-family: 'Hind', sans-serif;     font-weight: 700; border: 1px solid #0088FC; padding: 1.5% 10%; font-size: 18px; -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc; box-shadow: 0 8px 6px -6px #0088fc; }


.commonform1::-webkit-input-placeholder{font-size: 13px;} /* Chrome/Opera/Safari */
.commonform1::-moz-placeholder{font-size: 13px;} /* Firefox 19+ */
.commonform1:-ms-input-placeholder{font-size: 13px;} /* IE 10+ */
.commonform1:-moz-placeholder{font-size: 13px;} /* Firefox 18- */


/****************************** Seventh Division ****************************/
/* div#seventhslider {    margin: -15% 0 0 0; padding: 6% 3% 2% 3%;}
#seventhslider .vc_column-inner {width:100%!important;}
.footerelement {text-align: center;}
ul.footersubmenu { padding: 0; text-align: center; list-style: none; margin: 6% 0 0 0; }
ul.footersubmenu h4 { color: white; font-size: 16px; line-height: 19px; font-family: 'Roboto', medium; margin: 0 0 3% 0;} 
ul.footersubmenu li {margin: 2% 0;}
ul.footersubmenu li a { color: white; font-size: 12px;font-family: 'Roboto', Regular;}

ul.footersocialmenu li {  color: white;  margin: 0 5%; font-size: 18px;}
ul.footersocialmenu { text-align: center; margin: 6% 0 0 0;}
ul.footersocialmenu li a {color:white;}
ul.footersocialmenu li a:hover { color: #4a4a4a;}
p.copyright { font-size: 14px; color: white; font-family: 'Hind', sans-serif;      margin: 6% 0 0 0%; text-align:center;}   */
  


 /****************************** Eight Division ****************************/
 div#eightslider { padding: 0% 3%; }
#eightslider .vc_column-inner {width:100%!important;}
#eightslider h2{font-family: 'Hind', sans-serif; width:60%;
    font-weight: 700;
    font-size: 25px;
    color: #fff;}

div#eightslider p { font-family: 'Hind', sans-serif; font-size: 14px; color: #787C88; line-height: 28px; text-align: center; margin: -8% 0 0 0;}
.business {
    border-radius: 10px;
    background-color: white;
    margin: 2% 2% 2% 5%;
    width: 97%;
}
.business p {padding: 4% 0 6% 0;}
.business img {     width: 50px;     padding: 10% 0; }

  
}


/* Media query for 280 to 449 */
@media (min-width: 280px) and (max-width: 449px) {
  


h1{font-family: 'Hind', sans-serif;     font-weight: 700;  font-size: 27px; color: #787C88;  padding: 0 0 0 8.5%;  margin: 16% 0 0 0; }
h2 { font-family: 'Hind', sans-serif;     font-weight: 700; font-size: 25px; color: #484848; }
p { font-family: 'Hind', sans-serif; font-size: 14px;  color: #787C88;}

/****************************** First Division ****************************/
div#firstbanner{    background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png); background-size: auto; background-repeat: no-repeat; background-position: 18% 85%;}
iframe { border: 15px solid white;    box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0);}
.appscripvideo{ margin: 0 0 0 8.5%;     width: 75%;}
button.gtchbtn { margin: 7% 0 0 8.5%; background-color: #0088FC; color: white; font-family: 'Hind', sans-serif;     font-weight: 700; 
    border: 1px solid #0088FC; padding: 0.5% 1%;    font-size: 18px;
    -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc;
    box-shadow: 0 8px 6px -6px #0088fc;}
ul.list-inline.sociallogo { margin: 5% 0 0 8.5%;}    
ul.list-inline.sociallogo li {padding: 0 8% 0 0; font-size: 20px; color: #484848;}
ul.list-inline.sociallogo li a:hover { color: #0088fc!important;}

#headerAppscripNew h1{ margin-top:20%}

/****************************** Second Division ****************************/
.scndslidertext {padding: 0 0 0 8.5%;}
.scndslidertext h2 { margin: 0% 0 6% 0; font-family: 'Hind', sans-serif;     font-weight: 700;  font-size: 25px; color: #fff;}
.scndslidertext p {  font-family: 'Hind', sans-serif; font-size: 14px;color: #ffffff;}
img.apprening { margin: 15% 0 0 0;}
.scndsliderslider.wpb_column.vc_column_container.vc_col-sm-6 {
    background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+22.png);
    background-repeat: no-repeat; background-size: auto;     background-position: 88% 113%; padding: 0 10%; margin: -11% 0 0 5%;}
div#secondslider { padding-bottom: 3%;}

/****************************** Third Division ****************************/
div#thirdslider {padding: 3% 5.5%; background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); background-size: auto; background-repeat:  no-repeat; background-position: 4% 1%;      margin-top: -7%;  }
#thirdslider .vc_column-inner{width:100%!important;}
#thirdslider h3{font-family: 'Hind', sans-serif;     font-weight: 700; color:#484848;font-size: 18px;}
#thirdslider p{font-family: 'Hind', sans-serif; font-size: 13px;color: #787C88; line-height: 20px;}
div#thirdslider h2 { padding: 0 0 0 3.5%;}

button.newbtn {margin: 0% -16% 0 0%; border: 1px solid #0088fc; float: right; padding: 1% 6%;  font-size: 17px;  font-family: 'Hind', sans-serif;     font-weight: 700; color: white; background-color: #0088fc;}

/* .parent{display: flex;} */
.parentChild{ display: flex; flex-direction: column; justify-content: space-between; }
.child{ flex: 1; height: 365px; background-size: contain; background-repeat: no-repeat; background-position: center center;}



.child1{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+172.png); }
.child1.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+172.webp);}

.child2{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+171.png);}
.child2.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+171.webp);}

.child3{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+174.png); }
.child3.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+174.webp);}

.child4{ height: 480px; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+175.png); }
.child4.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+175.webp);}

.pro2, .pro3 { padding: 9% 11% 0 11%;}
.pro1, .pro4 {padding: 25% 11% 0 11%;}

.child21{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+176.png);}
.child21.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+176.webp);}

.child31{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+178.png); }
.child31.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+178.webp);}

.child11{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+177.png); }
.child11.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+177.webp);}

.child41{ height: 478px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+179.png); }
.child41.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+179.webp);}

.child22{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+182.png);}
.child22.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+182.webp);}

.child32{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+183.png); }
.child32.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+183.webp);}

.child12{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+180.png); }
.child12.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+180.webp);}

.child42{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+181.png); }
.child42.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+181.webp);}

.child13{ height: 480px;background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+194.png); }
.child13.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+194.webp);}

.child23{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+193.png);}
.child23.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+193.webp);}

.child33{background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+195.png); }
.child33.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+195.webp);}

.child43{ height: 480px; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+196.png); }
.child43.webpEnabled{ background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/webpimg/Group+196.webp);}
 

/****************************** forth Division ****************************/
div#frthslider { padding: 8% 0 8% 5.5%; background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+25.png); background-size: auto; background-repeat: no-repeat; background-position: 23% 72%;}
.frthslidervd iframe { height: 100%!important;  width: 100%!important;}
.frthslidertext{padding:0 8% 0 1%;}
.frthslidertext h2 { margin: -10% 0 10% 4%;}
.frthslidertext p { margin: 0 0 0 4%;}


/****************************** fifth Division ****************************/
div#fifthslider { background-image: url(https://dji16gnzvk5qv.cloudfront.net/appscriphome/Mask+Group+27.png); background-size: auto; background-repeat: no-repeat;     padding: 6% 0 0 0; margin-top: -8%; background-position: 36% 0;}
#fifthslider .vc_column-inner {width:100%!important;}
#fifthslider h2{margin: 0 0 0 10%;}

/****************************** sixth Division ****************************/
div#sixthslider {padding: 8% 3% 8% 5.5%; background-image: url("https://dji16gnzvk5qv.cloudfront.net/appscriphome/Group+190.png"); background-size: auto; background-repeat:  no-repeat; background-position: 87% -4%; }
div#sixthslider .vc_column-inner{    width: 100%!important;}
.usaaddress, .indaddress {box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0); padding: 6% 0 6% 0; background-color:white; }
.usaaddress{margin: 0 0 10% 0; display: flex;}
.indaddress{margin:0px; display: flex;}
.addimg { display: flex; align-items: center;  justify-content: center;}

#sixthslider h2{margin: 4% 0 5% 10%;}
#sixthslider h4{color: #484848; font-size: 18px;  font-weight: 700; font-family: 'Hind', sans-serif;}
#sixthslider p{line-height: 24px; font-size:13px;}
.contactform {box-shadow: 0 0 27px rgba(0,0,0,0.19), 0 6px 34px rgba(0,0,0,0);background-color: white; margin: 0!important;}

input.wpcf7-form-control.wpcf7-text.wpcf7-validates-as-required.commonform1, select.wpcf7-form-control.wpcf7-select.wpcf7-validates-as-required.commonform1, input.wpcf7-form-control.wpcf7-quiz.commonform1 {
    border: solid 1px #CECECE; padding: 15px; font-size: 13px; width: 100%;     margin-left: 0%; margin-bottom: 10px; border-top-left-radius: 30px; height: 58px!important; outline: 0px; }
textarea.wpcf7-form-control.wpcf7-textarea.commonform1{ width:100%; height: 106px;  border-top-left-radius: 30px; border: solid 1px #ccc1c1; outline: 0px; padding-left: 20px;  padding-top: 10px;}
input.wpcf7-form-control.wpcf7-submit.btnSubmit1{  margin: 5% 5% 5% 0; background-color: #0088FC; color: white; float: right;   
    font-family: 'Hind', sans-serif;     font-weight: 700; border: 1px solid #0088FC; padding: 1.5% 10%; font-size: 18px; -webkit-box-shadow: 0 8px 6px -6px #0088fc;
    -moz-box-shadow: 0 8px 6px -6px #0088fc; box-shadow: 0 8px 6px -6px #0088fc; }


.commonform1::-webkit-input-placeholder{font-size: 13px;} /* Chrome/Opera/Safari */
.commonform1::-moz-placeholder{font-size: 13px;} /* Firefox 19+ */
.commonform1:-ms-input-placeholder{font-size: 13px;} /* IE 10+ */
.commonform1:-moz-placeholder{font-size: 13px;} /* Firefox 18- */


/****************************** Seventh Division ****************************/
/* div#seventhslider {    margin: -15% 0 0 0; padding: 6% 3% 2% 3%;}
#seventhslider .vc_column-inner {width:100%!important;}
.footerelement {text-align: center;}
ul.footersubmenu { padding: 0; text-align: center; list-style: none; margin: 6% 0 0 0; }
ul.footersubmenu h4 { color: white; font-size: 16px; line-height: 19px; font-family: 'Roboto', medium; margin: 0 0 3% 0;} 
ul.footersubmenu li {margin: 2% 0;}
ul.footersubmenu li a { color: white; font-size: 12px;font-family: 'Roboto', Regular;}

ul.footersocialmenu li {  color: white;  margin: 0 5%; font-size: 18px;}
ul.footersocialmenu { text-align: center; margin: 6% 0 0 0;}
ul.footersocialmenu li a {color:white;}
ul.footersocialmenu li a:hover { color: #4a4a4a;}
p.copyright { font-size: 14px; color: white; font-family: 'Hind', sans-serif;      margin: 6% 0 0 0%; text-align:center;}   */
  

/****************************** Eight Division ****************************/
div#eightslider { padding: 0 3% 3%; }
#eightslider .vc_column-inner {width:100%!important;}
#eightslider h2{font-family: 'Hind', sans-serif; width:100%;
    font-weight: 700;
    font-size: 25px;
    color: #fff;}

div#eightslider p { font-family: 'Hind', sans-serif; font-size: 14px; color: #787C88; line-height: 28px; text-align: center; margin: -8% 0 0 0;}
.business {
    border-radius: 10px;
    background-color: white;
    margin: 2% 2% 2% 4%;
    width: 94%;
}
.business p {padding: 4% 0 6% 0;}
.business img {     width: 50px;     padding: 10% 0; }

  
}
 
</style>




<div class="page-container">
    <div class="container-fluid" style="padding-right: 0px; 
     padding-left: 0px; ">
        <div class="row">
            <div class="page-content">
             <div class="col-md-12">
              <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
				<?php the_content(); ?>	
			<?php endif; ?>	  
				</div>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<?php get_footer(); ?>

<script>
jQuery.noConflict();
    var lastId,
    topMenu = jQuery("#header_link_menu"),
    topMenuHeight = topMenu.outerHeight(),
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = jQuery(jQuery(this).attr("href"));
      if (item.length) { return item; }
    });

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
  var href = jQuery(this).attr("href"),
      offsetTop = href === "#" ? 0 : jQuery(href).offset().top-topMenuHeight;
  jQuery('html, body').stop().animate({ 
      scrollTop: offsetTop
  }, 500);
  e.preventDefault();
});

// Bind to scroll
jQuery(window).scroll(function(){
   // Get container scroll position
   var fromTop = jQuery(this).scrollTop()+topMenuHeight+60;
   
   // Get id of current scroll item
   var cur = scrollItems.map(function(){
     if (jQuery(this).offset().top < fromTop)
       return this;
   });
   // Get the id of the current element
   cur = cur[cur.length-1];
   var id = cur && cur.length ? cur[0].id : "";
   
   if (lastId !== id) {
       lastId = id;
       // Set/remove active class
       menuItems
         .parent().removeClass("active")
         .end().filter("[href='#"+id+"']").parent().addClass("active");
   }                   
});
</script>
<script>
jQuery.noConflict();
    var lastId,
    topMenu = jQuery("#top_menu_app"),
    topMenuHeight = topMenu.outerHeight()+60,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = jQuery(jQuery(this).attr("href"));
      if (item.length) { return item; }
    });

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
  var href = jQuery(this).attr("href"),
      offsetTop = href === "#" ? 0 : jQuery(href).offset().top-topMenuHeight;
  jQuery('html, body').stop().animate({ 
      scrollTop: offsetTop
  }, 500);
  e.preventDefault();
});

// Bind to scroll
jQuery(window).scroll(function(){
   // Get container scroll position
   var fromTop = jQuery(this).scrollTop()+topMenuHeight+60;
   
   // Get id of current scroll item
   var cur = scrollItems.map(function(){
     if (jQuery(this).offset().top < fromTop)
       return this;
   });
   // Get the id of the current element
   cur = cur[cur.length-1];
   var id = cur && cur.length ? cur[0].id : "";
   
   if (lastId !== id) {
       lastId = id;
       // Set/remove active class
       menuItems
         .parent().removeClass("active")
         .end().filter("[href='#"+id+"']").parent().addClass("active");
   }                   
});
</script>
  <script src="https://dji16gnzvk5qv.cloudfront.net/appscriphome/slick.js" type="text/javascript" charset="utf-8"></script>


<script>
  jQuery(document).ready(function($) {	   

//    script for popup in middle
    var popup_bool = false;
   var window_ht = $(window).height();
   $(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
   
    if(scroll>window_ht&&!popup_bool){
     $("#show_popup_offer").trigger('click');
     popup_bool = true;
    }
});

  $(".box_shadow img").click(function(){
  });
  
  
  $(".box_shadow").click(function(){
	    var src= $(this).find('a').attr('href');
		window.location.href = src;
  });
  $('.center').slick({
  centerMode: true,
  autoplay: true,
  autoplaySpeed: 2000,
  centerPadding: '60px',
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});

var hold = false;
$("#hold").click(function() {
  slider.slick("slickSetOption", "accessibility", hold);
  slider.slick("slickSetOption", "draggable", hold);
  slider.slick("slickSetOption", "swipe", hold);
  slider.slick("slickSetOption", "touchMove", hold);
  
  hold = !hold;
  
  $(this).toggleClass("disabled");
});
		
  }) 
  
  
  

</script>



<!-- /* Slick Slider script*/ -->


