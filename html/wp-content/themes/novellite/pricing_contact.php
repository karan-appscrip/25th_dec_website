<?php
/*
  Template Name: Pricing_contact
 */
?>
<style>
.header_container {
    background: grey;
}
nav.navbar.navbar-default.navbar-fixed-top {
    margin: 0%;
}
</style>

<?php get_header(); ?>

<h1 style="text-align:center;font-weight:bolder;margin-top:100px;font-size:31px;font-weight:800;margin-bottom:60px">PRICING - CONTACT</h1> 
<div class="page-container">
    <div class="container-fluid" style="padding-right: 0px; 
     padding-left: 0px; ">
        <div class="row">
            <div class="page-content">
             <div class="col-md-12">
              <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
				<?php the_content(); ?>	
			<?php endif; ?>	  
				</div>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>
</div>