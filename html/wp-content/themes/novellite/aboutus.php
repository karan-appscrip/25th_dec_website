<?php
/*
  Template Name: About us
 */
?>
<?php get_header(); ?>

<div class="page_heading_container2">
  <div class="container">
        <div class="row">
		<div class="col-md-12">
		<div class="page_heading_content">
		<h1 class="wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; margin-top:200px;font-size:40px">CREATING THE NEXT <span style="font-weight:bolder;font-size:40px">APP-RENEUR</span></h1>
		<p style="font-size: 20px;text-align: center;line-height: 28px;color: #fff;margin-top: 75px;">Mobile technology is disrupting industry value chains across verticals. We have been perfecting our value proposition for the past 24 months to power the technology supporting your business ideas by building beautiful, fast apps. Think you have a good idea? Get in touch with us and we will bring it to life.</p>
                 
		</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>


<div class="page-container">
    <div class="container-fluid" style="padding-right: 0px; 
     padding-left: 0px; ">
        <div class="row">
            <div class="page-content">
             <div class="col-md-12">
              <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
				<?php the_content(); ?>	
			<?php endif; ?>	  
				</div>
			</div>
        </div>
        <div class="clear"></div>
</div>
</div>
</div>
    

<div> <span class="counter" style="display: inline-block; width: 32%">52,147</span>



    
<?php get_template_part( 'template/section_pricing');  ?>
<?php get_template_part( 'template/section_countactus');  ?>
<?php get_footer(); ?>
<script>
    jQuery(document).ready(function( $ ) {
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    });
</script>