<div class="scroll-target_plain">
<?php
/**
 * The Footer widget areas.
 *
 * @package WordPress
 */
?>
<?php
/* The footer widget area is triggered if any of the areas
 * have widgets. So let's check that first.
 *
 * If none of the sidebars have widgets, then let's bail early.
 */
if (!is_active_sidebar('first-footer-widget-area') && !is_active_sidebar('second-footer-widget-area') && !is_active_sidebar('third-footer-widget-area') && !is_active_sidebar('fourth-footer-widget-area') && !is_active_sidebar('five-footer-widget-area') && !is_active_sidebar('six-footer-widget-area')
)
    return;
// If we get this far, we have widgets. Let do this.
?>

<div>
<!-------------- Footer Sidebar----------->
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <div class="widget-area first">
        <?php if (is_active_sidebar('first-footer-widget-area')) : ?>
            <?php dynamic_sidebar('first-footer-widget-area'); ?>
        <?php endif; ?>
    </div>
</div>
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <div class="widget-area second">
        <?php if (is_active_sidebar('second-footer-widget-area')) : ?>
            <?php dynamic_sidebar('second-footer-widget-area'); ?>
        <?php endif; ?>
    </div>
</div>
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <div class="widget-area third">
        <?php if (is_active_sidebar('third-footer-widget-area')) : ?>
            <?php dynamic_sidebar('third-footer-widget-area'); ?>
        <?php endif; ?>
    </div>
</div>
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <div class="widget-area last">
        <?php if (is_active_sidebar('fourth-footer-widget-area')) : ?>
            <?php dynamic_sidebar('fourth-footer-widget-area'); ?>
        <?php endif; ?>
    </div>
</div>
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <div class="widget-area five">
        <?php if (is_active_sidebar('five-footer-widget-area')) : ?>
            <?php dynamic_sidebar('five-footer-widget-area'); ?>
        <?php endif; ?>
    </div>
</div>
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <div class="widget-area six">
        <?php if (is_active_sidebar('six-footer-widget-area')) : ?>
            <?php dynamic_sidebar('six-footer-widget-area'); ?>
        <?php endif; ?>
    </div>
</div>
</div>

<div class="col-md-12" style="margin-left:-10%; margin-bottom: 4%;text-align:center"  id="footer_social">
<ul>
 <li><a href="https://www.linkedin.com/company/appscrip" class="fa fa-fw fa-linkedin" target="_blank" style="font-size:30px;color:#fff"> </a></li>
 <li><a href="https://www.facebook.com/appscrip" target="_blank" class="fa fa-fw fa-facebook" style="font-size:30px;color:#fff"> </a></li>
 <li><a href="https://www.youtube.com/channel/UC7U65c_o1hNwiwTkWXG32fg" class="fa fa-fw fa-youtube" target="_blank" style="font-size:30px;color:#fff"> </a> 
 </li>
 <li><a href="https://plus.google.com/u/0/+Appscrip" class="fa fa-fw fa-google-plus" target="_blank" style="font-size:30px;color:#fff"> </a></li>
 <li><a href="https://twitter.com/appscrip" class="fa fa-fw fa-twitter" target="_blank" style="font-size:30px;color:#fff"> </a></li>
</ul>
</div>

<div class=" col-md-12 footer_menuu">
   <ul>
    <li><a href="/index.php">HOME</a><li>
    <li><a href="/about/">ABOUT US</a><li>
    <li><a href="/ios-android-clone-scripts/">PRODUCTS</a><li>
    <li><a href="/custom-mobile-app-development/">SERVICES</a><li>
    <li><a href="/get-rich-with-mobile-app-clones-how-to-raise-funding-for-mobile-apps-appscrip-reviews/">SUCCESS STORIES</a><li>
    <li><a href="/contact-us/">CONTACT US</a><li>
    <li><a href="/blog/">BLOG</a><li>
   </ul>
</div>

</div>