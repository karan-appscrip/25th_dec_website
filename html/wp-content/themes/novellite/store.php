<?php
/*
  Template Name: Pricing 
 */
?>
<style>
    .navbar .sf-menu li a{color:#000!important}
    .pricing_button{    padding: 5px 30px;
    font-size: 20px;
    border-radius: 25px;
    border: solid 1px #4a4a4a;
    color: #4a4a4a;
    margin-left: 8%;
    margin-top: 0%;}
	 .pricing_button:hover{    background: #000;
    border: 1px solid #000;
	transition: all 0.3s ease-in-out;
	color:#fff}
	.pricing_rte{
	font-weight:bolder;
    font-family: ar;}
</style>
<?php get_header(); ?>
<h1 style="text-align:center;font-weight:bolder;margin-top:100px;font-size:42px">PRICING</h1>
<div class="page-container">
    <div class="container-fluid" style="padding-right: 0px; 
     padding-left: 0px; ">
        <div class="row">
            <div class="page-content">
             <div class="col-md-12">
              <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
				<?php the_content(); ?>	
			<?php endif; ?>	  
				</div>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<?php get_footer(); ?>
