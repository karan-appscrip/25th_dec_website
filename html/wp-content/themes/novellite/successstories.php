<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 4711811;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->
<style>
   #succ_div1{background:#fecc0f;height:100%}
   #succ_div2{background:#3f88a9;height:100%}
   #succ_div3{background:#fddd58;height:100%}
   #succ_div4{background:#0875ba;height:100%}
  #suc_img1 {
    float: left;
    width: 38%;
    margin-top: 22%;
    margin-left: 13%;
}
   #suc_img2 {
    float: left;
    width: 53%;
    margin-left: -12%;
    margin-top: 28%;
}
   img#roadyo {
       float: right;
    margin-top: 18%;
    width: 25%;
    margin-right: 16%;}
   img#primelogo{
	   margin-top: 80px;
    width: 50%;
   }
   #suc_para1 {
   margin-top: 40%;
    width: 84%;
    font-size: 18px;
    font-weight: 800;
    line-height: 32px;
    color: #4b4a4b;
}
   #suc_para2{
	   margin-top: 15%;
    WIDTH: 90%;
    font-size: 18px;
    font-weight: 800;
    line-height: 32px;
    color: #fff;
   }
   a#succ_button {
    font-size: 18px;
    color: #54514c;
    font-weight: 800;
    border-radius: 60px;
    border: solid 2px;
	position:absolute;
    padding: 10px 20px;
    margin-top: 31px;
	}
	a#succ_button2 {
    font-size: 18px;
    color: #fff;
    font-weight: 800;
    border-radius: 60px;
    border: solid 2px #fff;
	position:absolute;
    padding: 10px 20px;
    margin-top: 31px;
	}
#success_downloadbut{margin-left: 56%;}
	 #app_buttons,#app_buttonsb{font-size: 18px;
    font-weight: bolder;
	text-align:center;
    line-height: 32px;
    color: #4b4a4b;}
	#app_buttons2{font-size: 18px;
    font-weight: bolder;
	text-align:center;
    line-height: 32px;
    color: #fff;}
	a#succ_button2:hover{    background: #fff;
    color: #4b4a4b;
    -webkit-transition: background 0.5s linear;
    -moz-transition: background 0.3s linear;
    -ms-transition: background 0.3s linear;
    -o-transition: background 0.3s linear;
    transition: background 0.4s ease-in-out;}
	.succ_app_buttons{
		font-size: 18px;
    color: #54514c;
    font-weight: 800;
    border-radius: 40px;
    border: solid 2px;
    padding: 4px 24px;
	}
	.succ_app_buttonsb{
		font-size: 18px;
    color: #fff;
    font-weight: 800;
    border-radius: 40px;
    border: solid 2px #fff;
    padding: 4px 24px;
	}
	.succ_app_buttons2{font-size: 18px;
    color: #54514c;
    font-weight: 800;
    border-radius: 40px;
    border: solid 2px;
    padding: 2px 24px;}
	.succ_app_buttons2b{font-size: 18px;
    color: #fff;
    font-weight: 800;
    border-radius: 40px;
    border: solid 2px #fff;
    padding: 2px 24px;}
	a#succ_button:hover{        background: rgb(75, 74, 75);
    color: #fff;
	border-color:rgb(75, 74, 75);
    -webkit-transition: background 0.5s linear;
    -moz-transition: background 0.3s linear;
    -ms-transition: background 0.3s linear;
    -o-transition: background 0.3s linear;
    transition: background 0.4s ease-in-out;}
	.succ_app_buttons:hover,.succ_app_buttons2:hover{
		background:rgba(255, 255, 255, 0.4);
    color: #54514c;
	border:#fff solid 2px;
    -webkit-transition: background 0.5s linear;
    -moz-transition: background 0.3s linear;
    -ms-transition: background 0.3s linear;
    -o-transition: background 0.3s linear;
    transition: background 0.4s ease-in-out;
-webkit-box-shadow: 0px 0px 16px 5px rgba(156,156,156,0.56);
-moz-box-shadow: 0px 0px 16px 5px rgba(156,156,156,0.56);
box-shadow: 0px 0px 16px 5px rgba(156,156,156,0.56);
	}
	.succ_app_buttonsb:hover,.succ_app_buttons2b:hover{
		background:rgba(255, 255, 255, 0.2);
    color: #54514c;
	border:#fff solid 2px;
    -webkit-transition: background 0.5s linear;
    -moz-transition: background 0.3s linear;
    -ms-transition: background 0.3s linear;
    -o-transition: background 0.3s linear;
    transition: background 0.4s ease-in-out;
-webkit-box-shadow: 0px 0px 16px 5px rgba(42,171,253,0.56);
-moz-box-shadow: 0px 0px 16px 5px rgba(42,171,253,0.56);
box-shadow: 0px 0px 16px 5px rgba(42,171,253,0.56);
	}
#succ_div2,#succ_div4{padding-left:8%}
</style>
<?php
/*
  Template Name: Success stories
 */
?>
<?php get_header(); ?>
<div class="container-fluid">
   <div class="row scroll-target" id="succ_div1">
       <div class="col-xs-12 col-sm-12 col-md-5">
                         <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/02.png" alt="roadyoimg" class="img-responsive" id="suc_img1">
			 <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/roadyo-phone-image-1.png" alt="roadyoimg" class="img-responsive" id="suc_img2">
      </div>
	  <div class="col-xs-12 col-sm-12 col-md-7">
	         <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/rapido.png" alt="rapidologo" class="img-responsive" id="roadyo">
             <p id="suc_para1">How many times have you seen a bike-rider zoom past while you’re stuck in the long and congested traffic ? How many times have you repented paying so much while traveling alone in a cab ?
             Introducing Rapido - a nextgen platform which provides Bike Taxi Services in India. Rapido is the cheapest, fastest and most convenient way to travel/commute intracity.</p>
             <a href="#" id="succ_button">VIEW MORE DETAILS</a>
			 <div style="" id="success_downloadbut">
				<div style="float:left;margin-left:30px;margin-top:30px"><p id="app_buttons">DRIVER APP</p>
				   <div class="succ_app_buttons"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/android-logo.png" width="30px" alt="googleplay"><span style="margin-left:10px">GOOGLE PLAY</span></div><br>
				   <div class="succ_app_buttons2"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/apple-black-logo.png" alt="Iphone icon" width="28px"><span style="margin-left:30px">APP STORE</span></div>
				</div>
			 </div>
      </div>
    </div>
	
   <div class="row scroll-target" id="succ_div2">
     <div class="col-xs-12 col-sm-12 col-md-7">
	         <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/prive-text.png" alt="primelogo" class="img-responsive" id="primelogo">
             <p id="suc_para2">How many times have you seen a bike-rider zoom past while you’re stuck in the long and congested traffic ? How many times have you repented paying so much while traveling alone in a cab ?
             Introducing Rapido - a nextgen platform which provides Bike Taxi Services in India. Rapido is the cheapest, fastest and most convenient way to travel/commute intracity.</p>
             <a href="#" id="succ_button2">VIEW MORE DETAILS</a>
			 <div>
			    <div id="ss_sect2a">
				 <p id="app_buttons2">CUSTOMER APP</p>
				   <div class="succ_app_buttonsb"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/window-hover+(1).png" width="30px" alt="googleplay"><span style="margin-left:10px">GOOGLE PLAY</span></div><br>
				   <div class="succ_app_buttons2b"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/apple-hover.png" alt="Iphone icon" width="28px"><span style="margin-left:30px">APP STORE</span></div>
				</div>
				<div id="ss_sect2b"><p id="app_buttons2">DRIVER APP</p>
				   <div class="succ_app_buttonsb"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/window-hover+(1).png" width="30px" alt="googleplay"><span style="margin-left:10px">GOOGLE PLAY</span></div><br>
				   <div class="succ_app_buttons2b"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/apple-hover.png" alt="Iphone icon" width="28px"><span style="margin-left:30px">APP STORE</span></div>
				</div>
			 </div>
      </div>
	  <div class="col-xs-12 col-sm-12 col-md-5">
             <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/prive+image+1.png" alt="roadyoimg" class="img-responsive" id="suc_img1">
			 <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/priveimage+2.png" alt="roadyoimg" class="img-responsive" id="suc_img2">
      </div>
   </div>

<div class="row scroll-target" id="succ_div3">
       <div class="col-xs-12 col-sm-12 col-md-5">
                         <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/Untitled-1.png" alt="roadyoimg" class="img-responsive" id="suc_img1">
			 <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/baxi+image+2.png" alt="roadyoimg" class="img-responsive" id="suc_img2">
      </div>
	  <div class="col-xs-12 col-sm-12 col-md-7">
	         <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/baxi.png" alt="baxilogo" class="img-responsive" id="roadyo">
             <p id="suc_para1">How many times have you seen a bike-rider zoom past while you’re stuck in the long and congested traffic ? How many times have you repented paying so much while traveling alone in a cab ?
             Introducing Rapido - a nextgen platform which provides Bike Taxi Services in India. Rapido is the cheapest, fastest and most convenient way to travel/commute intracity.</p>
             <a href="#" id="succ_button">VIEW MORE DETAILS</a>
			 <div style="" id="success_downloadbut">
				<div style="float:left;margin-left:30px;margin-top:30px"><p id="app_buttons">DRIVER APP</p>
				   <div class="succ_app_buttons"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/android-logo.png" width="30px" alt="googleplay"><span style="margin-left:10px">GOOGLE PLAY</span></div><br>
				   <div class="succ_app_buttons2"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/apple-black-logo.png" alt="Iphone icon" width="28px"><span style="margin-left:30px">APP STORE</span></div>
				</div>
			 </div>
      </div>
    </div>

   <div class="row scroll-target" id="succ_div4">
     <div class="col-xs-12 col-sm-12 col-md-7">
	         <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/mober.png" alt="primelogo" class="img-responsive" id="primelogo">
             <p id="suc_para2">How many times have you seen a bike-rider zoom past while you’re stuck in the long and congested traffic ? How many times have you repented paying so much while traveling alone in a cab ?
             Introducing Rapido - a nextgen platform which provides Bike Taxi Services in India. Rapido is the cheapest, fastest and most convenient way to travel/commute intracity.</p>
             <a href="#" id="succ_button2">VIEW MORE DETAILS</a>
			 <div>
			    <div id="ss_sect2a">
				 <p id="app_buttons2">CUSTOMER APP</p>
				   <div class="succ_app_buttonsb"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/window-hover+(1).png" width="30px" alt="googleplay"><span style="margin-left:10px">GOOGLE PLAY</span></div><br>
				   <div class="succ_app_buttons2b"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/apple-hover.png" alt="Iphone icon" width="28px"><span style="margin-left:30px">APP STORE</span></div>
				</div>
				<div id="ss_sect2b"><p id="app_buttons2">DRIVER APP</p>
				   <div class="succ_app_buttonsb"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/window-hover+(1).png" width="30px" alt="googleplay"><span style="margin-left:10px">GOOGLE PLAY</span></div><br>
				   <div class="succ_app_buttons2b"><img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/apple-hover.png" alt="Iphone icon" width="28px"><span style="margin-left:30px">APP STORE</span></div>
				</div>
			 </div>
      </div>
	  <div class="col-xs-12 col-sm-12 col-md-5">
             <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/mober-image-1.png" alt="roadyoimg" class="img-responsive" id="suc_img1">
			 <img src="https://s3-ap-southeast-1.amazonaws.com/appscrip/success+/mober-image-2.png" alt="roadyoimg" class="img-responsive" id="suc_img2">
      </div>
   </div>
</div>


<?php get_template_part( 'template/section_pricing');  ?>
<?php get_template_part( 'template/section_countactus');  ?>
<?php get_footer(); ?>
<script>
  function hover(element) {
    element.setAttribute('src', 'http://localhost/wordpress/wp-content/uploads/2016/06/window-hover.png');
}
function unhover(element) {
    element.setAttribute('src', 'http://localhost/wordpress/wp-content/uploads/2016/06/window-icon.png');
}
</script>
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script>

/* RowScroll 1.1.1, https://github.com/ericbutler555/row-scroll */
/*editable->*/ var scrollTargets=".scroll-target", wiggleRoom=10, scrollSpeed=400;
$.extend($.easing,{eoq:function(t,e,n,o,i){return-o*(e/=i)*(e-2)+n}}),$(document).ready(function(){function t(){o.each(function(){i.push(Math.ceil($(this).offset().top))})}function e(t){$("html, body").stop().animate({scrollTop:$(t).offset().top},c,"eoq")}function n(t){var n=window.scrollY;if(t.deltaY>0)var c="up";else if(t.deltaY<=0)var c="down";if("down"==c){for(var l=0;r>l;l++)if(n>=i[l]-a&&n<i[l+1]-a){e(o[l+1]);break}}else if("up"==c)for(var l=0;r>l;l++)if(n<=i[l+1]+a&&n>i[l]+a){e(o[l]);break}return!1}var o=$(scrollTargets),i=[],a=wiggleRoom,c=scrollSpeed,r=o.length,l=0,u=0;r>0&&(t(),$(window).on("resize",function(){i.length=0,t()}),$(window).on("mousewheel",function(t){t.preventDefault(),n(t)}),$(window).on("touchstart",function(t){tsX=t.originalEvent.touches[0].clientX,tsY=t.originalEvent.touches[0].clientY}),$(window).on("touchmove",function(t){tmX=t.originalEvent.changedTouches[0].clientX,tmY=t.originalEvent.changedTouches[0].clientY,Math.abs(l-tmX)>30&&Math.abs(u-tmY)<5||(0==u||Math.abs(u-tmY)>1)&&t.preventDefault(),l=tmX,u=tmY}),$(window).on("touchend",function(t){if(teX=t.originalEvent.changedTouches[0].clientX,teY=t.originalEvent.changedTouches[0].clientY,Math.abs(tsX-teX)>50)return!0;if(tsY>teY+5)t.deltaY=-1;else{if(!(tsY<teY-5))return!0;t.deltaY=1}t.preventDefault(),n(t)}))});

</script>