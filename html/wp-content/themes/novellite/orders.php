<?php
/*
  Template Name: myaccount_orders page
 */
?>

<?php get_header(); ?>
<style>
nav.woocommerce-MyAccount-navigation li:hover {
    border-bottom: 2px solid grey;
   
    transition: width 2s, height 4s;
}
.woocommerce-MyAccount-content {
    margin-top: 5%;
}
nav.woocommerce-MyAccount-navigation li {
    list-style: none;
    display: inline;
    font-size: 20px;
    border-bottom: 2px solid;
    margin: 0% 8% 0% 0%;
    margin-bottom: 15%;
    padding: 0% 0% 1% 0%;
}
.page-container {
    margin-bottom: 10px!important;    
    margin-left: 10%;
    margin-right: 10%;   
}
.navbar-default .nav li a{color:#000} 
.navbar-default.navbar-fixed-top {
    z-index: 999999!important;
}

input.woocommerce-Button.button {
    background: #108ffd;
    width: 30%;
    outline: 0px;
}
form.login {
    display: none;
}
.woocommerce .woocommerce-info, .woocommerce-page .woocommerce-info {
    border-top: 3px solid #1e85be;
    border-radius: 0;
    box-shadow: none;
}
nav.woocommerce-MyAccount-navigation li {
    list-style: square!important;
    display: inline;
    font-size: 17px;
    border-bottom: 0px solid;
    margin: 0% 8% 0% 0%;
    margin-bottom: 1%;
    padding: 0% 0% 1% 0%;
    margin-left: -4%;
    width: 100%;
    /* right: 0; */
}
.woocommerce form .form-row .input-text, .woocommerce-page form .form-row .input-text {
    border: none;
    border-bottom: 1px solid #cfc9c9;
}
span.woocommerce-Price-amount.amount {
    font-family: sans-serif;
}
@media only screen and (min-width: 200px) and (max-width: 767px){
	
	nav.woocommerce-MyAccount-navigation li {
     list-style: square; 
     display: block; 
     font-size: 14px; 
    margin: 0% 8% 0% 0%;
    margin-bottom: 4%;
    padding: 0% 0% 1% 0%;
    border-bottom: transparent;
}
.vc_column_container>.vc_column-inner {
    box-sizing: border-box;
    padding-left: 15px;
    padding-right: 15px;
    width: 100%!important;
}
.woocommerce form .form-row .input-text, .woocommerce-page form .form-row .input-text {
    border: none;
    border-bottom: 1px solid #bbb9b9;
}
h2 {
    font-size: 20px;
}
td.product-name {
    font-size: 12px;
    font-weight: 600;
}
span.woocommerce-Price-amount.amount {
    font-family: sans-serif;
}
p {
    font-size: 14px;   
}
}
.woocommerce-account .woocommerce-MyAccount-navigation {
    float: left;
    width: 60%;
}
.woocommerce-account .woocommerce-MyAccount-content {
     float: none; 
     width: 100%; 
}

</style>
<h1 style="    text-align: center;
    font-weight: bolder;
    margin-top: 100px;
    font-size: 28px;
    letter-spacing: 1px;
    color: #7d7bab;
    padding-bottom: 10px;
    border-bottom: solid 1px #ececec;">ORDERS</h1>

   


<div class="page-container">
    <div class="container-fluid" style="padding-right: 0px; 
     padding-left: 0px; ">
        <div class="row">
            <div class="page-content">
             <div class="col-md-12">
              <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
				<?php the_content(); ?>	
			<?php endif; ?>	  
				</div>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>
</div>

<?php get_footer(); ?>
