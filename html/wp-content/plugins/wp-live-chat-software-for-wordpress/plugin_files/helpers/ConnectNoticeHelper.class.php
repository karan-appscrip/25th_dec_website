<?php

require_once('LiveChatHelper.class.php');

class ConnectNoticeHelper extends LiveChatHelper
{
    public function render()
    {
        ?>
        <div class="lc-design-system-typography notice notice-info lc-notice" id="lc-connect-notice">
            <div class="lc-notice-column">
                <img class="lc-notice-logo" src="<?php echo plugins_url('wp-live-chat-software-for-wordpress').'/plugin_files/images/livechat-logo.svg'; ?>" alt="LiveChat logo" />
            </div>
            <div class="lc-notice-column">
                <p id="lc-connect-notice-header">
                    <?php _e('Action required - connect LiveChat', 'wp-live-chat-software-for-wordpress') ?>
                </p>
                <p>
                    <?php _e('Please') ;?>
                    <a href="admin.php?page=livechat_settings"><?php _e('connect your LiveChat account'); ?></a>
                    <?php _e('to start chatting with your customers.', 'wp-live-chat-software-for-wordpress'); ?>
                </p>
            </div>
            <div class="lc-notice-column" id="lc-connect-notice-button-column">
                <p>
                    <button class="lc-btn lc-btn--primary" id="lc-connect-notice-button" type="button">
                        <?php _e('Connect', 'wp-live-chat-software-for-wordpress'); ?>
                    </button>
                </p>
            </div>
        </div>
        <?php
    }
}
