<?php

require_once('LiveChatHelper.class.php');

class ReviewNoticeHelper extends LiveChatHelper
{
    public function render()
    {
        ?>
        <div class="lc-design-system-typography lc-notice notice notice-info is-dismissible" id="lc-review-notice">
            <div class="lc-notice-column">
                <img class="lc-notice-logo" src="<?php echo plugins_url('wp-live-chat-software-for-wordpress').'/plugin_files/images/livechat-logo.svg'; ?>" alt="LiveChat logo" />
            </div>
            <div class="lc-notice-column">
                <p><?php _e('Hey, you’ve been using <strong>LiveChat</strong> for more than 14 days - that’s awesome! Could you please do us a BIG favour and <strong>give LiveChat a 5-star rating on WordPress</strong>? Just to help us spread the word and boost our motivation.', 'wp-live-chat-software-for-wordpress'); ?></p>
                <p><?php _e('<strong>&ndash; The LiveChat Team</strong>'); ?></p>
                <div id="lc-review-notice-actions">
                    <a href="https://wordpress.org/support/plugin/wp-live-chat-software-for-wordpress/reviews/#new-post" target="_blank" class="lc-review-notice-action lc-btn lc-btn--compact lc-btn--primary" id="lc-review-now">
                        <i class="material-icons">thumb_up</i> <span><?php _e('Ok, you deserve it', 'wp-live-chat-software-for-wordpress'); ?></span>
                    </a>
                    <a href="#" class="lc-review-notice-action lc-btn lc-btn--compact" id="lc-review-postpone">
                        <i class="material-icons">schedule</i> <span><?php _e('Maybe later', 'wp-live-chat-software-for-wordpress'); ?></span>
                    </a>
                    <a href="#" class="lc-review-notice-action lc-btn lc-btn--compact" id="lc-review-dismiss">
                        <i class="material-icons">not_interested</i> <span><?php _e('No, thanks', 'wp-live-chat-software-for-wordpress'); ?></span>
                    </a>
                </div>
            </div>
        </div>
        <?php
    }
}
