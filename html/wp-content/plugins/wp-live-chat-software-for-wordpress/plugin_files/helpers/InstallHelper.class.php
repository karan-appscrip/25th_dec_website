<?php

require_once('LiveChatHelper.class.php');

class InstallHelper extends LiveChatHelper {
    public function add_nonce() {
        echo '&nonce=' . wp_create_nonce('livechat-security-check');
    }

    public function render() {
        $user = LiveChat::get_instance()->get_user_data();
        $url = get_site_url();

        ?>
        <div class="lc-design-system-typography lc-table">
            <div class="lc-column">
                <p id="lc-plus-wp">
                    <img src="<?php echo plugins_url('wp-live-chat-software-for-wordpress'); ?>/plugin_files/images/lc-plus-wp.png" srcset="<?php echo plugins_url('wp-live-chat-software-for-wordpress'); ?>/plugin_files/images/lc-plus-wp.png, <?php echo plugins_url('wp-live-chat-software-for-wordpress'); ?>/plugin_files/images/lc-plus-wp@2x.png 2x" alt="LiveChat for Wordpress">
                </p>
                <p>
                    <iframe id="login-with-livechat" src="https://addons.livechatinc.com/sign-in-with-livechat/wordpress/?designSystem=1&popupRoute=signup&partner_id=wordpress&utm_source=wordpress.org&utm_medium=integration&utm_campaign=wordpress_plugin&email=<?php echo urlencode($user['email']); ?>&name=<?php echo urlencode($user['name']); ?>&url=<?php echo urlencode($url) ?>" > </iframe>
                </p>
                <form id="licenseForm" action="?page=livechat_settings&actionType=install<?php $this->add_nonce(); ?>" method="post">
                    <input type="hidden" name="licenseEmail" id="licenseEmail">
                    <input type="hidden" name="licenseNumber" id="licenseNumber">
                </form>
            </div>
            <div class="lc-column">
                <p>
                    <img src="<?php echo plugins_url('wp-live-chat-software-for-wordpress').'/plugin_files/images/lc-app.png'; ?>" alt="LiveChat apps" id="lc-app-img">
                </p>
                <p>
                    <?php _e('Check out our apps for', 'wp-live-chat-software-for-wordpress'); ?>
                    <a href="https://www.livechatinc.com/applications/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=wordpress_plugin" target="_blank">
                        <?php _e('desktop or mobile!', 'wp-live-chat-software-for-wordpress'); ?>
                    </a>
                </p>
            </div>
        </div>
        <?php
    }
}
