<?php

require_once('LiveChatHelper.class.php');

class SettingsHelper extends LiveChatHelper
{
    public function add_nonce() {
        echo '&nonce=' . wp_create_nonce('livechat-security-check');
    }

    public function render()
    {

        $license_email = LiveChat::get_instance()->get_login();
        $license_id = LiveChat::get_instance()->get_license_number();
        $settings = LiveChat::get_instance()->get_settings();

        if (isset($_GET['actionType']) && $_GET['actionType'] === 'install') { ?>
            <div class="updated installed">
                <p>
                    <?php _e('LiveChat is now installed on your website!', 'wp-live-chat-software-for-wordpress'); ?>
                </p>
                <span id="installed-close">x</span>
            </div>
        <?php } ?>
        <div class="lc-design-system-typography lc-table">
            <div class="lc-column">
                <p id="lc-plus-wp">
                    <img src="<?php echo plugins_url('wp-live-chat-software-for-wordpress'); ?>/plugin_files/images/lc-plus-wp.png" srcset="<?php echo plugins_url('wp-live-chat-software-for-wordpress'); ?>/plugin_files/images/lc-plus-wp.png, <?php echo plugins_url('wp-live-chat-software-for-wordpress'); ?>/plugin_files/images/lc-plus-wp@2x.png 2x" alt="LiveChat for Wordpress">
                </p>
                <p>
                    <?php _e('Currently you are using your', 'wp-live-chat-software-for-wordpress'); ?><br>
                    <strong><?php echo $license_email ?></strong><br>
                    <?php _e('LiveChat account.', 'wp-live-chat-software-for-wordpress'); ?>
                </p>
                <p id="lc-webapp">
                    <a href="https://my.livechatinc.com/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=wordpress_plugin" target="_blank">
                        <button class="lc-btn lc-btn--primary">
                            <?php _e('Open web application', 'wp-live-chat-software-for-wordpress'); ?>
                        </button>
                    </a>
                </p>
                <div class="settings">
                    <div>
                        <div class="title">
                            <span><?php _e('Hide chat on mobile', 'wp-live-chat-software-for-wordpress'); ?></span>
                        </div>
                        <div class="onoffswitch">
                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="disableMobile" <?php echo ($settings['disableMobile']) ? 'checked': '' ?>>
                            <label class="onoffswitch-label" for="disableMobile">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                    <div>
                        <div class="title">
                            <span><?php _e('Hide chat for Guest visitors', 'wp-live-chat-software-for-wordpress'); ?></span>
                        </div>
                        <div class="onoffswitch">
                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="disableGuests" <?php echo ($settings['disableGuests']) ? 'checked': '' ?>>
                            <label class="onoffswitch-label" for="disableGuests">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <iframe id="login-with-livechat" src="https://addons.livechatinc.com/sign-in-with-livechat" style="display: none"></iframe>
                <p class="lc-meta-text">
                    <?php _e('Something went wrong?', 'wp-live-chat-software-for-wordpress'); ?> <a id="resetAccount" href="?page=livechat_settings&reset=1<?php $this->add_nonce(); ?>" style="display: inline-block">
                        <?php _e('Disconect your account.', 'wp-live-chat-software-for-wordpress'); ?>
                    </a>
                </p>
            </div>
            <div class="lc-column">
                <p>
                    <img src="<?php echo plugins_url('wp-live-chat-software-for-wordpress').'/plugin_files/images/lc-app.png'; ?>" alt="LiveChat apps" id="lc-app-img">
                </p>
                <p>
                    <?php _e('Check out our apps for', 'wp-live-chat-software-for-wordpress'); ?>
                    <a href="https://www.livechatinc.com/applications/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=wordpress_plugin" target="_blank">
                        <?php _e('desktop or mobile!', 'wp-live-chat-software-for-wordpress'); ?>
                    </a>
                </p>
            </div>
            <script>
                var lcDetails = {
                    license: <?php echo htmlspecialchars($license_id); ?>,
                    email: '<?php echo htmlspecialchars($license_email); ?>',
                    nonce: '<?php $this->add_nonce(); ?>'
                }
            </script>
        </div>
        <?php
    }
}